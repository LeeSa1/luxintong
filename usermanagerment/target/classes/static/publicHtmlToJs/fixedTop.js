document.writeln("<div id=\'top\'>");
document.writeln("		<div class=\'fixed\'>");
document.writeln("<span  class=\'logo\'>");
document.writeln("			<img src=\'img/logo.png\' style=\'width: 30px; height: 30px;margin-top: -5px;\'> ");
document.writeln("			  <span class=\'unitname\'></span>");
document.writeln("			  <span class=\'norname\'>执法审批管理系统</span>");
document.writeln("</span>");
document.writeln("			<ul class=\'top-menu\'>");
document.writeln("				<li><a class=\'fullview\'></a></li>");
document.writeln("");
document.writeln("				<li id=\'unReadMessage\'><a href=\'notice\' title=\'\' class=\'messages\'><i");
document.writeln("					style=\'display : none; \'	class=\'new-message\'></i></a></li>");
document.writeln("				<li class=\'dropdown\' style=\'margin-right: 5px\'><a class=\'user-menu\'");
document.writeln("					data-toggle=\'dropdown\'><img alt=\'\' width=\'20px\' height=\'20px\'");
document.writeln("						class=\'touXiang\' /><span class=\'userName\'><b class=\'caret\'></b></span></a>");
document.writeln("					<ul class=\'dropdown-menu\' style=\'min-width: 120px;\'>");
document.writeln("						<li><a href=\'javascript:void(0);\' title=\'\'");
document.writeln("							onclick=\'showMessage();\'><i class=\'icon-user\'></i>个人资料</a></li>");
document.writeln("						<li><a href=\'notice\' title=\'\'><i class=\'icon-inbox\'></i>站内消息<span");
document.writeln("								class=\'badge badge-info\' style=\'display:none;\' id=\'notRead\'></span></a></li>");
document.writeln("						<li><a href=\'javascript:void(0)\' title=\'\'");
document.writeln("							onclick=\'showResetPassword();\'><i class=\'icon-cog\'></i>重置密码</a></li>");
document.writeln("						<li><a href=\'javascript:void(0);\' title=\'\'");
document.writeln("							onclick=\'logOut();\'><i class=\'icon-remove\'></i>注销</a></li>");
document.writeln("					</ul>");
document.writeln("			</ul>");
document.writeln("		</div>");
document.writeln("	</div>");

/**
 * 得到登录的执法人员所在的执法单位显示在系统头部
 */
function getOrgName() {
    var officeId = $.cookie("officeId");
    if(officeId != null){
        $.ajax({
            url : "/luzheng/app/common!readOrgInfo",
            data : {"id" : officeId},
            success : function (data) {
                data = JSON.parse(data);
                if(data.message != null){
                    $(".unitname").text(data.message.org);
                }else{
                }
            }
        });
    }
}
getOrgName();