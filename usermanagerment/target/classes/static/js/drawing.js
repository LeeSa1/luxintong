/**
 * Created by BuDD on 2017/12/25.
 */
$(function () {
    init();
    getCaseCount();
})
function init() {
    $.ajax({
        url : "/luzheng/app/pb!readCaseInfoByDraw",
        type : "get",
        data : {
            "caseInfoExample.startDate" : new Date($("#startDate1").val()).getTime(),
            "caseInfoExample.endDate" : new Date($("#startDate2").val()).getTime(),
            "caseInfoExample.caseType" : $("#caseType").find('option:selected').val(),
            "caseInfoExample.orgName" : $("#selectOrg").find('option:selected').val(),
        },
        success : function(data) {
            if(data != null && data.length > 0) {
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    console.log(data);
                    var chufa = getChufa(obj.message[0]);
                    var pb = getPb(obj.message[1]);
                    var qz = getQz(obj.message[2]);
                    var xk = getXk(obj.message[3]);
                    draw(chufa, pb, qz, xk);
                }
            }
        }
    })
}

function draw(chufa,pb,qz,xk) {

    var dom = document.getElementById("draw");
    var myChart = echarts.init(dom);
    var app = {};
    option = null;
    option = {
        title: {
            text: '案件情况',
        },
        tooltip: {
            trigger: 'axis'
        }, legend: {
            data: getType()
        },
        toolbox: {
            show: true,
            feature: {
                magicType: {show: true, type: ['stack', 'tiled']},
                saveAsImage: {show: true}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: getX()
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                name: '行政处罚',
                type: 'line',
                smooth: true,
                data: chufa
            },
            {
                name: '路产配补偿',
                type: 'line',
                smooth: true,
                data: pb
            },
            {
                name: '行政强制',
                type: 'line',
                smooth: true,
                data: qz
            },
            {
                name: '行政许可',
                type: 'line',
                smooth: true,
                data: xk
            }
        ]
    };
    ;
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }
}


function getX(){
    var months = ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'];
    // var day = [];
    return months;
}

/**
 * 根据案件来进行分类
 * @param arr
 */
function getCase(arr) {
    for(var i=0; i<arr.length; i++) {
        if(i == 0) {
            getChufa(arr[i]);
        }else if( i== 1) {
            getPb(arr[i]);
        }else if( i == 2) {
            getQz(arr[i]);
        }else{
            getXk(arr[i]);
        }
    }
}


function getChufa(arr){
    var data = new Array(arr.length);
    for(var i=0; i<arr.length; i++) {
        data[i] = arr[i];
    }
    return data;
}

function getPb(arr) {
    var data = new Array(arr.length);
    for(var i=0; i<arr.length; i++) {
        data[i] = arr[i];
    }
    return data;
}

function getQz(arr) {
    var data = new Array(arr.length);
    for(var i=0; i<arr.length; i++) {
        data[i] = arr[i];
    }
    return data;
}

function getXk(arr) {
    var data = new Array(arr.length);
    for(var i=0; i<arr.length; i++) {
        data[i] = arr[i];
    }
    return data;
}

function getType() {
    var arr = ['行政处罚', '路产配补偿', '行政强制','行政许可'];
    var type = $("#caseType").val();
    if(type != undefined && type>0) {
        arr = [arr[type-1]];
    }
    return arr;
}

/**
 * 得到本单位的案件数量及下级单位的案件数量
 */
function getCaseCount(){
    $.ajax({
        url: "/luzheng/app/flow!getCaseCount",
        success: function(data){
            if(data != null && data.length > 0) {
                var obj = JSON.parse(data);
                console.log(data);
                if(obj.status == "success"){
                    for(var i=0; i<obj.message.length / 2; i++) {
                       add(i);
                       $("#orgName"+i).text(obj.message[i]);
                       $("#caseCount"+i).text(obj.message[i + obj.message.length / 2]);
                    }
                }
            }else{

            }
        }
    })
}
