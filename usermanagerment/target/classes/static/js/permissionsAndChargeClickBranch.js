// 判断点击的是哪个分支下的页面 并展开相应分支
var array1 = ["执法单位列表", "执法单位", "执法人员", "账号管理"];
var array2 = ["行政许可", "行政强制", "路产赔补偿", "行政处罚"];
var array3 = ["当事人", "车辆信息", "案件文书编号", "行政处罚案由", "处罚案件案由", "强制案件案由", "赔补偿案件案由", "许可申请事项",
    "审批意见"];
var array4 = ["案发地图统计", "线性图统计"];
var array5 = ["站内消息", "消息通知", "手机短信"];
$(function () {
    var obj = $(".breadcrumb").children().eq(1).text();
    for (var i = 1; i < 6; i++) {
        id = "ChildMenu" + i;
        $("#" + id).attr("class", "collapse");
    }
    if (($.inArray(obj, array1)) > -1) {
        $("#ChildMenu1").attr("class", "expanded");
    } else if (($.inArray(obj, array2)) > -1) {
        $("#ChildMenu2").attr("class", "expanded");
    } else if (($.inArray(obj, array3)) > -1) {
        $("#ChildMenu3").attr("class", "expanded");
    } else if (($.inArray(obj, array4)) > -1) {
        $("#ChildMenu4").attr("class", "expanded");
    } else if (($.inArray(obj, array5)) > -1) {
        $("#ChildMenu5").attr("class", "expanded");
    }
});

var bath = "/luzheng";

function DoMenu(emid) {
// 保存页面点击的上一个一级菜单id
    if ($.cookie("LastLeftID") == undefined) {
        $.cookie("LastLeftID", emid, {
            path: bath
        })
    } else {
        $.cookie("LastLeftID", $.cookie("LastLeftID"));
    }
    var obj = document.getElementById(emid);
    //当前点击一级菜单是关闭状态则展开,是展开状态则关闭
    obj.className = (obj.className.toLowerCase() == "expanded" ? "collapse"
        : "expanded");

    if (($.cookie("LastLeftID") != "") && (emid != $.cookie("LastLeftID"))) // 关闭上一个Menu
    {
        document.getElementById($.cookie("LastLeftID")).className = "collapse";
        //Jquery不能修改
        $.cookie("LastLeftID", emid, {
            path: bath
        });
    }
}

// 用户权限的校验 查看自己权限内的内容
showName();
showImage();
var vt = $.cookie('userType');
// 超级管理员
if (vt == "0") {
    $("#organization").show();
    $("#case").show();
    $("#information").show();
    $("#satistical").show();
    $("#SMS").show();
}
// 执法单位
if (vt == "1") {
    $("#organization").show();
    $("#case").hide();
    $("#information").hide();
    $("#satistical").hide();
    $("#SMS").hide();
}
// 执法人员
if (vt == "2") {
    $("#organization").hide();
    $("#case").show();
    $("#information").show();
    $("#satistical").show();
    $("#SMS").show();
}

var orgAuth = $.cookie('orgSmallAuth') == undefined ? null : $.cookie('orgSmallAuth').split(',');
var userBig = $.cookie('authBigCode').split(',');
var userSamll = $.cookie('authSmallCode').split(',');
if (userBig != undefined && userBig.length > 0) {
    var len = userBig.length;
    for (var i = 0; i < len; i++) {
        switch (userBig[i]) {
            case "1" :
                $("#case").hide();
                break;
            case "2" :
                $("#information").hide();
                break;
            case "3" :
                $("#satistical").hide();
                break;
            case "4" :
                $("#SMS").hide();
                break;
        }
    }
}

if (userSamll != undefined && userSamll.length > 0) {
    var len = userSamll.length;
    for (var i = 0; i < len; i++) {
        switch (userBig[i]) {
            case "1" :
                $("#cfcase").hide();
                break;
            case "2" :
                $("#pbcase").hide();
                break;
            case "3" :
                $("#qzcase").hide();
                break;
            case "4" :
                $("#xkcase").hide();
                break;
            case "5" :
                $("#person").hide();
                break;
            case "6" :
                $("#car").hide();
                break;
            case "7" :
                $("#document-no").hide();
                break;
            case "8" :
                $("#caseAction").hide();
                break;
            case "9" :
                $("#casesataAction").hide();
                break;
            case "a" :
                $("#constraintAction").hide();
                break;
            case "b" :
                $("#permit-apply").hide();
                break;
            case "c" :
                $("#approval").hide();
                break;
            case "d" :
                $("#count-cri").hide();
                break;
            case "e" :
                $("#count-lin").hide();
                break;
            case "f" :
                $("#notice").hide();
                break;
            case "g" :
                $("#mobnew").hide();
                break;
        }
    }
}

if (orgAuth != null && orgAuth.length > 0) {
    var len = orgAuth.length;
    for (var i = 0; i < len; i++) {
        switch (orgAuth[i]) {
            case "1" :
                $("#cfcase").hide();
                break;
            case "2" :
                $("#pbcase").hide();
                break;
            case "3" :
                $("#qzcase").hide();
                break;
            case "4" :
                $("#xkcase").hide();
                break;
            case "5" :
                $("#person").hide();
                break;
            case "6" :
                $("#car").hide();
                break;
            case "7" :
                $("#document-no").hide();
                break;
            case "8" :
                $("#caseAction").hide();
                break;
            case "9" :
                $("#casesataAction").hide();
                break;
            case "a" :
                $("#constraintAction").hide();
                break;
            case "b" :
                $("#permit-apply").hide();
                break;
            case "c" :
                $("#approval").hide();
                break;
            case "d" :
                $("#count-cri").hide();
                break;
            case "e" :
                $("#count-lin").hide();
                break;
            case "f" :
                $("#notice").hide();
                break;
            case "g" :
                $("#mobnew").hide();
                break;
        }
    }
}


//得到当前登录人员的执法证号
var toUser = $.cookie("cardId");
/* 判断是当前登录人员是否有未读站内消息 */
$(function () {
    var notReadCount = 0;//未读消息
    $.ajax({
        url: "/luzheng/app/common!listSysMsgByMySelf",
        data: {"id": toUser},
        success: function (data) {
            data = JSON.parse(data);
            if (data.message != "") {
                for (var i = 0; i < data.message.length; i++) {
                    /* 如果有未读消息 显示标志 退出循环 */
                    if (data.message[i].readflag == 0) {
                        /* 显示站内消息未读标志 */
                        $("#unReadMessage").find("i").show();
                        notReadCount++;
                    }
                    if (notReadCount != 0) {
                        $("#notRead").show().text(notReadCount);
                    } else {
                        $("#notRead").hide();
                    }
                }
            }
        }
    });
});


