/**
 * Created by BuDD on 2017/12/18.
 */
$(function () {
    readCaseInfoByParams()
});

/**
 * 根据页面的四个参数进行分条件查询案件
 *  立案日期    开始--结束
 *  案件类型    1-4     处罚/赔补/强制/许可
 *  执法单位
 */
function readCaseInfoByParams() {
    var caseInfos = [];
    var startDate = new Date($("#startDate1").val()).getTime();
    var endDate = new Date($("#startDate2").val()).getTime();
    var caseType = $("#caseType").find('option:selected').val();
    var orgName = $("#applyAvoidFzjg").find('option:selected').val();
    $.ajax({
        url: "/luzheng/app/pb!readCaseInfoByParams",
        method: "get",
        data: {
            "caseInfoExample.caseType": caseType,
            "caseInfoExample.startDate": startDate,
            "caseInfoExample.endDate": endDate,
            "caseInfoExample.orgName": orgName
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    console.log(data);
                    if (obj.message.length > 0) {
                        for (var i = 0; i < obj.message[1].length; i++) {
                            var caseInfo = {
                                caseId: obj.message[0][i].id,
                                type: obj.message[0][i].caseType,
                                x: obj.message[1][i].x,
                                y: obj.message[1][i].y,
                                caseSite: obj.message[0][i].caseSite,
                                caseName: obj.message[0][i].caseName,
                                orgName: obj.message[0][i].orgName,
                                number: obj.message[0][i].number,
                                litigantName: obj.message[0][i].litigantName
                            };
                            caseInfos[i] = caseInfo;
                        }
                        init(caseInfos);
                    }
                }
            } else {
               NoDataInit();
            }
        }
    });
}


function NoDataInit(){
    //地图
    var map = new AMap.Map('map', {
        resizeEnable: true,
        zoom: 14
    });
    $(".amap-container").css("height", "500px");

}
function init(caseInfos) {
    //地图
    var map = new AMap.Map('map', {
        resizeEnable: true,
        zoom: 5,
    });

    $(".amap-container").css("height", "500px");

    //添加bar
    map.plugin('AMap.ToolBar', function () {
        map.addControl(new AMap.ToolBar());
    });

    //创建Geocoder对象
    var geocoder = null;

    //实例化
    AMap.service('AMap.Geocoder', function () {//回调函数
        //实例化Geocoder
        geocoder = new AMap.Geocoder();
    })

    var infoWindow = new AMap.InfoWindow({offset: new AMap.Pixel(0, -30)});
    for (var i = 0; i < caseInfos.length; i++) {
        if (caseInfos[i].x != undefined && caseInfos[i].x > 0 && ( caseInfos[i].y != undefined && caseInfos[i].y > 0)) {
            //添加点标
            marker = new AMap.Marker({
                map: map,
                bubble: true,
                position: [caseInfos[i].x, caseInfos[i].y]
            });

            var typeName = caseInfos[i].type == "1" ? "行政处罚" : caseInfos[i].type == "2" ? "路产配补偿" : "行政强制";
            var path = typeName == "行政处罚" ? "case.html" : (typeName == "路产配补偿" ? "pb-case.html" : "qz-case.html");
            var url = path + "?caseId=" + caseInfos[i].caseId;
            marker.content = '案件名称：' + caseInfos[i].caseName + ' <br>' +
                '案件编号: ' + caseInfos[i].number + '<br>' +
                '案件类型:' + typeName + '<br>' +
                '案发地址：' + caseInfos[i].caseSite + '<br>' +
                '执法单位：' + caseInfos[i].orgName + '<br>' +
                '当事人:' + caseInfos[i].litigantName + '<br>' +
                '详细情况: <a src="" onclick="show('+caseInfos[i].caseId+','+caseInfos[i].type+')">点击</a>';
            marker.on('mouseover', markerClick);
            marker.emit('mouseover', {target: marker});

        }
        map.setFitView();
    }

    function markerClick(e) {
        infoWindow.setContent(e.target.content);
        infoWindow.open(map, e.target.getPosition());
    }

    function closeInfoWindow() {
        map.clearInfoWindow();
    }

}

function show(id,type) {
    $.cookie(id,'read',{path : '/luzheng'});
    var url = type == 1 ? "cf.html?caseId="+id : (type == 2 ? "pb.html?caseId="+id : "qz.html?caseId="+id);
    window.location.href= "/luzheng/html/" + url;
}


    /**
 * 给table展现数据
 */
function showTable(arr) {
    var jsonData = "";
    console.log(arr.length);
    for (var i = 0; i < arr.length; i++) {
        if (i >= 1) {
            jsonData += ',{' +
                '"order" : ' + (i + 1) + ',' +
                '"number" : "' + arr[i].number + '",' +
                '"litigantName" : "' + arr[i].litigantName + '"}';
        } else {
            jsonData += '{' +
                '"order" : ' + (i + 1) + ',' +
                '"number" : "' + arr[i].number + '",' +
                '"litigantName" : "' + arr[i].litigantName + '"}';
        }
    }
    var obj = eval('[' + jsonData + ']');
    // console.log(jsonData);
    if ($("#table").bootstrapTable('getData').length > 0) {
        $("#table").bootstrapTable('destroy');
    }
    $("#table").bootstrapTable({
        data: obj,
    });
    $("#showSum").text('案件记录总条数:' + (obj.length));
}
/**
 * 案件对象
 * @param type  案件类型
 * @param x 经度
 * @param y 纬度
 * @param caseSite  案发地点
 * @param caseName  案件名称
 * @param orgName   执法单位名称
 * @param number    案件编号
 * @param litigantName  当事人
 */
function caseInfo(caseId, type, x, y, caseSite, caseName, orgName, number, litigantName) {
    this.caseId = caseId;
    this.type = type;
    this.x = x;
    this.y = y;
    this.caseSite = caseSite;
    this.caseName = caseName;
    this.orgName = orgName;
    this.number = number;
    this.litigantName = litigantName;
}
