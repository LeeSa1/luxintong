var caseId = GetQueryString("caseId");
var caseDocId;//需要审批文书id
var status;//需要审批文书录入状态
var endCase = false;//结案标志
var approve = false;//提交审批标志
var thisPageDocType;//本环节需要审批页面的文书对应的文书列表中的id
var caseLink;//案件所处环节

/**
 * 设置导航栏
 * 检查页面是执法人员录入页面还是审批人审批页面
 * 根据是否页面url有approveId确定是否是审批页面 有为审批人审批页面 没有为执法人员操作页面
 */
$(function () {
    getCaseLink();//得到案件环节
    setNavigationBar();//设置状态栏当前标题颜色突出
    if (GetQueryString("approveId") == null) {
        //当案件环节不是文书归档时才处理页面文书的操作
        if (caseLink != 7) {
            officerHandle();
        }
    } else {
        //审批人审批页面
        approveHandle(GetQueryString("approveId"));
    }
});

/**
 * 得到案件环节
 */
function getCaseLink() {
    if ($("#submitApproval").length > 0) {
        caseLink = $("#submitApproval").attr("onclick").replace(/[^0-9]/ig, "");//得到当前页面所在案件环节 根据提交审批函数参数
    } else {
        caseLink = $("#next").attr("onclick").replace(/[^0-9]/ig, "") - 1;//得到当前页面所在案件环节 根据下一步函数参数
    }
}

/**
 * 判断需要显示什么导航栏并且调用setNavigaHrefUrl设置当前环节标题颜色变深
 *
 */
function setNavigationBar() {
    //当前环节标题颜色变深
    $("#link" + caseLink).find("li").addClass("change");
}

/**
 * 导航栏跳转链接
 */
function setUrl(linkId) {
    var approveId = GetQueryString("approveId");
    switch (linkId) {
        case "link1":
            if (approveId != null) {
                window.location.href = "cf-normreg.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-normreg.html?caseId=" + caseId;
            }
            break;
        case "link2":
            if (approveId != null) {
                window.location.href = "cf-norminvest.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-norminvest.html?caseId=" + caseId;
            }
            break;
        case "link3":
            if (approveId != null) {
                window.location.href = "cf-normaudit.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-normaudit.html?caseId=" + caseId;
            }
            break;
        case "link4":
            if (approveId != null) {
                window.location.href = "cf-normhear.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-normhear.html?caseId=" + caseId;
            }
            break;
        case "link5":
            if (approveId != null) {
                window.location.href = "cf-normaction.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-normaction.html?caseId=" + caseId;
            }
            break;
        case "link7":
            if (approveId != null) {
                window.location.href = "cf-normfile.html?caseId=" + caseId + "&approveId=" + approveId;
            } else {
                window.location.href = "cf-normfile.html?caseId=" + caseId;
            }
            break;
        case "link8":
            if (approveId != null) {
                window.location.href = "cf-normfile.html?caseId=" + caseId + "&approveId=" + approveId + "&notlian=" + 2;
            } else {
                window.location.href = "cf-normfile.html?caseId=" + caseId + "&notlian=" + 2;
                ;
            }
            break;
        case "jianyi1":
            if (approveId != null) {
                window.location.href = "cf-simple.html?caseId=" + caseId + "&approveId=" + approveId + "&notlian=" + 2;
            } else {
                window.location.href = "cf-simple.html?caseId=" + caseId + "&notlian=" + 2;
            }
            break;
    }
}

/**
 * 执法人员操作页面 遍历读取文书状态
 */
function officerHandle() {
    $("tr").each(function () {
        var docType = $(this).children().eq(1).attr("docType");//得到对应文书的文书列表中的id
        var tdStatus = $(this).children().eq(1);
        $.ajax({
            url: "/luzheng/app/case!readCaseDocumentByCaseIdAndDocType",
            data: {
                "caseDocument.caseId": caseId,
                "caseDocument.docType": docType
            },
            async: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.message != null) {//文书已录入 设置状态
                    caseDocId = getCaseDocId(data.message.docType, data.message.id);
                    status = getStatus(data.message.docType, data.message.docStatus);
                    tdStatus.attr("caseDocId", data.message.id);//将录入文书的id存入caseDocId属性 提交审批有用
                    tdStatus.text(getDocStatus(data.message.docStatus));//设置文书状态(中文显示)
                } else {
                    //文书未录入
                    tdStatus.text(getDocStatus(0));
                }
            }
        });
    });
    setAllStatus(caseLink);//根据案件是否结案和本环节是否提交审批设置页面按钮和对文书的操作
}

/**
 * 审批人审批页面 只有审批按钮 所有文书只能查看
 * 其他页面没有任何按钮  所有文书只能查看
 * @param approveId 审批记录id
 */
function approveHandle(approveId) {
    /* 根据审批记录id读取审批记录 判断审批页面是哪个环节 */
    $.ajax({
        url: "/luzheng/app/case!getApproveById",
        data: {"id": approveId},
        success: function (data) {
            data = JSON.parse(data);
            setApproveButtonStatus(data.message.caseStatus, approveId);
        }
    });
}

/**
 * 设置审批页面的按钮只有审批 其他页面没有按钮且文书只能查看
 * @param caseStatus
 */
function setApproveButtonStatus(caseStatus, approveId) {
    $("tr").each(function () {
        $(this).find("a :first").remove();//设置文书状态只能查看
        var docType = $(this).children().eq(1).attr("docType");//得到对应文书的文书列表中的id
        var tdStatus = $(this).children().eq(1);
        $.ajax({
            url: "/luzheng/app/case!readCaseDocumentByCaseIdAndDocType",
            data: {
                "caseDocument.caseId": caseId,
                "caseDocument.docType": docType
            },
            async: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.message != null) {//文书已录入 设置状态
                    tdStatus.text(getDocStatus(data.message.docStatus));//设置文书状态(中文显示)
                } else {
                    //文书未录入
                    tdStatus.text(getDocStatus(0));
                }
            }
        });
    });
    //设置审批页面按钮只有审批 其他页面没有按钮
    if (caseLink == caseStatus) {
        $(".officerHandle").remove();
        //审批页面显示审批按钮
        var showApproveButtonHtml = '<div class="form-actions">'
            + '<button type="button" href="javascript:void(0)" class="btn btn-danger" onClick="goApprove(' + approveId + ')">审批</button>';
        +'</div>'
        $("#approveShowButton").append(showApproveButtonHtml);
    } else {
        $(".officerHandle").remove();
    }
}

function goApprove(approveId) {
    window.location.href = "cfCase-appmine.html?approveId=" + approveId;
}

/**
 * 根据案件是否结案和本环节是否提交审批来设置按钮和对文书的操作
 */
function setAllStatus(caseLink) {
    checkEndCaseAndApprove(caseLink);//设置结案标志
    if (endCase) {
        setAllButtonUnUse();//结案
    } else if (approve) {
        setApprovalAndResetButtonUnUse();//未结案但是已审批
    }
}

/**
 * 检查是否结案和是否提交审批
 */
function checkEndCaseAndApprove(caseLink) {
    //检查是否结案
    $.ajax({
        url: "/luzheng/app/case!readCaseInfo",
        data: {"id": caseId},
        async: false,
        success: function (data) {
            data = JSON.parse(data);
            if (data.message.status == -1) {
                endCase = true;
            } else {
                switch (parseInt(caseLink)) {
                    case 1://立案环节
                        thisPageDocType = 3;//立案审批书docType
                        break;
                    case 2://调查取证
                        thisPageDocType = 27;//案件处理意见书docType
                        break;
                    case 5://执行环节
                        thisPageDocType = 46;//处罚结案报告docType
                        break;
                    case 6://简易程序 案件处理环节
                        thisPageDocType = 46;//简易程序 处罚结案报告docType
                        break;
                    default: {
                        thisPageDocType = null;
                    }
                }
                //判断当前案件环节是否需要提交审批 需要提交审批才检查是否提交审批
                if (thisPageDocType != null) {
                    //案件未结案检查本环节是否提交审批
                    $.ajax({
                        url: "/luzheng/app/case!readCaseDocumentByCaseIdAndDocType",
                        data: {
                            "caseDocument.caseId": caseId,
                            "caseDocument.docType": thisPageDocType
                        },
                        async: false,
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data.message != "") {
                                if (data.message.docStatus != null) {
                                    if (data.message.docStatus > 1) {
                                        approve = true;//已经提交审批
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}


/**
 * 每个环节审批页面的文书id
 * @param docType 需要审批文书对应文书表中的id
 * @param caseDocId 录入文书的id
 */
function getCaseDocId(docType, docId) {
    switch (parseInt(docType)) {
        case 3 ://立案环节 立案审批文书
            return docId;
            break;
        case 27://调查取证环节 案件处理意见书
            return docId;
            break;
        case 46://一般流程执行环节/简易程序案件处理环节 处罚结案报告
            return docId;
            break;
        default: {
            return caseDocId;
        }
    }
}

/**
 * 每个环节审批页面的录入状态id
 * @param docType 需要审批文书对应文书表中的id
 * @param docStatus 需要审批文书的录入状态
 * @returns {*}
 */
function getStatus(docType, docSts) {
    switch (parseInt(docType)) {
        case 3 ://立案环节 立案审批文书
            return docSts;
            break;
        case 27://调查取证 案件处理意见书
            return docSts;
            break;
        case 46://一般流程执行环节/简易程序案件处理环节 处罚结案报告
            return docSts;
            break;
        default: {
            return status;
        }
    }
}

/**
 * 根据文书状态码返回文书状态 当文书的状态标志大于2时 说明审批人已经审批 需要读取每个审批人的审批结果 不能在从案件文书中读取它的状态 需要从审批表中读取
 * @param docMark 状态码 int类型
 * @returns {string} 中文名状态
 */
function getDocStatus(docMark) {
    switch (parseInt(docMark)) {
        case 0 :
            return "未录入";
            break;
        case 1 :
            return "已录入";
            break;
        case 2 :
            return "未审批";
            break;
        case 3 :
        case 4 :
        case 5 :
            return getApproveStatus();
    }
}

/**
 * 得到两个审批人的审批状态 如果两个人都已经审批 两个人必须全部审批同意才能进行下一步 否则只能结案
 */
function getApproveStatus() {
    var approveStatus = "";//审批状态描述
    var docStatus;//审批人审批状态标志
    var isBothApprove = true;//两个人是否已经审批
    var bothAgree = true;//默认两个人都同意
    $.ajax({
        url: "/luzheng/app/case!getApproveByCaseIdAndCaseStatus",
        data: {
            "caseId": caseId,
            "caseStatus": caseLink
        },
        async: false,
        success: function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.message.length; i++) {
                if(data.message[i].status != 3){//如果有人审批不同意 设置状态标志为false
                    if(data.message[i].status <= 2) {
                        //有人未审批
                        isBothApprove = false;
                    }
                    bothAgree = false;
                }
                docStatus = data.message[i].status > 2 ? (data.message[i].status == 3 ? "同意" : (data.message[i].status == 4 ? "不同意" : "保留意见")) : "未审批";
                var cardId = data.message[i].cardId;
                $.ajax({
                    url: "/luzheng/app/common!readOfficerByCardId",
                    data: {"cardId": cardId},
                    async: false,
                    success: function (data) {
                        data = JSON.parse(data);
                        approveStatus += data.message.userName + ":" + docStatus + "  ";
                    }
                });
            }
            //未结案才判断两个人审批状态 并设置按钮属性
            if(!endCase){
                //两个人必须都审批后才去看两个人的审批态度
                if(isBothApprove){
                    if(caseLink == 5){
                        //如果是执行环节  不管审批同意不同意  只要两个人都审批了 就可以结案
                        setNotAgree();
                    }else{
                        if(bothAgree){
                            //两个人都同意 显示下一步
                            setBothAgree();
                        }else{
                            //有人不同意 只有结案按钮
                            setNotAgree();
                        }
                    }
                }
            }

        }
    });
    return approveStatus;
}

/**
 * 重置文书
 */
function resetDocs() {
    var resetDoc = new Array();
    var mark = false;//标记页面是否存在录入的文书 没有为false 有为true
    $("tr").each(function () {
        if ($(this).children().eq(1).text() != "未录入") {
            mark = true;
            resetDoc.push($(this).children().eq(1).attr("docType"));//添加已经录入的文书
        }
    });
    if (mark) {
        if (window.confirm("重置将会清空已填写的文书内容,确定重置?")) {
            $.ajax({
                url: "/luzheng/app/case!resetCaseDocument",
                data: {
                    "caseId": caseId,
                    "resetDoc": resetDoc.toString()
                },
                success: function () {
                    window.history.go(0);//刷新页面 相当于客户端点F5刷新页面
                }
            });
        }
    } else {
        alert("当前没有文书录入!");
    }
}

/**
 * 跳转到文书归档换季节
 */
function finishCase() {
    window.location.href = "cf-normfile.html?caseId=" + caseId;
}

/**
 * 结案 改变案件状态
 */
function makeSureFinishCase() {
    var status = -1;
    if (window.confirm("确定结案,结案后不可再编辑案件!")) {
        $.ajax({
            url: "/luzheng/app/case!updateCaseInfo",
            data: {
                "caseInfo.id": caseId,
                "caseInfo.status": status
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == "success") {
                    window.location.href = 'cf-case.html';
                }
            }
        });
    }
}

/**
 * 案件移交处理
 * @constructor
 */
function HandOver() {
    window.location.href = "case-changeOfficer.html?caseId=" + GetQueryString("caseId");
}

/**
 * 显示与隐藏文书div
 * @param id 主控制div的id
 * @param showOrHide 显示与隐藏div的id
 */
function showOrHide(id) {
    $("#" + id).val() == 0 ? $("." + id).hide() : $("." + id).show();
}

/**
 * 下一步确定按钮 改变未结案案件所处环节
 */
function changeCaseStatus(status) {
    var html = "";
    switch (status) {
        case 2:
            html = "cf-norminvest.html";
            break;
        case 3:
            html = "cf-normaudit.html";
            break;
        case 4:
            html = "cf-normhear.html";
            break;
        case 5:
            html = "cf-normaction.html";
            break;
        case 6:
            html = "cf-normfile.html";
            break;

    }
    /* 如果案件未结案 改变案件的所处环节和下拉选项 */
    if (!endCase) {
        var options = {
            url: "/luzheng/app/case!updateCaseInfo",
            data: {
                "caseInfo.id": caseId,
                "caseInfo.status": status
            },
            type: "post",
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == "success") {
                    window.location.href = html + '?caseId=' + caseId;
                }
            }
        };
        $("form").ajaxSubmit(options);
    } else {//案件已经结案 直接跳转页面 不做处理
        window.location.href = html + '?caseId=' + caseId;
    }
}

/**
 * 提交审批
 * @param caseStatus 案件所处环节
 */
function submitApproval(caseStatus) {
    if (status != 0) {
        changeDocStatus();//改变已录入的文书状态变为未审批
        $.ajax({
            url: "/luzheng/app/case!updateApprovalByMe",
            data: {
                "caseId": caseId,
                "caseDocId": caseDocId,
                "caseStatus": caseStatus,
                "status": 2//文书为未审批
            },
            success: function () {
                window.history.go(0);//刷新页面 相当于客户端按F5
                alert("提交审批成功");
                //do SomeThing 提交按钮变灰 文书不可编辑
            }
        });
    } else {
        switch (parseInt(caseStatus)) {
            case 1 :
                alert("必须填写文书:《立案审批表》");
                break;
            case 2:
                alert("必须填写文书:《案件处理意见书》")
                break;
            case 6:
            case 5:
                alert("必须填写文书:《处罚结案报告》")
                break;

        }
    }

}

/**
 * 设置录入的文书状态由已录入变为未审批
 */
function changeDocStatus() {
    $("tr").each(function () {
        var caseDocId = $(this).children().eq(1).attr("caseDocId");
        if (caseDocId != undefined) {
            $.ajax({
                url: "/luzheng/app/case!updateCaseDocument",
                data: {
                    "caseDocument.id": caseDocId,//文书id
                    "caseDocument.docStatus": 2//未审批状态码为2
                }
            });
        }
    });
}

/**
 * 提交审批后但未结案 设置提交审批按钮和重置按钮不可用 文书只能查看
 */
function setApprovalAndResetButtonUnUse() {
    setOnlyReadDoc();
    $("#submitApproval").attr("disabled", "disabled").attr("onclick", "");
    $("#resetButton").attr("disabled", "disabled");
}

/**
 * 结案后 设置提交审批,重置,结案,移交四个按钮都不可用 文书只能查看
 */
function setAllButtonUnUse() {
    setApprovalAndResetButtonUnUse();
    $("#endCase").attr('disabled', 'disabled').attr("onclick", "");
    $("#handOver").attr('disabled', 'disabled').attr("onclick", "");
}

/**
 * 结案或提交审批后 都不能操作文书 只能查看
 */
function setOnlyReadDoc() {
    $("tr").each(function () {
        $(this).find("a :first").remove();
    });
}

/**
 * 两个人审批都同意 显示下一步按钮
 */
function setBothAgree(){
    $(".commitApprove").hide();
    $(".endCase").hide();
    $(".nextOne").show();

}

/**
 * 有人不同意 显示结案按钮
 */
function setNotAgree(){
    $(".commitApprove").hide();
    $(".endCase").show();
    $(".nextOne").hide();
}
