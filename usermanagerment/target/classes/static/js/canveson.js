window.onload = function(){
	var oC = document.getElementById('c1');
    var oGC = oC.getContext('2d');  

	oC.onmousedown = function(ev){
		var ev = ev || window.event;
		oGC.beginPath();
		
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
		if (userAgent.indexOf("Firefox") > -1) {
           oGC.moveTo(ev.layerX-oC.offsetLeft,ev.layerY-oC.offsetLeft);
	    } //判断是否Firefox浏览器
	    if (userAgent.indexOf("Chrome") > -1){
	        oGC.moveTo(ev.offsetX,ev.offsetY);
	     }//判断是否Chrome浏览器
	    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
	       oGC.moveTo(ev.x-oC.offsetLeft,ev.y-oC.offsetLeft);
	    }; //判断是否IE浏览器
	    
		document.onmousemove = function(ev){
			var ev = ev || window.event;
			if (userAgent.indexOf("Firefox") > -1) {
		      oGC.lineTo(ev.layerX-oC.offsetLeft,ev.layerY-oC.offsetTop);
		    } //判断是否Firefox浏览器
		    if (userAgent.indexOf("Chrome") > -1){
		        oGC.lineTo(ev.offsetX,ev.offsetY);
		     }//判断是否Chrome浏览器
		    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		       oGC.lineTo(ev.x-oC.offsetLeft,ev.y-oC.offsetTop);
		    }; //判断是否IE浏览器
			oGC.stroke();
		};
		oGC.closePath();
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		};
	};
	
};
function sign(){
	 
	    $("#myModal").modal("show"); 
	        var oC = document.getElementById('c1');
	        var ctx = oC.getContext('2d');
	        ctx.setTransform(1, 0, 0, 1, 0, 0);
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		}
	    

	
//提交形成图片显示在页面上面
function back(){        
          var canvas = document.getElementById("c1");
          var context = canvas.getContext("2d");
          
          $("#inputDiv").hide();
          $("#imghide").show();
           var data=canvas.toDataURL("image/png");
          $("#signDiv img").attr("src",data);
          //$("#addhide").hide();
          $("#myModal").modal("hide");
           var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
            var divT=document.getElementById("currenttime");
             divT.innerHTML= "签名时间："+currentdate;
}


