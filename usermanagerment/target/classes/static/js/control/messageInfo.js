/**
 * Created by BuDD on 2017/11/10.
 */
/**
 * 展示用户名
 */
function showName() {
    var userName = $.cookie('uName');
    $(".userName").text(userName);
}

/**
 * 展示头像
 */
function showImage() {
    var imgUrl = $.cookie('imgUrl');
    if (imgUrl != undefined && imgUrl.length > 0) {
        $(".touXiang").attr("src", "/usermanagerment" + imgUrl);
    } else {
        $(".touXiang").attr("src", "/img/userpic.png");
    }
}

/**
 * 跳转到个人资料
 */
function showMessage() {
    var id = $.cookie('id');
    window.location.href = "/perdata";
}

/**
 * 跳转到重置密码
 */
function showResetPassword() {
    var id = $.cookie('id');
    window.location.href = "/pwdreset";
}

/**
 * 用户注销
 */
function logOut() {
    $.ajax({
        url: "/logout",
        success: function (res) {
            if (res.result == "ok") {
                $.cookie('orgSmallAuth',null,{path:'/'});
                $.cookie('authBigCode',null,{path:'/'});
                $.cookie('authSmallCode',null,{path:'/'});
                window.location.href = "login";
            }
        }
    })
}

function showOperat(){
    var vt = $.cookie('userType');
    // 超级管理员
    if (vt == "0") {
        $("#organization").show();
        $("#case").show();
        $("#information").show();
        $("#satistical").show();
        $("#SMS").show();
    }
    // 执法单位
    if (vt == "1") {
        $("#organization").show();
        $("#case").hide();
        $("#information").hide();
        $("#satistical").hide();
        $("#SMS").hide();
    }
    // 执法人员
    if (vt == "2") {
        $("#organization").hide();
        $("#case").show();
        $("#information").show();
        $("#satistical").show();
        $("#SMS").show();
    }
}

/**
 * 頁面加載
 */
$(function () {
    var vt2 = $.cookie('userType');
    if (vt2 == "1") {
        $("ul.tabs-nav").children("li:last").children("a:first").attr("href", "indexedit.html?id=" + $.cookie('officeId'));
    }
    if (vt2 == "2") {
        $.ajax({
            url: "/luzheng/app/common!readOfficerByCardId",
            data: {
                cardId: $.cookie('cardId')
            },
            success: function (data) {
                var obj = JSON.parse(data)
                if (obj.status == "success") {
                    $("ul.tabs-nav").children("li:last").children("a:first").attr("href", "index-officer-edit.html?id=" + obj.message.id);
                }
            }
        })
    }
})

//案件列表 案件编号处理
function wordFormatter(value,row){
    return row.word+" "+row.number;
}


/**
 *  行政处罚案由一级下拉框改变赋值二级下拉框
 * @param id 一级下拉框id
 */
function setChildren(id) {
    $("#causeActionId1").hide();
    if($("#"+id).val() != -1){
        if($("#"+id).val() == 0){
            $("#childrenSelect").empty();//清空上一个分类下的二级菜单
            $("#childrenSelect").append("<option value='-1'>请选择二级分类</option>");
            $("#childrenSelect").append("<option value='0'>许可相关处罚</option>");
            $("#childrenSelect").append("<option value='1'>经营管理违规</option>");
            $("#childrenSelect").append("<option value='2'>国际道路运输</option>");
            $("#childrenSelect").append("<option value='3'>巡游出租管理</option>");
            $("#childrenSelect").append("<option value='4'>驾驶员培训</option>");
            $("#childrenSelect").append("<option value='5'>城市交通管理</option>");
        }else if($("#"+id).val() == 1){
            $("#childrenSelect").empty();//清空上一个分类下的二级菜单
            $("#childrenSelect").append("<option value='-1'>请选择二级分类</option>");
            $("#childrenSelect").append("<option value='0'>未经许可处罚</option>");
            $("#childrenSelect").append("<option value='1'>违反法律法规处罚</option>");
        }
        $("#childrenSelect").show();
        $("#causeActionId").hide();
    }else{
        $("#childrenSelect").hide();
        $("#causeActionId").hide();
    }
}

