$(function() {
	var base = "/conference";
	$("table tbody").html("");
	$("table tbody").append("<tr class='first'><td>姓名</td><td>单位及职务</td><td>手机号</td></tr>");
	
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"11"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">VIP-官员</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"12"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">VIP-专家</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"13"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">VIP-企业</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"14"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">VIP-机构</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"21"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">协助人员-VIP联系人</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"22"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">协助人员-接待联系人</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"23"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">协助人员-会务联系人</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"31"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">工作人员-执委会</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	/*$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"32"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">工作人员-分会</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td>"+user.name+"</td><td>"+user.office+user.job+"</td><td>"+user.phone+"</td></tr>");
    			}
    			}
    		}
    	}
	});*/
	var categoryList=new Array();
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readCategoryList",
    	//data:{"user.userType":"32"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			for (i=0;i<data.message.length;i++){
    				var obj=data.message[i];
    				categoryList.push(obj);
    			}
    		}
    	}
	});
	for (i=0;i<categoryList.length;i++){
		var obj=categoryList[i];
		$.ajax({
	    	type:"post",
	    	url:base+"/app/common!readBranchUserList",
	    	data:{"user.category":obj.id},
	    	async:false,
	    	success:function(data){
	    		data=JSON.parse(data);
	    		console.log(data);
	    		if (data.status=="success"){
	    			if (data.message.length>0){
	    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">工作人员-'+obj.name+'</td></tr>');
	    			for (j=0;j<data.message.length;j++){
	    				var user=data.message[j];
	    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
	    			}
	    			}
	    		}
	    	}
		});
	}
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"41"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">参会人员-参展联系人</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readUserList",
    	data:{"user.userType":"42"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message.length>0){
    				$("table tbody").append('<tr class="tr-bg"><td colspan="4" style="text-align: center;">参会人员-参会联系人</td></tr>');
    			for (i=0;i<data.message.length;i++){
    				var user=data.message[i];
    				$("table tbody").append("<tr><td style='text-decoration:underline;color:#08c' onclick='lookUser("+user.id+")'>"+user.name+"</td><td>"+user.office+user.job+"</td><td><a href='tel:"+user.phone+"'>"+user.phone+"</a></td></tr>");
    			}
    			}
    		}
    	}
	});
});

function lookUser(userid){
	window.open("detailInfo.html?userid="+userid);
}
