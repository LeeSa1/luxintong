document.writeln("<header>");
document.writeln("			<span><img src=\'img/timg.png\' />医疗知识数据云</span>");
document.writeln("			<ul>");
document.writeln("				<a href=\'study.html\'>");
document.writeln("					<li>");
document.writeln("						学习");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'case.html\'>");
document.writeln("					<li>");
document.writeln("						病例");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'document.html\'>");
document.writeln("					<li>");
document.writeln("						文献");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'guide.html\'>");
document.writeln("					<li>");
document.writeln("						指南");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'resource.html\'>");
document.writeln("					<li>");
document.writeln("						资源");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'drug.html\'>");
document.writeln("					<li>");
document.writeln("						药品");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'policy.html\'>");
document.writeln("					<li>");
document.writeln("						政策");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("				<a href=\'exam.html\'>");
document.writeln("					<li>");
document.writeln("						考试");
document.writeln("					</li>");
document.writeln("				</a>");
document.writeln("			</ul>");
document.writeln("			<div class=\'user-box\'>");
document.writeln("				<img src=\'img/user.png\' />");
document.writeln("				<div>");
document.writeln("					<ul>");
document.writeln("						<a href=\'javascript:void(0)\' onClick=\'login();\'>");
document.writeln("							<li><i class=\'glyphicon glyphicon-user\'></i>个人中心</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'login.html'>");
document.writeln("							<li><i class=\'glyphicon glyphicon-off\'></i>退出</li>");
document.writeln("						</a>");
document.writeln("					</ul>");
document.writeln("");
document.writeln("					<ul>");
document.writeln("						<a href=\'study.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;学习</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'case.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;病例</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'document.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;文献</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'guide.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;指南</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'resource.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;资源</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'drug.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;药品</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'policy.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;政策</li>");
document.writeln("						</a>");
document.writeln("						<a href=\'exam.html\'>");
document.writeln("							<li>> &nbsp;&nbsp;&nbsp;考试</li>");
document.writeln("						</a>");
document.writeln("					</ul>");
document.writeln("				</div>");
document.writeln("			</div>");
document.writeln("		</header>");
document.writeln("");
document.writeln("		<div id=\'search-box\'>");
document.writeln("			<div class=\'search\'>");
document.writeln("				<span>云搜索</span>");
document.writeln("				<div>");
document.writeln("					<form action=\'\' method=\'post\'>");
document.writeln("						<input type=\'text\' name=\'\' id=\'search-input\' value=\'\' placeholder=\'请输入标题关键字\'/>");
document.writeln("						<input type=\'button\' id=\'search-button\' onClick=\'searchText();\'/>");
document.writeln("					</form>");
document.writeln("					<a href=\'javascript:void(0)\'>医学词典</a>");
document.writeln("				</div>");
document.writeln("			</div>");
document.writeln("		</div>");
document.writeln("		<div class=\'dict-box\' style=\'display:none\'>");
document.writeln("		    <div id=\'search_form\'>");
document.writeln("		        <form action=\'\' method=\'post\'>");
document.writeln("				    <input type=\'text\' name=\'\' id=\'search-input\' value=\'\' placeholder=\'请输入关键字\'/>");
document.writeln("					<input type=\'button\' id=\'search-button\' value=\'查询\' onClick=\'msearch();\'/>");
document.writeln("				</form>");
document.writeln("		    </div>");
document.writeln("		    <div id=\'dict_body\'>");
document.writeln("		    </div>");
document.writeln("		</div>");
document.writeln("		<div class=\'pass-box\' style=\'display:none\'>");
document.writeln("		    <div id=\'pass_form\'>");
document.writeln("		        <form action=\'\' method=\'post\'>");
document.writeln("				    <span>请输入密码：</span>");
document.writeln("				    <div><input type=\'password\' name=\'\' id=\'password\' value=\'\'/></div> ");
document.writeln("					<div><input type=\'button\' id=\'\' value=\'确定\' onClick=\'doLogin();\' class=\'btn btn-info\'/>");
document.writeln("					<input type=\'button\' id=\'\' value=\'取消\' onClick=\'cancelLogin();\' class=\'btn btn-danger\'/></div>");
document.writeln("				</form>");
document.writeln("		    </div>");
document.writeln("		</div>");

$(function() {

		$(document).on('click', function(e) {
			$('.user-box>div').fadeOut();
			if (e.target!=$('.dict-box')&&$(e.target).parents('.dict-box').length==0){
				$('.dict-box').fadeOut();
			}
		})

		$('.user-box img').on('click', function(e) {
			if($(window).width() > 991) {
				$(this).next().fadeIn();
				e.stopPropagation();
			} else {
				$(this).next().slideDown();
				e.stopPropagation();
			}
		})
		
		$('#search-box .search>div a').on('click', function(e) {
			$('.dict-box').fadeIn();
			e.stopPropagation();
		})
		
		$('.dict-box #dict_body').on('click','ul>li a',function(e){
			msearch($(this).attr('data-name'));
		});


})

function msearch(s){
	if (!s){
		if ($('.dict-box #search_form #search-input').val()!=''){
			s = $('.dict-box #search_form #search-input').val();
		}else{
		    alert('请输入要查询的词语！');
			return false;
	    }
	}
	//if ($('.dict-box #search_form #search-input').val()!=''){
		var base = "/hoslibrary";
	    var token = $.cookie('token');
        var id = $.cookie('id');
		console.log(s);
		$.post(base+"/app/common!getDict",{
		       "user.token":token,
			   "user.id":id,
			   name:s
		   },function(data){
			   $('.dict-box #search_form #search-input').val('');
			   console.log(data);
			   data=JSON.parse(data);
			   console.log(data.message);
			   if (data.status == 'success'){
			   var html = $.parseHTML(data.message);
			   console.log(html);
			   var len = $(html).find("#dict_body>.word").length;
			   console.log(len);
			   $("#dict_body").html('');
			   if (len > 0){
				   console.log(len);
				   for (i=0;i<len;i++){
					   var wordname = $(html).find("#dict_body>.word").eq(i).find(".word_name").text();
					   var word = '<div class="word">'
					            + '<div class="word_name">'+wordname+'</div>'
							    + '<div class="block">'
					   var dictlen = $(html).find("#dict_body>.word").eq(i).find(".block").length;
					   for (j=0;j<dictlen;j++){
						   word += $(html).find("#dict_body>.word").eq(i).find(".block").eq(j).html();
					   }
					   word += '</div>';
					   $("#dict_body").append(word);
				   }
				   $("#dict_body").append($(html).find("#dict_body h3"));
				   var linklen = $(html).find("#dict_body ul>li").length;
				   if (linklen > 0){
				       var wordul = '<ul>';
					   for (i=0;i<linklen;i++){
						   var keyword = decodeURI($(html).find("#dict_body ul>li").eq(i).find('a').attr('href'));
						   keyword = keyword.replace('/w/','');
						   var slink = $(html).find("#dict_body ul>li").eq(i).text();
						   if ($(html).find("#dict_body ul>li").eq(i).find('a').length>0){
						       wordul += '<li><a rel="nofollow" href="javascript:void(0)" data-name="'+keyword+'">'+slink+'</a></li>';
						   }else{
							   wordul += '<li>'+slink+'</li>';
						   }
					   }
					   wordul += '</ul>'
					   $("#dict_body").append(wordul);
				   }
			   }else{
				  $("#dict_body").append('<span style="font-size:2em;">你搜索的词语医学词典里找不到！</span>'); 
			   }
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	//}
}

function login(){
	var base = "/hoslibrary";
	var username = $.cookie('username')
	var pass = $.cookie('password');
	if (!username) username='';
	if(!pass) pass = '';
	console.log(username);
	if (username != '' && pass != ''){
		window.location.href = base+"/sysmgr/login!doLogin?username="+username+"&password="+pass;
	}else{
		if (confirm("您未设置记住密码，需在此处输入密码才能登录个人中心，确定吗？")){
			$(".pass-box").show();
		}
	}
}

function doLogin(){
	var base = "/hoslibrary";
	var pass = $(".pass-box #password").val();
	var token = $.cookie('token');
    var id = $.cookie('id');
	$.post(base+"/app/common!readUser",{
		       "user.token":token,
			   "user.id":id,
		   },function(data){
			   data = JSON.parse(data);
			   console.log(data);
			   if (data.status == 'success'){
				   var username = data.message.login;
				   window.location.href = base+"/sysmgr/login!doLogin?username="+username+"&password="+pass;
			   }else{
				   alert(data.message);
			   }
		   });
}

function cancelLogin(){
	$(".pass-box").hide();
}