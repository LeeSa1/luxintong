//打印页面当事人信息加载
function getLitigant() {
	var userId = $.cookie('id');
	var caseId = GetQueryString("caseId");
	// 当事人信息
	$.ajax({
		url : "/luzheng/app/case!readCaseInfo",
		data : {
			"id" : caseId
		},
		success : function(data) {
			data = JSON.parse(data);
			$("#litigantName").val(data.message.litigantName);
			$("#litigantName").text(data.message.litigantName);
			$(".litigantName").val(data.message.litigantName);
			$("#litigantAddress").text(data.message.litigantAddress);
			$("#officer1Id").text(data.message.officer1Id);
			$("#officer1IdCard").val(data.message.officer1IdCard);
			$("#officer2Id").text(data.message.officer2Id);
			$("#officer2IdCard").val(data.message.officer2IdCard);
		}
	});
}

