var caseId = GetQueryString("caseId");//案件id
var docType = GetQueryString("docType");//该文书对应文书列表的id
var userId = $.cookie("id");//登录人员的id
var caseDocId;//已录入文书的id


/**
 * 判断案件的类型 精简模式选意见 流程审批选审批人
 */
function checkCaseType() {
    $.ajax({
        url: "/luzheng/app/case!readCaseInfo",
        data: {"id": caseId},
        success: function (data) {
            data = JSON.parse(data);
            if (data.message != "") {
                if (data.message.caseType1 == 0) {
                    $(".process").remove();
                    getOpinion();//精简模式初始化意见
                }
                if (data.message.caseType1 == 1) {
                    $(".simple").remove();
                    if (!checkAddOrUpdate()) {
                        getApprovePeople();//文书未录入 流程审批初始化审批人
                    }
                    else {//文书已经录入了 读取录入的审批人信息
                        getApprove();
                    }
                }
            }
        }
    });
}

/**
 * 读取审批信息
 */
function getApprove() {
    $.ajax({
        url: "/luzheng/app/case!getApproveByCaseDocIdAndCaseId",
        data: {
            "caseId": caseId,
            "caseDocId": caseDocId
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.message != "") {
                $("#approvePerson11").val(data.message[0].name);
                $("#approvePerson1CardId").val(data.message[0].cardId);
                $("#approvePerson21").val(data.message[1].name);
                $("#approvePerson2CardId").val(data.message[1].cardId);
            }
        }
    });
}

/**
 * 判断文书是否已经录入 录入返回true 没录入返回false
 * @returns {boolean}
 */
function checkAddOrUpdate() {
    var addOrUpdate = false;//文书是否录入标志
    $.ajax({
        url: "/luzheng/app/case!readCaseDocumentByCaseIdAndDocType",
        data: {
            "caseDocument.caseId": caseId,
            "caseDocument.docType": docType
        },
        async: false,
        success: function (data) {
            data = JSON.parse(data);
            if (data.message != null) {
                console.log("已录入");
                caseDocId = data.message.id;
                addOrUpdate = true;//文书已录入标志
                changeApproveToInput();//文书已经录入 审批人变为input框
            }
        }
    });
    return addOrUpdate;
}

/**
 * 得到审批人
 */
function getApprovePeople() {
    $
        .ajax({
            url: "/luzheng/app/common!findAllOfficer",
            data: {
                "user.id": userId
            },
            success: function (data) {
                data = JSON.parse(data);
                //初始化审批人信息
                $("#approvePerson1 option").remove();
                $("#approvePerson2 option").remove();
                $("#approvePerson1").append(
                    '<option value="index">--请选择--</option>');
                $("#approvePerson2").append(
                    '<option value="index">--请选择--</option>');
                for (var i = 0; i < data.message.length; i++) {
                    $("#approvePerson1")
                        .append(
                            '<option value="' + data.message[i].userName + '" cardId="' + data.message[i].cardId + '">'
                            + data.message[i].userName
                            + '</option>');
                    $("#approvePerson2")
                        .append(
                            '<option value="' + data.message[i].userName + '" cardId="' + data.message[i].cardId + '">'
                            + data.message[i].userName
                            + '</option>');
                }
                $('#approvePerson1,#approvePerson2').searchableSelect();
            }
        });
}

/**
 * 审批人改变时 改变相应的执法证号
 * @param id 下拉列表的id
 */
function funOfficer(id) {
    if ($("#" + id).val() != "index") {
        $("#" + id + "CardId").val($("#" + id + " option:selected").attr("cardId"));
    } else {
        $("#" + id + "CardId").val("");
    }
}

/**
 * 更新状态进入时 审批人下拉列表隐藏 显示input框
 */
function changeApproveToInput() {
    $("#approvePerson1").attr("disabled", true).hide();
    $("#approvePerson2").attr("disabled", true).hide();
    $("#approvePerson11").show();
    $("#approvePerson21").show();
}