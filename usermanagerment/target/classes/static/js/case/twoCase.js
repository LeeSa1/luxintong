/**
 * Created by BuDD on 2018/1/10.
 */

/**
 * 初始化方法
 * @param type 文书类型的数组
 */
function init(type) {
    var typeStr = type.toString();
    getCaseDocumentStatus(GetQueryString('caseId'), typeStr);
    readCaseInfoByNeed(GetQueryString('caseId'));
}

/**
 * 文书是否结案
 */
function setCaseDocmentShow() {
    $("tr").each(function () {
        $(this).children('td:last').children('a:first').hide();
    });
    $("#resetDiv").hide();
    $("#approveSubmit").hide();
    $("#settleSubmit").hide();
}
/**
 * 判断是否需要申辩/鉴定/听证/回避/委托,并且判断是否结案
 * @param id
 */
function readCaseInfoByNeed(id) {
    $.ajax({
        url: "/luzheng/app/pb!readCaseInfo",
        data: {
            "id": id,
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                var obj = JSON.parse(data);
                //是否需要陈述申辩
                if (obj.message.requireStatementArgued == 1) {
                    $("#defendSel").val(1);
                    defend();
                }
                //是否需要鉴定
                if (obj.message.needIdentification == 1) {
                    $("#identSel").val(1);
                    ident();
                }
                //是否需要听证
                if (obj.message.needHearing == 1) {
                    $("#hearSel").val(1);
                    hear();
                }
                //是否需要回避
                if (obj.message.applyHearingWithdrawal == 1) {
                    $("#avoidSel").val(1);
                    avoid();
                }
                //是否需要委托人
                if (obj.message.entrustOthersJoin == 1) {
                    $("#joinSel").val(1);
                    join();
                }
                //案件申请是否通过,强制案件第一part的时候
                if (obj.message.applyCompulsoryExecution == 1) {
                    $("#applyStatus").show();
                    $("#disSel").val(1);
                    help();
                    //是否需要延长扣押期限
                    if (obj.message.havaIllegal == 1) {
                        $("#identSel").val(1);
                        ident();
                    }
                    //是否履行义务        /没有履行为1
                    if (obj.message.administrativePenalty == 1) {
                        $("#disSel").val(1);
                        setNotDutyProm();
                        //强制是否继续执行  审批同意
                        if (obj.message.doCompulsoryMeasures == 1) {
                            $("#identSel").val(1);
                            $("#applyStatus2").show();
                            //符合代履行
                            if (obj.message.majorCases == 1) {
                                $("#actionSel").val(1);
                                actionx();
                            }
                            //申请法院强制
                            if (obj.message.separatePayPunishMoney == 1) {
                                $("#illegalSel").val(1);
                                illegal();
                            }
                        }
                    }

                }
                //案件是否结案
                if (obj.message.status == -1) {
                    setCaseDocmentShow();
                    $("#fileDiv").show();
                }
            }
        }
    });
}

/**
 * 获取文书的状态
 * @param id  案件id
 * @param type 文书类型的数组
 */
function getCaseDocumentStatus(id, type) {
    $.ajax({
        url: "/luzheng/app/flow!readCaseDocByCaseId",
        method: "post",
        data: {
            "id": id,
            "docType": type
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                var obj = JSON.parse(data);
                if (obj.status === "success") {
                    for (var i = 0; i < obj.message.length; i++) {
                        $(".caseDocumentStatus").each(function () {
                            if (obj.message[i].docType == this.id) {
                                chooseDocStatus(obj.message[i].docStatus, this);
                            }
                        });
                    }
                }
            }
        }
    })
}

function chooseDocStatus(status, ob) {
    if (status == 1) {
        $(ob).text("已录入");
        if(GetQueryString('approveId') != undefined) {
            $("#showOprate").hide();
        }
    } else if (status == 2) {
        $(ob).text("未审批");
        setCaseDocmentShow();
        ifapproveHtml();
    } else if (status == 3) {
        $(ob).text("审批通过");
        $("#approveSubmit").hide();
        if(window.location.href.indexOf("qz") >= 0) {
            return;
        }
        setCaseDocmentShow();
    } else if (status == 4) {
        $(ob).text("审批未通过");
        $("#approveSubmit").hide();
        setCaseDocmentShow();
    } else if (stauts == 5) {
        $(ob).text("保留意见");
        $("#approveSubmit").hide();
        setCaseDocmentShow();
    }
    else {
        $(ob).text("未录入");
    }
}

/**
 * 判断审批人所在的环节
 * 1    2   5   4
 */
function chooseCaseStatus() {
    var status = new Array(1, 2, 3, 4, 5, 6, 7);
    var caseStatus = GetQueryString('caseStatus');
    var final = $("#hideStatus").val();
    status[caseStatus - 1] = 0;
    if(caseStatus == final) {
        for (var i = 0; i < status.length; i++) {
            if (status[i] != 0) {
                $("#approveHtml").hide();
            } else {
                $("#approveHtml").show();
                break;
            }
        }
    }else{
        $("#approveHtml").hide();
    }
}

/**
 * 点击结案跳转的页面
 */
function finsh() {
    window.location.href = "pb-normfile.html?value=2&caseId=" + GetQueryString('caseId');
}

//需要申辩
function defend() {
    Apply($("#defendSel").val());
}

function Apply(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#defendDiv").hide();
    }
    else if (id == 1) {
        $("#defendDiv").show();
    }
}

//需要鉴定
function ident() {
    Apply2($("#identSel").val())
}

function Apply2(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#identDiv").hide();
    } else if (id == 1) {
        $("#identDiv").show();
    }
}

//需要听证
function hear() {
    Apply3($("#hearSel").val())
}

function Apply3(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#hearDiv").hide();
    }
    else if (id == 1) {
        $("#hearDiv").show();
    }
}

//回避
function avoid() {
    Apply4($("#avoidSel").val())
}

function Apply4(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#avoidDiv").hide();
    }
    else if (id == 1) {
        $("#avoidDiv").show();
    }
}

//委托
function join() {
    Apply5($("#joinSel").val())
}

function Apply5(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#joinDiv").hide();
    }
    else if (id == 1) {
        $("#joinDiv").show();
    }
}

//需要审批同意
function help() {
    Apply6($("#disSel").val())
}

function Apply6(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#helpDiv").hide();
    }
    else if (id == 1) {
        $("#helpDiv").show();
    }
}

//未履行义务
function setNotDutyProm() {
    Apply7($("#disSel").val())
}

function Apply7(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#NotDutyProm").hide();
    }
    else if (id == 1) {
        $("#NotDutyProm").show();
    }
}

//符合代履行
function actionx() {
    Apply8($("#actionSel").val())
}

function Apply8(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#actionDiv").hide();
    }
    else if (id == 1) {
        $("#actionDiv").show();
    }
}

//是否申请法院
function illegal() {
    Apply9($("#illegalSel").val());
}

function Apply9(id) {
    id = parseInt(id);
    if (id == 0) {
        $("#illegalDiv").hide();
    }
    else if (id == 1) {
        $("#illegalDiv").show();
    }
}

/**
 * 导航栏跳转
 * @param caseId 案件的id
 * @param address 跳转的路径
 */
function navigatinBarHref(caseId, address) {
    if (GetQueryString('approveId') == undefined) {
        window.location.href = "/luzheng/html/" + address + "?caseId=" + caseId;
        return;
    }
    window.location.href = "/luzheng/html/" + address + "?caseId=" + caseId + "&approveId=" + GetQueryString('approveId') + "&caseStatus="+GetQueryString('caseStatus');
}

/**
 * 执法人员提交审批
 * @param caseId 案件的id
 * @param case_status 案件所处的环节
 * @param docType   审批人的文书
 * @param status    审批状态
 * @param docTypes  要改变的文书状态
 */
function setApproveToSubmit(caseId, case_status, docType, status, docTypes) {
    $.ajax({
        url: "/luzheng/app/pb!updateApprove",
        method: "post",
        data: {
            "approve.caseId": caseId,
            "approve.caseDocId": docType,
            "approve.caseStatus": case_status,
            "approve.status": status
        },
        success: function () {
            changeDocStatus(caseId, docTypes.toString(), status);
            alert("提交审批成功");
            window.history.go(0);
        }
    })
}

/**
 * 改变文书的状态
 * @param caseId 案件的id
 * @param docTypes 文书的类型,字符串
 * @param status 要改变成的状态
 */
function changeDocStatus(caseId, docTypes, status) {
    $.ajax({
        url: "/luzheng/app/flow!updateCaseDocumentByApprove",
        data: {
            "id": caseId,
            "docType": docTypes,
            "status": status
        }
    })
}

/**
 * 跳转到审批页面
 * @param caseId
 */
function forWordHtmlBySP(docType) {
    window.location.href = "case-appmine.html?id=" + GetQueryString('approveId') + "&docType=" + docType.toString();
}

function ifapproveHtml() {
    var approveId = GetQueryString('approveId');
    if (approveId != undefined) {
        $("#approveHtml").show();
        $("#showOprate").hide();
        chooseCaseStatus();
    }
}