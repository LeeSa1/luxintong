/**
 * Created by BuDD on 2017/11/16.
 */
//获取案件文书编号
function getDocumentNo() {
    $.ajax({
        url: "/luzheng/app/common!getAllDocNo",
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#selectDocNO").append('<option value="' + obj.message[i].docNo + '" sds="' + obj.message[i].id + '">' + obj.message[i].docNo + '</option>');
                }
                $("#selectDocNO").searchableSelect();
            }
        },
        error: function () {
            alert("请求错误！！！请重试");
        }
    })
}

//获取执法人员
function getOfficer() {
    $.ajax({
        url: "/luzheng/app/common!readAllOfficer",
        async : false,
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#selectOfficerFirst").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '" phone="' + obj.message[i].phone + '" cardId="' + obj.message[i].cardId + '">' + obj.message[i].userName + '</option>');
                    $("#selectOfficerSecond").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '" cardId="' + obj.message[i].cardId + '">' + obj.message[i].userName + '</option>');
                    $("#selectOfficerThird").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '">' + obj.message[i].userName + '</option>');
                    $("#selectOfficerForth").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '">' + obj.message[i].userName + '</option>');
                    $("#selectOfficerFifth").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '">' + obj.message[i].userName + '</option>');
                    $("#selectApprovePeople").append('<option value="' + obj.message[i].userName + '" sds ="' + obj.message[i].id + '" cardId="' + obj.message[i].cardId + '">' + obj.message[i].userName + '</option>');
                }
                $("#selectOfficerFirst").searchableSelect();
                $("#selectOfficerSecond").searchableSelect();
                $("#selectOfficerThird").searchableSelect();
                $("#selectOfficerForth").searchableSelect();
                $("#selectOfficerFifth").searchableSelect();
                $("#selectApprovePeople").searchableSelect();
            }
        },
        error: function () {
            alert("请求失败！！！请重试");
        }
    })
}

function getCauseAction() {
    $.ajax({
        url: "/luzheng/app/common!readAllcauseAction",
        success: function (data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#resSel").append('<option value="' + obj.message[i].caseReason + '" sds="' + obj.message[i].punishBasis + '">' + obj.message[i].caseReason + '</option>');
                    $("#selTexta").append('<option value="' + obj.message[i].caseReason + '" sds="' + obj.message[i].punishBasis + '">' + obj.message[i].caseReason + '</option>');
                }
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求错误！！！请重试");
        }
    })
}

/**
 * 获取配补偿案由
 */
function getCompensationAction() {
    $.ajax({
        url: "/luzheng/app/pb!readCompensationAction",
        success: function (data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#resSel").append('<option value="' + obj.message[i].caseReason + '" sds="' + obj.message[i].punishBasis + '">' + obj.message[i].caseReason + '</option>');
                }
                $("#resSel").searchableSelect();
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求错误！！！请重试");
        }
    })
}




function getLitigantByCaseInfo(id) {
    $.ajax({
        url: "/luzheng/app/pb!readCaseInfo",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj.status == "success") {
                $("#orgName").val(obj.message.orgName);
                $("#LitigantName").val(obj.message.litigantName);
                $("#LitigantWorkUnit").val(obj.message.litigantWorkUnit);
                $("#LitigantOccupation").val(obj.message.litigantOccupation);
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求错误！！！请重试");
        }
    })
}

//获取执法机构
function getOrg() {
    $.ajax({
        url: "/luzheng/app/common!findAllOrgInfo",
        async:false,
        data: {
            "user.id": $.cookie('id')
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#selectOrg").append('<option value="' + obj.message[i].org + '" sds ="' + obj.message[i].id + '">' + obj.message[i].org + '</option>');
                    $("#listenEvidenceEnforceLaw").append('<option value="' + obj.message[i].address + '" sds ="' + obj.message[i].id + '">' + obj.message[i].address + '</option>');
                    $("#applyAvoidFzjg").append('<option value="' + obj.message[i].org + '" sds ="' + obj.message[i].id + '">' + obj.message[i].org + '</option>');
                }
                $("#selectOrg").searchableSelect();
            }
        }
    })
}

//根据类型获取当事人
function getLitigant(ob) {
    var litigantType = $(ob).find("option:selected").val();
    $.ajax({
        url: "/luzheng/app/common!readAllLitigantByType",
        data: {
            "litigant.litigantType": litigantType,
            "id": $.cookie('officeId')
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                if (litigantType == 0) {
                    for (var i = 0; i < obj.message.length; i++) {
                        $("#selectLitigant").append('<option value="' + obj.message[i].name + '" sds ="' + obj.message[i].id + '">' + obj.message[i].name + '</option>');
                    }
                    $("#selectLitigant").searchableSelect();

                } else if (litigantType == 1) {
                    for (var i = 0; i < obj.message.length; i++) {
                        $("#selectLitigant2").append('<option value="' + obj.message[i].name + '" sds ="' + obj.message[i].id + '">' + obj.message[i].name + '</option>');
                    }
                    $("#selectLitigant2").searchableSelect();
                }
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求失败！！！请重试");
        }
    })
}

function getAgent() {
    $.ajax({
        url: "/luzheng/app/common!findAllAgent",
        success: function (data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#FirstAgent").append('<option value="' + obj.message[i].agentName + '">' + obj.message[i].agentName + '</option>');
                }
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求错误！！！请重试");
        }
    });
}

//获取编号
function getNumber(ob) {
    var id = ob.element.find('option:selected').attr('sds')    // alert(id);
    $.ajax({
        url: "/luzheng/app/common!readDocumentNumber",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                if (obj.message.length < 4) {
                    var length = obj.message.length;
                    var temp = "0";
                    for (var i = 0; i < 4 - length; i++) {
                        obj.message = temp + obj.message;
                    }
                    $("#doc_num").val(obj.message);
                } else {
                    $("#doc_num").val(obj.message);
                }
                $("#doc_id").val(id);
            }
        },
        error: function () {
            alert("请求失败！！！");
        }
    })
}

//获取审批意见
function getApprOpinion() {
    $.ajax({
        url: "/luzheng/app/common!findAllApprOpinion",
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#selectApprOpinion").append('<option value="' + obj.message[i].opinionDesc + '" sds ="' + obj.message[i].id + '">' + obj.message[i].opinionDesc + '</option>');
                    $("#selText").append('<option value="' + obj.message[i].opinionDesc + '" sds ="' + obj.message[i].id + '">' + obj.message[i].opinionDesc + '</option>');
                    $("#selText2").append('<option value="' + obj.message[i].opinionDesc + '" sds ="' + obj.message[i].id + '">' + obj.message[i].opinionDesc + '</option>');
                    $("#selText3").append('<option value="' + obj.message[i].opinionDesc + '" sds ="' + obj.message[i].id + '">' + obj.message[i].opinionDesc + '</option>');
                }
                $("#selectApprOpinion").searchableSelect();
                $("#selText").searchableSelect();
                $("#selText2").searchableSelect();
                $("#selText3").searchableSelect();
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求失败！！！请重试");
        }
    })
}

//获取案件信息
function getCaseInfo(id) {
    $.ajax({
        url: "/luzheng/app/case!readCaseInfo",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#number").val(obj.message.number);
                $("#caseName").val(obj.message.caseName);
                $("causeDepPunish").val(obj.message.causeDepPunish);
                $("#officerFirst").val(obj.message.officer1Id);
                $("#officerSecond").val(obj.message.officer2Id);
                $("#officerFirstCardId").val(obj.message.officer1IdCard);
                $("#officerSecondCardId").val(obj.message.officer2IdCard);
                $("#causeAction").val(obj.message.causeActionId);
                $("#causeDepPunish").val(obj.message.causeDepPunish);
                if (obj.message.litigantType == 0) {
                    $("#litigantName").val(obj.message.litigantName);
                    $("#litigantSex").val(obj.message.litigantSex == null ? '' : obj.message.litigantSex == '0' ? '女' : '男');
                    $("#litigantAge").val(obj.message.litigantAge);
                    $("#litigantCard").val(obj.message.litigantCard);
                    $("#litigantAddress").val(obj.message.litigantAddress);
                    $("#litigantWork").val(obj.message.litigantWorkUnit);
                    $("#litigantOccupation").val(obj.message.litigantOccupation);
                    $("#litigantPhone").val(obj.message.litigantPhone);
                    $("#litigantNameAddressPostcode").val(obj.message.litigantNameAddressPostcode);
                } else {
                    if (obj.message.caseType == "2") {
                        $("#litigantName").val(obj.message.litigantName);
                        // $("#litigantSex").val(obj.message.litigantSex == null ? '' : obj.message.litigantSex == '0' ? '女' : '男');
                        $("#litigantAddress").val(obj.message.litigantAddress);
                        // $("#litigantWork").val(obj.message.litigantWorkUnit);
                        // $("#litigantOccupation").val(obj.message.litigantOccupation);
                        $("#litigantPhone").val(obj.message.litigantPhone);
                        $("#litigantNameAddressPostcode").val(obj.message.litigantNameAddressPostcode);
                    }
                    $("#litigantName2").val(obj.message.litigantName);
                    $("#litigantAddress2").val(obj.message.litigantAddress);
                    $("#litigantRepresentative").val(obj.message.litigantRepresentative);
                    $("#litigantPhone2").val(obj.message.litigantPhone);
                }
                $("#litigantNameOther").val(obj.message.litigantName);
                $("#litigantRepresentative").val(obj.message.litigantRepresentative);
                $("#agentName").val(obj.message.agentName);
                $("#agentSex").val(obj.message.agentSex == '0' ? '女' : '男');
                $("#agentAge").val(obj.message.agentAge);
                $("#agentOccupation").val(obj.message.agentOccupation);
                $("#agentPhone").val(obj.message.agentPhone);
                $("#agentWork").val(obj.message.agentWorkUnit);
                $("#agentAddress").val(obj.message.agentAddress);
                $("#agentNameAddressPostcode").val(obj.message.agentNameAddressPostcode);
                $("#causeDepPunish").val(obj.message.causeDepPunish);
                $("#applyAvoidReason").val(obj.message.applyAvoidReason);
                $("#applyAvoidTime").val(new Date(obj.message.applyAvoidTime).format("yyyy-MM-dd"));
                $("#applyAvoidBeApplyPersonJob").val(obj.message.applyAvoidBeApplyPersonJob);
                $("#applyAvoidBeApplyPersonIdUnit").val(obj.message.applyAvoidBeApplyPersonIdUnit);
                $("#applyAvoidBeApplyPersonId").val(obj.message.applyAvoidBeApplyPersonId);
                $("#listenEvidenceMechanism").val(obj.message.listenEvidenceMechanism);
                $("#listenEvidenceAddress").val(obj.message.listenEvidenceAddress);
                $("#listenEvidencePreside").val(obj.message.listenEvidencePreside);
                $("#listenEvidencePerson").val(obj.message.listenEvidencePerson1 + "-" + obj.message.listenEvidencePerson2);
                $("#listenEvidenceRecord").val(obj.message.listenEvidenceRecord);
                $("#detectSite").val(obj.message.detectSite);
                $("#inquestStartTime").val(new Date(obj.message.inquestStartTime).format("yyyy-MM-dd hh:mm"));
                $("#inquestEndTime").val(new Date(obj.message.inquestEndTime).format("yyyy-MM-dd hh:mm"));
                $("#orgName").val(obj.message.orgName);
                $("#orgAddress").val(obj.message.orgAddress);
                $("#orgPhone").val(obj.message.orgPhone);
                $("#orgContact").val(obj.message.orgContact);
                $("#orgPostcode").val(obj.message.orgPostcode);
                $("#closeDate").val(new Date(obj.message.closeDate == "" ? 0 : obj.message.closeDate).format("yyyy-MM-dd"));
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求异常!!!请重试");
        }
    })
}

function getCaseDocumentByWitnesses(id, type) {
    $.ajax({
        url: "/luzheng/app/pb!readCaseDocumentByCaseIdAndDocType",
        data: {
            "caseDocument.caseId": id,
            "caseDocument.docType": type
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#beWitnessesName").val(obj.message[0].beWitnessesName);
                $("#beWitnessesSex").val(obj.message[0].beWitnessesSex == 0 ? '女' : '男');
                $("#beWitnessesAge").val(obj.message[0].beWitnessesAge);
                $("#beWitnessesPhone").val(obj.message[0].beWitnessesPhone);
                $("#beWitnessesWork").val(obj.message[0].beWitnessesWork);
                $("#beWitnessesAddress").val(obj.message[0].beWitnessesAddress);
                $("#beWitnessesCode").val(obj.message[0].beWitnessesCode);
                $("#illegalNoticeOfficeUnitId").val(obj.message[0].illegalNoticeOfficeUnitId);
                $("#investigationReportMaterial").val(obj.message[0].investigationReportMaterial);
                $("#caseName").val(obj.message[1].caseName);
                $("#officerFirstCardId").val(obj.message[1].officer1IdCard);
                $("#officerSecondCardId").val(obj.message[1].officer2IdCard);
                $("#beWitnessesOrgName").val(obj.message[0].beWitnessesOrgName);
                $("#beWitnessesOrgCode").val(obj.message[0].beWitnessesOrgCode);
                $("#beWitnessesOrgAddress").val(obj.message[0].beWitnessesOrgAddress);
                $("#listenEvidenceRecordContext").val(obj.message[0].listenEvidenceRecordContext);
                $("#litigantName").val(obj.message[1].litigantName);
                $("#litigantName2").val(obj.message[1].litigantName);
                $("#getDocNumber").val(obj.message[0].docNumber);
                var date2 = new Date(obj.message[0].date2).format("yyyy-MM-dd");
                if (date2 != undefined && date2 != "") {
                    $("#showYear").val(date2.substring(0, 4));
                    $("#showMonth").val(date2.substring(5, 7));
                    $("#showDay").val(date2.substring(8));
                    $("#six").val(date2.substring(0, 4));
                    $("#seven").val(date2.substring(5, 7));
                    $("#eight").val(date2.substring(8));
                }
                var context = obj.message[0].stePleaContext;
                var arr = ["one", "two", "three", "four", "five"];
                $("#nine").val(obj.message[1].litigantName);
                $("#ten").val(context.split('?')[9]);
                adviceSplitTheString(context, arr);
                var docWord = obj.message[0].docWord;
                if (docWord != undefined && docWord != "") {
                    $("#elven").val(docWord.split('〔')[0]);
                    $("#twelve").val(docWord.split('〔')[1].substring(0, 4));
                    $("#thirteen").val(obj.message[0].docNumber);
                }
                $("#officer1Id1").val(obj.message[0].officer1Id1);
                $("#causeAction").val(obj.message[1].causeActionId);
            } else {
                alert(obj.message);
            }
        },
        error: function () {
            alert("请求失败！！！请重试");
        }
    })
}

//获取抽样物品
function getArticleInfo(id, type) {
    $.ajax({
        url: "/luzheng/app/pb!readArticleInfo",
        data: {
            "caseDocument.caseId": id,
            "caseDocument.docType": type
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    if (i >= 1) {
                        fun();
                    }
                    $("#name" + i).val(obj.message[i].name);
                    $("#specification" + i).val(obj.message[i].specification);
                    $("#count" + i).val(obj.message[i].count);
                    $("#address" + i).val(obj.message[i].address);
                }
            }
        }
    })
}

//获取执法单位具体信息
function getOrgInfo(ob) {
    var id = $(ob).find('option:selected').attr('sds');
    $.ajax({
        url: "/luzheng/app/common!readOrgInfo",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#beWitnessesOrgAddress").val(obj.message.address);
                $("#beWitnessesOrgCode").val(obj.message.orgPostcode);
            }
        },
        error: function () {
            alert("请求失败！！！请重试");
        }
    })
}

//获取officer的职务及单位名称
function getData(id, zhiwu) {
    $.ajax({
        url: "/luzheng/app/common!readOfficer",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#" + zhiwu).val(obj.message.userZhiwu);
                ($.ajax({
                    url: "/luzheng/app/common!readOrgInfo",
                    data: {
                        id: obj.message.officeId
                    },
                    success: function (da) {
                        var o = JSON.parse(da);
                        $("#work").val(o.message.org);
                    }
                }));
            }
        }
    })
}

//获取officer的联系电话
function getOfficerPhone(id, phone) {
    $.ajax({
        url: "/luzheng/app/common!readOfficer",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#" + phone).val(obj.message.phone);
            }
        }
    })
}

//获取组织邮编
function getOrgCode(id, code) {
    $.ajax({
        url: "/luzheng/app/common!readOrgInfo",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#" + code).val(obj.message.orgPostcode);
            }
        }
    })
}

//获取机构的联系电话
function getOrgPhone(id, phone) {
    $.ajax({
        url: "/luzheng/app/common!readOrgInfo",
        data: {
            id: id
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                $("#" + phone).val(obj.message.phone);
            }
        }
    })
}

/**
 *
 * @param id 案件id
 * @param type 案件文书类型
 */
function deleteCaseDocument(id, type) {
    if (confirm("确定要是删除吗????")) {
        $.ajax({
            url: "/luzheng/app/pb!deleteCaseDocumentByCaseIdAndDocType",
            type: "post",
            data: {
                "caseId": id,
                "docType": type
            },
            success: function (data) {
                if (data != null && data.length > 0) {
                    var obj = JSON.parse(data);
                    if (obj.status == "success") {
                        alert(obj.message.toString());
                    }
                } else {
                    alert("删除失败哦!!!!");
                }
            },
            error: function () {
                alert("数据异常!!!请重试");
            }
        });
    }
}

/**
 * 要跳转的输出页面
 * @param caseId 案件id
 * @param type  案件文书对应的类型
 * @param out   输出页面的名称
 */
function forwordPrint(caseId, type, out) {
    window.location.href = "pb-b" + out + ".html?caseId=" + caseId + "&type=" + type;
}

//签名的输入
function getSrcValue() {
    $("#inputDiv").val($("#imghide").attr("src"));
    $("#inputDiv2").val($("#imghide2").attr("src"));
    $("#inputDiv3").val($("#imghide3").attr("src"));
    $("#inputDiv4").val($("#imghide4").attr("src"));
    $("#inputDiv5").val($("#imghide5").attr("src"));
    $("#inputDiv6").val($("#imghide6").attr("src"));
}


/**
 * 求出每个图片要显示的宽与高
 * @param width div的宽度
 * @param height    div的高度
 * @param count 图片的个数
 */
function changeImgSize(width, height, count) {
    var oneWidth = width / count;
    var oneHeight = height / count;
    return oneWidth + ":" + oneHeight;
}

/**
 * 开始日期
 * 日期处理
 */
function handleDateStart(startTime) {
    $("#startYear").text(startTime.substring(0, 4));
    $("#startMonth").text(startTime.substring(5, 7));
    $("#startDay").text(startTime.substring(8, 10));
    $("#startHours").text(startTime.substring(11, 13));
    $("#startMinute").text(startTime.substring(14));
}

function handleDateEnd(endTime) {
    $("#endYear").text(endTime.substring(0, 4));
    $("#endMonth").text(endTime.substring(5, 7));
    $("#endDay").text(endTime.substring(8, 10));
    $("#endHours").text(endTime.substring(11, 13));
    $("#endMinute").text(endTime.substring(14));
}

/**
 * 点击输出页面时添加 页数
 * @param id 案件文书id
 */
function setPageCount(id) {
    $.ajax({
        url: "/luzheng/app/case!setPrintPageCount",
        data: {
            "caseDocument.id": id,
            "caseDocument.pageCount": Math.ceil($("#pageCount").height() / 1053)
        }
    });
}

/**
 * 分隔字符串
 * @param advice    要分割的字符串
 * @param params    需要填充的参数
 */
function adviceSplitTheString(advice, params) {
    var str = advice.split('?');
    for (var i = 0; i < params.length; i++) {
        $("#" + params[i]).val(str[i]);
        $("#" + params[i]).text(str[i]);
    }
}


/**
 * 例外
 * @param advice
 * @param params
 */
function adviceSplitTheStringOhter(advice, params) {
    var str = advice.split('?');
    for (var i = 0; i < params.length; i++) {
        if (i >= 5) {
            $("#" + params[i]).val(str[i % (str.length - 2)]);
            continue;
        }
        $("#" + params[i]).val(str[i]);
    }
}

/**
 * 获取一点
 * @param advice
 * @param params
 */
function adviceSplitTheStringOhterLittle(advice, params) {
    var str = advice.split('?');
    $("#" + params[0]).val(str[0]);
    $("#" + params[1]).val(str[1]);
    $("#" + params[2]).val(str[4]);
}

/**
 * 获取案件文书信息
 * @param id
 * @param type  要查询的type
 */
function getCaseDocumentByMessage(id, type) {
    $.ajax({
        url: "/luzheng/app/pb!readCaseDocumentByCaseIdAndDocType",
        data: {
            "caseDocument.caseId": id,
            "caseDocument.docType": type
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                if (obj.message[1].litigantType == 0) {
                    $("#litigantName").val(obj.message[1].litigantName);
                    $("#litigantName").text(obj.message[1].litigantName);
                    $("#litigantAddress").val(obj.message[1].litigantAddress);
                } else {
                    $("#litigantName2").val(obj.message[1].litigantName);
                    $("#litigantAddress2").val(obj.message[1].litigantAddress);
                }
                $("#litigantName3").val(obj.message[1].litigantName);
                $("#litigantName10").val(obj.message[1].litigantName);
                $("#officer").val(obj.message[1].officer1Id + ":" + obj.message[1].officer2Id);
                $("#officer").text(obj.message[1].officer1Id + ":" + obj.message[1].officer2Id);
                $("#elventh").val(obj.message[1].litigantName);
                $("#twelvth").val(obj.message[1].litigantName);
                $("#number").val(obj.message[1].number);
                $("#number").text(obj.message[1].number);
                $("#causeAction").val(obj.message[1].causeActionId);
                $("#causeAction").text(obj.message[1].causeActionId);
                $("#orgName").val(obj.message[1].orgName);
                $("#orgName").text(obj.message[1].orgName);
                $("#puncHandlingResults").val(obj.message[1].puncHandlingResults);
                $("#puncHandlingResults").text(obj.message[1].puncHandlingResults);
                $("#closeDate").val(obj.message[1].closeDate);
                if (type == 81) {
                    $("#lawStartTime").val(new Date(obj.message[1].lawStartTime).format("yyyy-MM-dd hh:mm"));
                    $("#lawEndTime").val(new Date(obj.message[1].lawEndTime).format("yyyy-MM-dd hh:mm"));
                    $("#inquireAddress").val(obj.message[0].inquireAddress);
                    $("#inquireAddress").text(obj.message[0].inquireAddress);
                    var time1 = new Date(obj.message[1].lawStartTime).format("yyyy-MM-dd hh:mm");
                    $("#year").text(time1.substring(0, 4));
                    $("#month").text(time1.substring(5, 7));
                    $("#day").text(time1.substring(8, 10));
                    $("#hours").text(time1.substring(11, 13));
                    $("#mintus").text(time1.substring(14));
                    var time2 = new Date(obj.message[1].lawEndTime).format("yyyy-MM-dd hh:mm");
                    $("#hours2").text(time2.substring(11, 13));
                    $("#mintus2").text(time2.substring(14));
                    var date1 = new Date(obj.message[1].archiveTime).format("yyyy-MM-dd");
                    $("#year3").text(date1.substring(0, 4));
                    $("#month3").text(date1.substring(5, 7));
                    $("#day3").text(date1.substring(8));
                    var date2 = new Date(obj.message[1].closeDate).format("yyyy-MM-dd");
                    $("#year4").text(date2.substring(0, 4));
                    $("#month4").text(date2.substring(5, 7));
                    $("#day4").text(date2.substring(8));
                    return;
                }
                if (type == 82) {
                    var da = obj.message[0].stePleaContext.split('?');
                    $("#first").val(da[4]);
                    $("#first").text(da[4]);
                    $("#second").val(da[5]);
                    $("#second").text(da[5]);
                    var date2 = new Date(obj.message[0].date2).format("yyyy-MM-dd");
                    $("#third").val(date2.substring(0, 4));
                    $("#fourth").val(date2.substring(5, 7));
                    $("#fifth").val(date2.substring(8));
                    $("#third").text(date2.substring(0, 4));
                    $("#fourth").text(date2.substring(5, 7));
                    $("#fifth").text(date2.substring(8));
                    $("#sixth").val(da[9]);
                    $("#sixth").text(da[9]);
                    $("#seventh").val(obj.message[0].docNumber);
                    $("#seventh ").text(obj.message[0].docNumber);
                    $("#decideTime").val(date2);
                    return;
                }
                if (type == 88) {
                    var date1 = new Date(obj.message[0].date1).format("yyyy-MM-dd");
                    $("#eighth").val(date1.substring(0, 4));
                    $("#ninth").val(date1.substring(5, 7));
                    $("#tenth").val(date1.substring(8));
                    var researchConcluAndOpinions = obj.message[0].researchConcluAndOpinions;
                    var arr = ["92one", "92two", "92three"];
                    adviceSplitTheStringOhterLittle(researchConcluAndOpinions, arr);
                    return;
                }
                if (type == 90) {
                    $("#thirteen").val(obj.message[0].docWord + obj.message[0].docNumber);
                    var researchConcluAndOpinions = obj.message[0].researchConcluAndOpinions;
                    var arr = ["fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "ninteen", "twentieth", "twenty-first", "twenty-second", "twenty-third"];
                    adviceSplitTheStringOhter(researchConcluAndOpinions, arr);
                    return;
                }
            }
        }
    });
}

function adviceSplitTheStringSubOne(data, params) {
    var str = data.split('?');
    for (var i = 0; i < params.length; i++) {
        if (i == params.length - 1) {
            $("#" + params[i]).val(str[i + 1]);
            continue;
        }
        $("#" + params[i]).val(str[i]);
        $("#" + params[i]).text(str[i]);
    }
}

function getCaseDocumentByMessageOhter(id, type, printType) {
    $.ajax({
        url: "/luzheng/app/pb!readCaseDocumentByCaseIdAndDocType",
        data: {
            "caseDocument.caseId": id,
            "caseDocument.docType": type
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                if (obj.message[1].litigantType == 0) {
                    $("#litigantName").val(obj.message[1].litigantName);
                    $("#litigantAddress").val(obj.message[1].litigantAddress);
                } else {
                    $("#litigantName2").val(obj.message[1].litigantName);
                    $("#litigantAddress2").val(obj.message[1].litigantAddress);
                }

                $("#litigantName3").val(obj.message[1].litigantName);
                if (type == 81) {
                    var mess = obj.message[0].investigationReportResult;
                    var arr1 = ["wfyear", "wfmonth", "wfday", "startMoving", "startGuiDing", "startGuiDing2", "startJueDing"];
                    adviceSplitTheString(mess, arr1);
                }
                if (type == 82) {
                    var date = new Date(obj.message[0].date2).format("yyyy-MM-dd");
                    var docWord = obj.message[0].docWord;
                    $("#getvala").val(obj.message[1].causeActionId);
                    $("#startYear").val(date.substring(0, 4));
                    $("#startMonth").val(date.substring(5, 7));
                    $("#startDay").val(date.substring(8));
                    $("#docWordFirst").val(docWord.split('〔')[0]);
                    $("#docWordSecond").val(docWord.split('〔')[1].substring(0, 4));
                    $("#docNumber").val(obj.message[0].docNumber);
                    $("#docNumber2").val(obj.message[0].docNumber);
                    $("#docNumber").text(obj.message[0].docNumber);
                    $("#dicedTime").val(date);
                    $("#arrvieTime").val(date);
                    $("#year1").val(date.substring(0,4));
                    $("#month").val(date.substring(5, 7));
                    $("#day").val(date.substring(8,10));
                    $("#implementTime").val(date);
                    $("#samplingOrgan").val(obj.message[0].samplingOrgan);
                    $("#alcType").val(getType(obj.message[0].stePleaContext));
                    $("#alcType").text(getType(obj.message[0].stePleaContext));
                    $("#implementTime").text(new Date(date).format("yyyy年MM月dd日"));
                    $("#samplingOrgan").text(obj.message[0].samplingOrgan);
                    $.ajax({
                        url: "/luzheng/app/pb!getAllFinancialListingByCaseDocument",
                        data: {
                            "caseDocument.id": obj.message[0].id,
                            "caseDocument.caseId": obj.message[0].caseId
                        },
                        success: function (da) {
                            if (da != null && da.length > 0) {
                                var o = JSON.parse(da);
                                var allname = "";
                                if (o.status == "success") {
                                    for (var i = 0; i < o.message.length; i++) {
                                        if (printType != undefined && printType == 151) {
                                            fun(i);
                                            $("#name" + i).val(o.message[i].name);
                                            $("#count" + i).val(o.message[i].count);
                                            $("#code" + i).val(o.message[i].code);
                                            $("#allowName" + i).val(o.message[i].allowName);
                                            $("#spe" + i).val(o.message[i].spe);
                                            $("#name" + i).text(o.message[i].name);
                                            $("#count" + i).text(o.message[i].count);
                                            $("#code" + i).text(o.message[i].code);
                                            $("#allowName" + i).text(o.message[i].allowName);
                                            $("#spe" + i).text(o.message[i].spe);
                                        }
                                        allname += o.message[i].name;
                                        if (i != o.message.length - 1) {
                                            allname += ",";
                                        }
                                    }
                                    $("#finList").val(allname);
                                    $("#finList").text(allname);
                                }
                            }
                        }
                    });
                }
                if (type == 89) {
                    var time = new Date(obj.message[0].date1).format("yyyy-MM-dd");
                    // $("#arrvieTime").val(new Date(obj.message[0].date1).format("yyyy-MM-dd"));
                }
                if (type == 90) {
                    $("#thirteen").val(obj.message[0].docWord + obj.message[0].docNumber);
                    var researchConcluAndOpinions = obj.message[0].researchConcluAndOpinions;
                    var arr = ["fourteen", "fifteen", "sixteen", "seventeen", "ninteen", "twentieth"];
                    adviceSplitTheStringSubOne(researchConcluAndOpinions, arr);
                }
            }
        }
    });
}
/**
 *判断是否选择了文书,选择为false,没有选择为true
 * @returns {boolean}
 */
function ifDocNumber() {
    if ($("#selectDocNO").val() == undefined || $("#selectDocNO").val().length <= 0) {
        return true;
    }
    return false;
}

function forwordMap(id, casesite, areaName) {
    var x, y;
    $.ajax({
        url: "/luzheng/app/pb!readLinLatCaseByCaseId",
        data: {
            "id": id
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    x = obj.message.x;
                    y = obj.message.y;
                    window.location.href = "selectMap.html?caseId=" + id + "&x=" + x + "&y=" + y + "&caseSite=" + casesite + "&areaName=" + areaName;
                }
            } else {
                window.location.href = "selectMap.html?caseId=" + id + "&x=" + x + "&y=" + y + "&caseSite=" + casesite + "&areaName=" + areaName;
            }
        }
    });
}

function checkedOfficer() {
    var userName = $.cookie('uName');
    $("#selectOfficerFirst").val(userName);
}

/**
 *  流程需要跳转的界面
 * @param caseId
 * @param type
 * @param content
 * @param address
 */
function forwordInputOrOutByParams(caseId, type, content, address) {
    window.location.href = address + "?caseId=" + caseId + "&type=" + type + "&content=" + content;
}

function getOrgName() {
    $.ajax({
        url: "/luzheng/app/common!findAllOrgInfo",
        data: {
            "user.id": $.cookie('id')
        },
        success: function (data) {
            var obj = JSON.parse(data);
            if (obj.status == "success") {
                for (var i = 0; i < obj.message.length; i++) {
                    $("#applyAvoidFzjg").append('<option value="' + obj.message[i].org + '" sds ="' + obj.message[i].id + '">' + obj.message[i].org + '</option>');
                }
            }
        }
    });
}

/**
 * 得到强制措施种类
 */
function getType(type) {
    var allType = ["扣押车辆", "扣押设备", "扣押工具", "扣押证件", ".强制脱离车辆", "强制卸货", "收缴证件", "收缴票证", "开拆查验", "扣押危险化学品", "其他"];
    var chooseType = type.split(',');
    var printType = "";
    for (var i = 0; i < chooseType.length; i++) {
        printType += allType[chooseType[i] - 1];
        if (i != chooseType.length - 1) {
            printType += ",";
        }
    }
    return printType;
}

//是否需要审批
function ifChooseApprove() {
    $.ajax({
        url: "/luzheng/app/pb!readCaseInfo",
        data: {
            id: GetQueryString('caseId')
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    if (obj.message.caseType1 == 0) {
                        $("#approveOption").show();
                        $("#approvePeople").hide();
                    } else if (obj.message.caseType1 == 1) {
                        $("#approveOption").hide();
                        $("#approvePeople").show();
                    }
                }
            }
        }
    });
}

/**
 * 根据文书id进行查找
 * @param caseId
 * @param docType
 */
function getApprovePeople(caseId, docType) {
    $.ajax({
        url: "/luzheng/app/pb!readApproveByCaseDocId",
        data: {
            "approve.caseDocId": docType,
            "approve.caseId": caseId
        },
        success: function (data) {
            if (data != null && data.length > 0) {
                console.log(data);
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    $("#changeApprovePeople").children('select:first').remove();
                    $("#changeApprovePeople").append('<input type="text" readonly="readonly" class="span12" id="name"/>');
                    $("#name").val(obj.message.name);
                    $("#cardId").val(obj.message.cardId);
                }
            }
        }
    })
}

/**
 * 获取审批人执法证号
 */
function getCardId() {
    $("#cardId").val($("#selectApprovePeople").find('option:selected').attr('cardId'));
}