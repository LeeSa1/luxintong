/**
 * Created by BuDD on 2017/12/12.
 */


/**
 * 判断上传文件的类型,
 *  如果是图片则返回true,否则返回false
 * @param unqunie
 */
function checkType(unqunie) {
    var name = ['bmp', 'jpg', 'jpeg', 'gif', 'png', 'tiff', 'psd'];
    var inputFile = document.getElementById(unqunie);
    var file = inputFile.files;
    if (file != undefined && file.length > 0) {
        for (var i = 0; i < file.length; i++) {
            if (!name.indexOf(file[i].name.split('.')[1])) {
                alert("只允许上传图片");
                return false;
            }
        }
    }
    return true;
}

/**
 * 判断上传的文件大小
 *  *  如果是图片的大小则返回true,否则返回false
 * @param unqunie
 */
function checkImgSize(unqunie) {
    var inputFile = document.getElementById(unqunie);
    var file = inputFile.files;
    if (file != undefined && file.length > 0) {
        for (var i = 0; i < file.size; i++) {
            if (file[i].length >= 1024 * 1024) {
                alert("图片太大了");
                return false;
            }
        }
    }
    return true;
}