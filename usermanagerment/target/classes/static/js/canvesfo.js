window.onload = function(){
	var oC = document.getElementById('c1');
	var oC2 = document.getElementById('c2');
	var oC3 = document.getElementById('c3');
	var oC4 = document.getElementById('c4');
	var oGC = oC.getContext('2d');  
	var oGC2 = oC2.getContext('2d');
	var oGC3 = oC3.getContext('2d');
	var oGC4 = oC4.getContext('2d');
	oC.onmousedown = function(ev){
		var ev = ev || window.event;
		oGC.beginPath();
		
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
		if (userAgent.indexOf("Firefox") > -1) {
           oGC.moveTo(ev.layerX-oC.offsetLeft,ev.layerY-oC.offsetLeft);
	    } //判断是否Firefox浏览器
	    if (userAgent.indexOf("Chrome") > -1){
	        oGC.moveTo(ev.offsetX,ev.offsetY);
	     }//判断是否Chrome浏览器
	    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
	       oGC.moveTo(ev.x-oC.offsetLeft,ev.y-oC.offsetLeft);
	    }; //判断是否IE浏览器
	    
		document.onmousemove = function(ev){
			var ev = ev || window.event;
			if (userAgent.indexOf("Firefox") > -1) {
		      oGC.lineTo(ev.layerX-oC.offsetLeft,ev.layerY-oC.offsetTop);
		    } //判断是否Firefox浏览器
		    if (userAgent.indexOf("Chrome") > -1){
		        oGC.lineTo(ev.offsetX,ev.offsetY);
		     }//判断是否Chrome浏览器
		    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		       oGC.lineTo(ev.x-oC.offsetLeft,ev.y-oC.offsetTop);
		    }; //判断是否IE浏览器
			oGC.stroke();
		};
		oGC.closePath();
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		};
	};
	
	oC2.onmousedown = function(ev){
		var ev = ev || window.event;
		oGC2.beginPath();
		
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
		if (userAgent.indexOf("Firefox") > -1) {
           oGC2.moveTo(ev.layerX-oC.offsetLeft,ev.layerY-oC.offsetLeft);
	    } //判断是否Firefox浏览器
	    if (userAgent.indexOf("Chrome") > -1){
	        oGC2.moveTo(ev.offsetX,ev.offsetY);
	     }//判断是否Chrome浏览器
	    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
	       oGC2.moveTo(ev.x-oC2.offsetLeft,ev.y-oC2.offsetLeft);
	    }; //判断是否IE浏览器
	    
		document.onmousemove = function(ev){
			var ev = ev || window.event;
			if (userAgent.indexOf("Firefox") > -1) {
		      oGC2.lineTo(ev.layerX-oC2.offsetLeft,ev.layerY-oC2.offsetTop);
		    } //判断是否Firefox浏览器
		    if (userAgent.indexOf("Chrome") > -1){
		        oGC2.lineTo(ev.offsetX,ev.offsetY);
		     }//判断是否Chrome浏览器
		    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		       oGC2.lineTo(ev.x-oC2.offsetLeft,ev.y-oC2.offsetTop);
		    }; //判断是否IE浏览器
			oGC2.stroke();
		};
		oGC2.closePath();
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		};
	};
	
	
	
	oC3.onmousedown = function(ev){
		var ev = ev || window.event;
		oGC3.beginPath();
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
		if (userAgent.indexOf("Firefox") > -1) {
           oGC3.moveTo(ev.layerX-oC3.offsetLeft,ev.layerY-oC3.offsetLeft);
	    } //判断是否Firefox浏览器
	    if (userAgent.indexOf("Chrome") > -1){
	        oGC3.moveTo(ev.offsetX,ev.offsetY);
	     }//判断是否Chrome浏览器
	    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
	       oGC3.moveTo(ev.x-oC3.offsetLeft,ev.y-oC3.offsetLeft);
	    }; //判断是否IE浏览器
	    
		document.onmousemove = function(ev){
			var ev = ev || window.event;
			if (userAgent.indexOf("Firefox") > -1) {
		      oGC3.lineTo(ev.layerX-oC3.offsetLeft,ev.layerY-oC3.offsetTop);
		    } //判断是否Firefox浏览器
		    if (userAgent.indexOf("Chrome") > -1){
		        oGC3.lineTo(ev.offsetX,ev.offsetY);
		     }//判断是否Chrome浏览器
		    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		       oGC3.lineTo(ev.x-oC3.offsetLeft,ev.y-oC3.offsetTop);
		    }; //判断是否IE浏览器
			oGC3.stroke();
		};
		oGC3.closePath();
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		};
	};
	oC4.onmousedown = function(ev){
		var ev = ev || window.event;
		oGC4.beginPath();
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
		if (userAgent.indexOf("Firefox") > -1) {
           oGC4.moveTo(ev.layerX-oC4.offsetLeft,ev.layerY-oC4.offsetLeft);
	    } //判断是否Firefox浏览器
	    if (userAgent.indexOf("Chrome") > -1){
	        oGC4.moveTo(ev.offsetX,ev.offsetY);
	     }//判断是否Chrome浏览器
	    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
	       oGC4.moveTo(ev.x-oC4.offsetLeft,ev.y-oC4.offsetLeft);
	    }; //判断是否IE浏览器
	    
		document.onmousemove = function(ev){
			var ev = ev || window.event;
			if (userAgent.indexOf("Firefox") > -1) {
		      oGC4.lineTo(ev.layerX-oC4.offsetLeft,ev.layerY-oC4.offsetTop);
		    } //判断是否Firefox浏览器
		    if (userAgent.indexOf("Chrome") > -1){
		        oGC4.lineTo(ev.offsetX,ev.offsetY);
		     }//判断是否Chrome浏览器
		    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		       oGC4.lineTo(ev.x-oC4.offsetLeft,ev.y-oC4.offsetTop);
		    }; //判断是否IE浏览器
			oGC4.stroke();
		};
		oGC4.closePath();
		document.onmouseup = function(){
			document.onmousemove = null;
			document.onmouseup = null;
		};
	};
	
};
//提交形成图片显示在页面上面
function back(){        
          var canvas = document.getElementById("c1");
          var context = canvas.getContext("2d");
          //context.fillStyle = "green";
          //context.fillRect(50, 50, 100, 100);
          // no argument defaults to image/png; image/jpeg, etc also work on some
          // implementations -- image/png is the only one that must be supported per spec.
          //window.location = canvas.toDataURL("image/jpg");
          $("#inputDiv").hide();
          $("#imghide").show();
           var data=canvas.toDataURL("image/png");
          $("#signDiv img").attr("src",data);
          //$("#addhide").hide();
          $("#myModal").modal("hide");
           var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
            var divT=document.getElementById("currenttime");
             divT.innerHTML= "签名时间："+currentdate;
}
function back2(){
          var canvas = document.getElementById("c2");
          var context = canvas.getContext("2d");
          $("#imghide2").show();
          $("#inputDiv2").hide();
           var data1=canvas.toDataURL("image/png");
          $("#imghide2").attr("src",data1);
          
          //$("#addhide2").hide();
          $("#myModal2").modal("hide");
            var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
            var divT2=document.getElementById("currenttime2");
             divT2.innerHTML= "签名时间："+currentdate;
           
}


function back3(){        
          var canvas = document.getElementById("c3");
          var context = canvas.getContext("2d");
          $("#imghide3").show();
          $("#inputDiv3").hide();
           var data=canvas.toDataURL("image/png");
          $("#signDiv3 img").attr("src",data);
          //$("#addhide3").hide();
          $("#myModal3").modal("hide");
           var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
            var divT3=document.getElementById("currenttime3");
             divT3.innerHTML= "签名时间："+currentdate;
}
function back4(){        
          var canvas = document.getElementById("c4");
          var context = canvas.getContext("2d");
          $("#imghide4").show();
          $("#inputDiv4").hide();
           var data=canvas.toDataURL("image/png");
          $("#signDiv4 img").attr("src",data);
          //$("#addhide4").hide();
          $("#myModal4").modal("hide");
           var date = new Date();
		    var seperator1 = "-";
		    var seperator2 = ":";
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
		            + " " + date.getHours() + seperator2 + date.getMinutes()
		            + seperator2 + date.getSeconds();
            var divT4=document.getElementById("currenttime4");
             divT4.innerHTML= "签名时间："+currentdate;
}

//点击显示


	    function sign(){
	    $("#myModal").modal("show");  
	        var oC = document.getElementById('c1');
	        var ctx = oC.getContext('2d');
	        ctx.setTransform(1, 0, 0, 1, 0, 0);
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		}
	    
	    function sign2(){
	       $("#myModal2").modal("show");
	       var oC2 = document.getElementById('c2');
	       var ctx2 = oC2.getContext('2d');
           ctx2.setTransform(1, 0, 0, 1, 0, 0);
           ctx2.clearRect(0, 0, ctx2.canvas.width, ctx2.canvas.height);
		}
	    
	    function sign3(){
	       $("#myModal3").modal("show");
	       var oC3 = document.getElementById('c3');
	       var ctx3 = oC3.getContext('2d');
           ctx3.setTransform(1, 0, 0, 1, 0, 0);
           ctx3.clearRect(0, 0, ctx3.canvas.width, ctx3.canvas.height);
		}
	     function sign4(){
	       $("#myModal4").modal("show");
	       var oC4 = document.getElementById('c4');
	       var ctx4 = oC4.getContext('2d');
           ctx4.setTransform(1, 0, 0, 1, 0, 0);
           ctx4.clearRect(0, 0, ctx4.canvas.width, ctx4.canvas.height);
		}
	
