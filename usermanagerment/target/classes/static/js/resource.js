﻿function smart_img($imgdom) {
	var w = $imgdom.width();
	var h = $imgdom.height();
	console.log(w+','+h);
	if(w > h) {
		$imgdom.css({
			'height': '100%'
		});
		var left = ($imgdom.width() - $imgdom.parent().width()) / 2 * (-1);
		$imgdom.css({
			'margin-left': left + 'px'
		});
		if($imgdom.width() < $imgdom.parent().width()) {
			$imgdom.css({
				'width': '100%',
				'height': 'auto',
				'margin-left': '0'
			});
		}
	} else {
		$imgdom.css({
			'width': '100%'
		});
	}
}

$(function() {
	$img = $('.box').find('img');
	smart_img($img);

	$('.box').each(function() {
		$(this).children().eq(1).find('img').each(function() {
			$(this).height($(this).width() / 3 * 4)
		})
	});
	/*$('.box').children().eq(1).find('img').each(function() {
		$(this).height($(this).width()/3*4);
	});*/
})

$(function() {
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	nextSubject(0);
	
	
	$(".modal-box .remore").each(function() {
		if($(window).width() > 991) {
			var maxwidth = 200;
			if($(this).text().length > maxwidth) {
				$(this).text($(this).text().substring(0, maxwidth));
				$(this).html($(this).html() + '...');
			}
		} else {
			var maxwidth = 140;
			if($(this).text().length > maxwidth) {
				$(this).text($(this).text().substring(0, maxwidth));
				$(this).html($(this).html() + '...');
			}
		}
	});

	$(".modal-box2 .remore").each(function() {
		if($(window).width() > 991) {
			var maxwidth = 310;
			if($(this).text().length > maxwidth) {
				$(this).text($(this).text().substring(0, maxwidth));
				$(this).html($(this).html() + '...');
			}
		} else {
			var maxwidth = 220;
			if($(this).text().length > maxwidth) {
				$(this).text($(this).text().substring(0, maxwidth));
				$(this).html($(this).html() + '...');
			}
		}
	});

	$('.box').on('click','li',function() {
		$('.modal-box .tansuo').attr('data-id',$(this).attr('data-id'));
		$('#discuss').attr('data-id',$(this).attr('data-id'));
		$('#report').attr('data-id',$(this).attr('data-id'));
		if ($(this).attr('data-pic')==undefined||$(this).attr('data-pic')==''){
			$('.modal-box img').eq(0).attr('src','img/default.png');
		}else{
			$('.modal-box img').eq(0).attr('src',$(this).attr('data-pic'));
		}
		$('.modal-box li').eq(0).find('span').text($(this).attr('data-name'));
		$('.modal-box li').eq(1).find('span').text($(this).attr('data-author'));
		$('.modal-box li').eq(2).find('span').text($(this).attr('data-press'));
		var subject = $(this).attr('data-cat');
		var sSubject = '';
		console.log(subject);
		if (subject == 1){
			sSubject = '中医';
		}else if (subject == 2){
			sSubject = '外科';
		}else if (subject == 3){
			sSubject = '医技科';
		}else if (subject == 4){
			sSubject = '精神科';
		}else if (subject == 5){
			sSubject = '医学保健';
		}else if (subject == 6){
			sSubject = '儿科';
		}else if (subject == 7){
			sSubject = '妇产科';
		}else if (subject == 8){
			sSubject = '内科';
		}else if (subject == 9){
			sSubject = '医药学';
		}else if (subject == 10){
			sSubject = '男科与生殖医学';
		}else{
			sSubject = '其他';
		}
		console.log(sSubject);
		$('.modal-box li').eq(4).find('span').text(sSubject);
		$('.modal-box li').eq(6).find('span').text($(this).attr('data-xcode'));
		$('.modal-box p').text($(this).attr('data-desc'));
		$('.modal-box button').eq(0).attr('onClick','openurl("'+$(this).attr('data-link')+'","resource",'+$(this).attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box').fadeIn(100)
		/*$('.modal-box').find('img').each(function() {
			$(this).height($(this).width() / 3 * 4);
			var m = $(this).height() / 2 * (-1);
			$(this).css({
				'top': '50%',
				'marginTop': m
			})
		})*/
		var img = $('.modal-box img').eq(0);
		$(img).height($(img).width() / 3 * 4);
		var m = $(img).height() / 2 * (-1);
		$(img).css({
				'top': '50%',
				'marginTop': m
		})
	})

	$('.box2').on('click','li', function() {
		$('.modal-box2 .tansuo').attr('data-id',$(this).attr('data-id'));
		$('#discuss').attr('data-id',$(this).attr('data-id'));
		$('#report').attr('data-id',$(this).attr('data-id'));
		$('.modal-box2 li').eq(0).find('h3').text($(this).attr('data-name'));
		$('.modal-box2 li').eq(1).find('span').text($(this).attr('data-author'));
		console.log($(this).attr('data-time'));
		var time = new Date(parseInt($(this).attr('data-time')));
		$('.modal-box2 li').eq(2).find('span').text(time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate());
		$('.modal-box2 li').eq(3).find('span').text($(this).attr('data-xcode'));
		$('.modal-box2 p').text($(this).attr('data-desc'));
		$('.modal-box2 button').eq(0).attr('onClick','openurl("'+$(this).attr('data-link')+'","resource",'+$(this).attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box2').fadeIn(100)
	})

	$('.modal-box , .modal-box2').on('click','.btn-danger', function() {
		$('#modal').fadeOut(100).find('.modal-box, .modal-box2').fadeOut(100)
	})
	
	$('#discuss .btn-danger').on('click',function(){
				$('#discuss').slideUp(300);
			})
			
			$('#discuss .btn-info').on('click', function(){
				var content = $('#discuss .user-box textarea').val();
				if (content == ''){
					alert('请输入心得！');
					return;
				}else{
					$('#discuss .user-box textarea').val('');
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#discuss').attr('data-id');
				$('.comment>ul').html('');
				$.post(base+'/app/common!createComment',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "comment.content":content
					   },function(data){
						   console.log(data);
						   listDiscuss(1);
					   });
			})
})

$('#report .btn-info').on('click', function(){
				var errtype = $('input[name="report"]').val();
				var content = $('#report textarea').val();
				if (errtype == ''){
					alert('请选择错误类型！');
					return;
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#report').attr('data-id');
				$.post(base+'/app/common!createCorrect',{
					       "user.token":token,
			               "user.id":id,
						   "correct.type":errtype,
						   "correct.resType":type,
						   "correct.resId":resId,
						   "correct.content":content
					   },function(data){
						   data =JSON.parse(data);
						   alert(data.message);
						   $('#report').hide();
					   });
			})

function nextSubject(index){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	$('.style'+(index+1)+' .head a').attr('href','resourcelist.html?channel='+index);
	var s = $('.search #search-input').val();
	$.post(base+"/app/common!readResourceList",{
		       "user.token":token,
			   "user.id":id,
		       channel:index,
			   name:s
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
				   $('.style'+(index+1)+' ul').html('');
				   for (i=0;i<data.message.resultList.length;i++){
					   var obj = data.message.resultList[i];
					   console.log(obj);
					   if (index<2 && i<8){
						   var pic = obj.pic;
				           var img = obj.pic;
						   var title = obj.name;
						   if(title.length>20){
							   title = title.substring(0,17)+'...';
						   }
				           if (pic.indexOf('/upload')==0){
					           pic = base+pic;
				           }
				           if (pic==undefined||pic==''){
				               img = 'img/default.png';
				           }else{
							   img = pic;
						   }
					       var html = '<li data-id="'+obj.id+'" data-channel="'+index+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'"><a href="javascript:void(0)"><img src="'+img+'" onerror="nofind(this);"/></a>'
							    + '<p><a href="javascript:void(0)">'+title+'</a></p>'
							    + '<p>'+obj.author+'</p></li>';
					       $('.style'+(index+1)+' ul').append(html);
						   $img = $('.box').find('img');
	                       smart_img($img);
						   $('.box').each(function() {
		                       $(this).children().eq(1).find('img').each(function() {
			                       $(this).height($(this).width() / 3 * 4)
		                       })
	                       });
					   }else if (index>1){
						   var pic = obj.pic;
				           if (pic.indexOf('/upload')==0){
					           pic = base+pic;
				           }
						   var html = '<li data-id="'+obj.id+'" data-channel="'+index+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-pic="'+pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><a href="javascript:void(0)">'+obj.name+'</a></li>';
					       $('.style'+(index+1)+' ul').append(html); 
					   }
				   }
				   if (index < 6){
					   nextSubject(index+1);
				   }
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
}

function showCollection(obj){
    var cangs_list = '';
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
    var txt = obj.parent().parent().find('div:first-child').text();
	console.log(obj.parent().parent());
	console.log(txt);
	var type = "resource";
	var resId = obj.parents('.tansuo').attr('data-id');
    var message_id = resId;
    $.get(
        base+'/app/common!readCollectList',{
			"user.token":token,
			"user.id":id,
		    "collect.resType":type,
			"collect.resId":resId,
		},function(data){
			console.log(data);
			data =JSON.parse(data);
            for(var i = 0 ;i < data.message.resultList.length; i ++){
                cangs_list += data.message.resultList[i].resId+',';
            }
            var arr = cangs_list.split(',');
            var system = {
                win: false,
                mac: false,
                xll: false,
                ipad:false
            };
            //检测平台
            var p = navigator.platform;
            system.win = p.indexOf("Win") == 0;
            system.mac = p.indexOf("Mac") == 0;
            system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
            system.ipad = (navigator.userAgent.match(/iPad/i) != null)?true:false;
            var isPC = 1;
            if (system.win || system.mac || system.xll) {

            } else {
                isPC = 0;
            }
            //收藏、取消收藏
            if($.inArray(resId,arr) < 0){
				console.log(obj.parent().parent().find('div:first-child'));
                obj.parent().parent().find('div:first-child').text('收藏');
				obj.parent().parent().width('80px');
                if (isPC == 1)
                {obj.parent().parent().find('div:first-child').show();}
            }
            else{
				console.log(obj.parent().parent().find('div:first-child'));
                obj.parent().parent().find('div:first-child').text('取消收藏');
				obj.parent().parent().width('115px');
                if (isPC == 1)
                {obj.parent().parent().find('div:first-child').show();}
            }
			
        }
    );
};
    function hideCollection(obj){
	obj.parent().parent().find('div:first-child').hide();
}
function collection(obj){
	//alert(token);
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
    var txt = obj.find('div:first-child').text();
	var type = "resource";
	var resId = obj.parents('.tansuo').attr('data-id');
	console.log(resId);
    if(token == '' || token == undefined){
        alert('登录后才可以收藏');
    }
    else{
        var message_id = resId;
        if(txt == '收藏'){
            //调用收藏接口
            $.get(
                base+'/app/common!createCollect',
				{
					"user.token":token,
			        "user.id":id,
				    "collect.resType":type,
					"collect.resId":resId,
				},
				function(data){
                    console.log(data);
                    //if(data.status == 200){
                        data =JSON.parse(data);
						alert(data.message);
                        obj.find('div:first-child').text('取消收藏');
                    //}
                }
            )
        }
        if(txt == '取消收藏'){
            //调用取消收藏接口
            $.get(
                base+'/app/common!cancelCollect',{
					"user.token":token,
			        "user.id":id,
				    "collect.resType":type,
					"collect.resId":resId,
				},function(data){
                    //if(data.status == 200){
						data =JSON.parse(data);
						alert(data.message);
                        obj.find('div:first-child').text('收藏');
                    //}
                }
            )
        }
    }
};
/*点赞功能*/
function zan(obj){
    $('#report').show();
};

/*分享功能*/
function share(){
    $('#share1').click();
};
/*评论弹框*/
function toDiscuss(){
	            listDiscuss(1);
				$('#discuss').fadeIn(300);
}

function listDiscuss(pageindex){
	            var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#discuss').attr('data-id');

				$('.comment>ul').html('');
				$('#discuss .page .title').remove();
				$.post(base+'/app/common!readCommentList',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "page.index":pageindex
					   },function(data){
						   console.log(data);
						   data = JSON.parse(data);
			               if (data.status == 'success'){
							   for (i=0;i<data.message.resultList.length;i++){
								   var obj = data.message.resultList[i];
								   var time = new Date(obj.createdAt);
								   var photo = obj.user.photo;
								   if (photo.indexOf('/upload')==0){
									   photo = base+photo;
								   }
								   var html = '<li><div><img src="'+photo+'" /></div>'
								            + '<div><p><b>'+obj.user.name+': </b><span>'+obj.content+'</span></p>'
								            + '<p>'+time.getFullYear()+'-'+fillzero((time.getMonth()+1))+'-'+fillzero(time.getDate())+' '+fillzero(time.getHours())+':'+fillzero(time.getMinutes())+':'+fillzero(time.getSeconds())+'</p></div></li>';
								   $('.comment>ul').append(html);
							   }
							   $table = $('#discuss .comment>ul');
				               $pageli = $('#discuss .page');
				               $myson = $('#discuss .comment>ul>li')
				               mypage($table, 10, data.message.index-1,data.message.resultList.length, $pageli, $myson);
						   }
					   });

}

function closeCorrect(){
	$('#report').hide();
}

function showTip(obj){
	var system = {
                win: false,
                mac: false,
                xll: false,
                ipad:false
            };
            //检测平台
            var p = navigator.platform;
            system.win = p.indexOf("Win") == 0;
            system.mac = p.indexOf("Mac") == 0;
            system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
            system.ipad = (navigator.userAgent.match(/iPad/i) != null)?true:false;
            var isPC = 1;
            if (system.win || system.mac || system.xll) {

            } else {
                isPC = 0;
            }
	if (isPC == 1){
	obj.parents("li").find("div").eq(0).show();}
	obj.css('opacity','1');
}
function hideTip(obj){
	obj.parents("li").find("div").eq(0).hide();
	obj.css('opacity','0.8');
}
        function fillzero(s){
			if (parseInt(s)<10){
				return '0'+s;
			}else{
				return s;
			}
		}
		
function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  r[2]; return null;
}

function openurl(url,resType,resId){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	$.post(base+"/app/common!logView",{
		                "user.token":token,
			            "user.id":id,
						"viewlog.resType":resType,
			            "viewlog.resId":resId
		            },function(data){
						console.log(data);
					}
				);
    window.open(base+'/sysmgr/openresource?url='+url);
}

function searchText(){
	//if ($('.search #search-input').val()!=''){
		doSearch();
	//}
}

function doSearch(){
	nextSubject(0);
}

function nofind(obj){
	obj.src="img/default.png";
}