$('.learn-head').children('ul').on('click', 'li', function() {
	var m = $(this).index();
	$('.learn-head li').removeClass('active').eq(m).addClass('active');
	$('.learn-head').next('div').children('ul').fadeOut(200).delay(220).eq(m).fadeIn(200);

})

$(function() {
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	nextSubject(0);
	$('.box').on('click','li',function() {
		var index = $(this).attr('data-index');
		if (index < 3){
			var data_id = $(this).attr('data-id');
			var link = 'detail.html?type=subresource&id='+data_id;
			window.open(link);
		}else if (index == 3){
			var channel = $(this).attr('data-channel');
			if (channel == '书籍' || channel == '视频'){
				if ($(this).attr('data-pic')==undefined||$(this).attr('data-pic')==''){
			$('.modal-box img').eq(0).attr('src','img/default.png');
		}else{
			$('.modal-box img').eq(0).attr('src',$(this).attr('data-pic'));
		}
		
		$('.modal-box li').eq(0).find('span').text($(this).attr('data-name'));
		$('.modal-box li').eq(1).find('span').text($(this).attr('data-author'));
		$('.modal-box li').eq(2).find('span').text($(this).attr('data-press'));
		var subject = $(this).attr('data-cat');
		var sSubject = '';
		console.log(subject);
		if (subject == 1){
			sSubject = '中医';
		}else if (subject == 2){
			sSubject = '外科';
		}else if (subject == 3){
			sSubject = '医技科';
		}else if (subject == 4){
			sSubject = '精神科';
		}else if (subject == 5){
			sSubject = '医学保健';
		}else if (subject == 6){
			sSubject = '儿科';
		}else if (subject == 7){
			sSubject = '妇产科';
		}else if (subject == 8){
			sSubject = '内科';
		}else if (subject == 9){
			sSubject = '医药学';
		}else if (subject == 10){
			sSubject = '男科与生殖医学';
		}else{
			sSubject = '其他';
		}
		console.log(sSubject);
		$('.modal-box li').eq(4).find('span').text(sSubject);
		$('.modal-box li').eq(6).find('span').text($(this).attr('data-xcode'));
		$('.modal-box p').text($(this).attr('data-desc'));
		$('.modal-box button').eq(0).attr('onClick','openurl("'+$(this).attr('data-link')+'","resource",'+$(this).attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box').fadeIn(100)
		$('.modal-box').find('img').each(function() {
			$(this).height($(this).width() / 3 * 4);
			var m = $(this).height() / 2 * (-1);
			$(this).css({
				'top': '50%',
				'marginTop': m
			})
		})
			}else{
				$('.modal-box2 li').eq(0).find('h3').text($(this).attr('data-name'));
		$('.modal-box2 li').eq(1).find('span').text($(this).attr('data-author'));
		console.log($(this).attr('data-time'));
		var time = new Date(parseInt($(this).attr('data-time')));
		$('.modal-box2 li').eq(2).find('span').text(time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate());
		$('.modal-box2 li').eq(3).find('span').text($(this).attr('data-xcode'));
		$('.modal-box2 p').text($(this).attr('data-desc'));
		$('.modal-box2 button').eq(0).attr('onClick','openurl("'+$(this).attr('data-link')+'","resource",'+$(this).attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box2').fadeIn(100)
			}
			$('.modal-box , .modal-box2').on('click','.btn-danger', function() {
		$('#modal').fadeOut(100).find('.modal-box, .modal-box2').fadeOut(100)
	})
		}else if (index == 4){
			var data_id = $(this).attr('data-id');
			var link = 'detail.html?type=policy&id='+data_id;
			window.open(link);
		}
	})
})

$(function() {
	$('.head>button').toggle(
		function() {
			var index = $(this).attr('data-index');
			getStudied(index);
			//$(this).attr('class', 'sbtn btn-success');
			//$(this).children('i').attr('class', 'glyphicon glyphicon-ok');
			//$(this).children('span').text('已学习');
			$(this).parents('.head').next().children('.wait-study').slideToggle().siblings('.ok-study').slideToggle();
		},
		function() {
			var index = $(this).attr('data-index');
			getNoStudied(index);
			//$(this).attr('class', 'sbtn btn-warning');
			//$(this).children('i').attr('class', 'glyphicon glyphicon-pencil');
			//$(this).children('span').text('待学习');
			$(this).parents('.head').next().children('.ok-study').slideToggle().siblings('.wait-study').slideToggle();
		}
	)
})

$(function() {
	if($(window).width() < 991) {
		$('.head>span').on('click', function(e) {
			$(this).next('ul').slideDown();
			e.stopPropagation();
		})

		$(document).on('click', function() {
			$('.head>ul').slideUp();
		})
	}
})

function nextSubject(index){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var url = '';
	var listurl = '';
	var channel;
	if (index < 3){
		url = base+"/app/common!readStudySubResList";
		if (index == 0){
			channel = 0;
		}else if (index == 1){
			channel = 2;
		}else if (index == 2){
			channel = 1;
		}
		$('.style'+(index+1)+' .head>a').attr('href','studylist.html?type=subresource&channel='+channel);
		$('.style'+(index+1)+' .head li>a').eq(0).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=内科');
		$('.style'+(index+1)+' .head li>a').eq(1).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=外科');
		$('.style'+(index+1)+' .head li>a').eq(2).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=妇产科');
		$('.style'+(index+1)+' .head li>a').eq(3).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=儿科');
		$('.style'+(index+1)+' .head li>a').eq(4).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=皮肤性病科');
		$('.style'+(index+1)+' .head li>a').eq(5).attr('href','studylist.html?type=subresource&channel='+channel+'&subject=临床其他');
	}else if (index == 3){
		url = base+"/app/common!readStudyResourceList";
		channel = '';
		$('.style'+(index+1)+' .head>a').attr('href','studylist.html?type=resource');
		$('.style'+(index+1)+' .head li>a').eq(0).attr('href','studylist.html?type=resource&channel=0');
		$('.style'+(index+1)+' .head li>a').eq(1).attr('href','studylist.html?type=resource&channel=1');
		$('.style'+(index+1)+' .head li>a').eq(2).attr('href','studylist.html?type=resource&channel=2');
		$('.style'+(index+1)+' .head li>a').eq(3).attr('href','studylist.html?type=resource&channel=3');
		$('.style'+(index+1)+' .head li>a').eq(4).attr('href','studylist.html?type=resource&channel=4');
	}else if (index == 4){
		url = base+"/app/common!readStudyPolicyList";
		channel = '';
		$('.style'+(index+1)+' .head>a').attr('href','studylist.html?type=policy');
		$('.style'+(index+1)+' .head li>a').eq(0).attr('href','studylist.html?type=policy&channel=0');
		$('.style'+(index+1)+' .head li>a').eq(1).attr('href','studylist.html?type=policy&channel=1');
		$('.style'+(index+1)+' .head li>a').eq(2).attr('href','studylist.html?type=policy&channel=2');
	}
	var s = $('.search #search-input').val();
	$.post(url,{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
			   name:s,
			   studied:"0"
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
				   for (j=0;j<data.message.subCountList.length;j++){
					   var tmp = data.message.subCountList[j];
					   if (index<3 && tmp[0] == channel){
						   if (tmp[1]<7){
							   $('.style'+(index+1)+' .head li').eq(tmp[1]-1).find('a span').text('('+tmp[2]+')');
						   }
					   }
				   }
				   for (j=0;j<data.message.channelCountList.length;j++){
					   var tmp = data.message.channelCountList[j];
					   if (index == 3 || index == 4){
							   $('.style'+(index+1)+' .head li').eq(tmp[0]).find('a span').text('('+tmp[1]+')');
					   }
				   }
				   for (i=0;i<data.message.resultList.length;i++){
					   var obj = data.message.resultList[i];
					   console.log(obj);
					   
					       var html = '<li data-index="'+index+'" data-id="'+obj.id+'" data-channel="'+obj.channel+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+obj.pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><a href="javascript:void(0)">'+obj.name+'</a></li>';
					       $('.style'+(index+1)+' .wait-study').append(html);
						   //$('.style'+(index+1)+' .ok-study').append(html);
				   }
			   }
			   if (index < 4){
					   nextSubject(index+1);
				   }
		   });
}

function getStudied(index){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var url = '';
	var listurl = '';
	var channel;
	if (index < 3){
		url = base+"/app/common!readStudySubResList";
		if (index == 0){
			channel = 0;
		}else if (index == 1){
			channel = 2;
		}else if (index == 2){
			channel = 1;
		}
	}else if (index == 3){
		url = base+"/app/common!readStudyResourceList";
		channel = '';
	}else if (index == 4){
		url = base+"/app/common!readStudyPolicyList";
		channel = '';
	}
	console.log(url);
	var s = $('.search #search-input').val();
	$.post(url,{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
			   name:s,
			   studied:"1"
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   $('.style'+(parseInt(index)+1)+' .ok-study').html('');	   
			   if (data.status == 'success'){
				   for (j=0;j<data.message.subCountList.length;j++){
					   var tmp = data.message.subCountList[j];
					   if (index<3 && tmp[0] == channel){
						   if (tmp[1]<7){
							   $('.style'+(parseInt(index)+1)+' .head li').eq(tmp[1]-1).find('a span').text('('+tmp[2]+')');
						   }
					   }
				   }
				   for (j=0;j<data.message.channelCountList.length;j++){
					   var tmp = data.message.channelCountList[j];
					   if (index == 3 || index == 4){
							   $('.style'+(parseInt(index)+1)+' .head li').eq(tmp[0]).find('a span').text('('+tmp[1]+')');
					   }
				   }
				   for (i=0;i<data.message.resultList.length;i++){
					   var obj = data.message.resultList[i];
					   console.log(obj);
					   
					       var html = '<li data-index="'+index+'" data-id="'+obj.id+'" data-channel="'+obj.channel+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+obj.pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><a href="javascript:void(0)">'+obj.name+'</a></li>';
					       //$('.style'+(index+1)+' .wait-study').append(html);
						   console.log(html);
						   console.log($('.style'+(index+1)+' .ok-study'));
						   $('.style'+(parseInt(index)+1)+' .ok-study').append(html);
				   }
				   console.log($('.style'+(parseInt(index)+1)+' .head>button'));
				   $('.style'+(parseInt(index)+1)+' .head>button').attr('class', 'sbtn btn-success');
			       $('.style'+(parseInt(index)+1)+' .head>button').children('i').attr('class', 'glyphicon glyphicon-ok');
			       $('.style'+(parseInt(index)+1)+' .head>button').children('span').text('已学习');
				   
			   }
			   
		   });
}

function getNoStudied(index){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var url = '';
	var listurl = '';
	var channel;
	if (index < 3){
		url = base+"/app/common!readStudySubResList";
		if (index == 0){
			channel = 0;
		}else if (index == 1){
			channel = 2;
		}else if (index == 2){
			channel = 1;
		}
	}else if (index == 3){
		url = base+"/app/common!readStudyResourceList";
		channel = '';
	}else if (index == 4){
		url = base+"/app/common!readStudyPolicyList";
		channel = '';
	}
	var s = $('.search #search-input').val();
	$.post(url,{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
			   name:s,
			   studied:"0"
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   $('.style'+(parseInt(index)+1)+' .wait-study').html('');	   
			   if (data.status == 'success'){
				   for (j=0;j<data.message.subCountList.length;j++){
					   var tmp = data.message.subCountList[j];
					   if (index<3 && tmp[0] == channel){
						   if (tmp[1]<7){
							   $('.style'+(parseInt(index)+1)+' .head li').eq(tmp[1]-1).find('a span').text('('+tmp[2]+')');
						   }
					   }
				   }
				   for (j=0;j<data.message.channelCountList.length;j++){
					   var tmp = data.message.channelCountList[j];
					   if (index == 3 || index == 4){
							   $('.style'+(parseInt(index)+1)+' .head li').eq(tmp[0]).find('a span').text('('+tmp[1]+')');
					   }
				   }
				   for (i=0;i<data.message.resultList.length;i++){
					   var obj = data.message.resultList[i];
					   console.log(obj);
					   
					       var html = '<li data-index="'+index+'" data-id="'+obj.id+'" data-channel="'+obj.channel+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+obj.pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><a href="javascript:void(0)">'+obj.name+'</a></li>';
					       $('.style'+(parseInt(index)+1)+' .wait-study').append(html);
						   //$('.style'+(index+1)+' .ok-study').append(html);
				   }
				   $('.style'+(parseInt(index)+1)+' .head>button').attr('class', 'sbtn btn-warning');
			       $('.style'+(parseInt(index)+1)+' .head>button').children('i').attr('class', 'glyphicon glyphicon-pencil');
			       $('.style'+(parseInt(index)+1)+' .head>button').children('span').text('待学习');
				   
			   }
			   
		   });
}

function openurl(url,resType,resId){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	$.post(base+"/app/common!logView",{
		                "user.token":token,
			            "user.id":id,
						"viewlog.resType":resType,
			            "viewlog.resId":resId
		            },function(data){
						console.log(data);
					}
				);
    window.open(base+'/sysmgr/openresource?url='+url);
}

function searchText(){
	//if ($('.search #search-input').val()!=''){
		doSearch();
	//}
}

function doSearch(){
	for (i=0;i<5;i++){
		console.log($('.style'+(parseInt(i)+1)+' .head>button').attr("class"));
		if ($('.style'+(parseInt(i)+1)+' .head>button').attr("class")=='sbtn btn-warning'){
			//alert("no");
			getNoStudied(i);
		}else{
			//alert("yes");
			getStudied(i);
		}
	}
}

function nofind(obj){
	obj.src="img/default.png";
}