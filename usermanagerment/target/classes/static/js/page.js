function mypage(mytable, pagenum, currentPage, numRows, pageli, myson) {
	var $table = mytable;
	var $son = myson;
	var $pageul = pageli;
	//分页效果
	//var currentPage = 0; //当前页
	var pageSize = pagenum; //每页行数（不包括表头）
	$table.bind('repaginate', function() {
		//$table.find($son).hide().slice(currentPage * pageSize, (currentPage + 1) * pageSize).show();
		$pageul.find("li").removeClass("active");
		$pageul.find("li").eq(currentPage + 2).addClass("active");
		if(numPages > 5) {
			if(currentPage < 2){
				$pageul.find(".title").hide().slice(0, 5).show();
			}else if(currentPage < numPages - 3 && currentPage >= 2){
				$pageul.find(".title").hide().slice(currentPage - 2, currentPage + 3).show();
			}else {
				$pageul.find(".title").hide().slice(numPages - 5, numPages).show();
			}
		}
	});
	//var numRows = $table.find($son).length; //记录总条数
	var numPages = Math.ceil(numRows / pageSize); //总页数
	var $pager = $('.right_page'); //分页div
	$pageul.find(".title").remove();
	for(var page = 0; page < numPages; page++) {
		//为分页标签加上链接
		$pageadd = $('<li class="title">' + (page + 1) + '</li>').bind("click", {
			"newPage": page
		}, function(event) {
			currentPage = event.data["newPage"];
			$("#pageindex").val(currentPage+1);
			doSearch();
			//$table.trigger("repaginate"); //调用前面的repaginate
		});
		
		$pager.before($pageadd);
	}
	$(".first_page").click(function() {
	  if(currentPage > 0) {
		currentPage = 0;
		//$table.trigger("repaginate");
		$("#pageindex").val(currentPage+1);
		doSearch();
	  }
	});
	$(".left_page").click(function() {
		if(currentPage > 0) {
			--currentPage;
			//$table.trigger("repaginate");
			$("#pageindex").val(currentPage+1);
			doSearch();
		}
	});
	$(".right_page").click(function() {
		if(currentPage < numPages - 1) {
			++currentPage;
			//$table.trigger("repaginate");
			$("#pageindex").val(currentPage+1);
			doSearch();
		}
	});
	$(".last_page").click(function() {
	  if(currentPage < numPages - 1) {
		currentPage = numPages - 1;
		//$table.trigger("repaginate");
		$("#pageindex").val(currentPage+1);
		doSearch();
	  }
	});
	$table.trigger("repaginate"); //初始化 
	$(".page-text-page b").text(numPages);
	$(".page-text-counts b").text(numRows);
	$(".page-jump").click(function() {
		currentPage = $(".pagecounts").val() - 1;
		$table.trigger("repaginate");
	});
}
