﻿/*
 * jQuery showTiming plugin v1.0
 * 
 * Copyright (c) 2013 Zero
 * Context - http://www.zero.com
 * 
 * Dual licensed under the MIT and GPL licenses.
 */
	jQuery.exporting = function(form,url,options) {
		
		var indicatorID;
   		var settings = {
   			'addClass': '',
       		'beforeShow': '', 
   			'afterShow': '',
   			'hPos': 'center', 
       		'vPos': 'center',
   			'indicatorZIndex' : 900, 
   			'overlayZIndex': 800, 
       		'parent': '',
   			'marginTop': 0,
   			'marginLeft': 0,
       		'overlayWidth': null,
   			'overlayHeight': null,
			'time':60
       	};

		jQuery.extend(settings, options);
       	
		// Set up ID and classes
		if ( settings.indicatorID ) {
			indicatorID = settings.indicatorID;
		}
		else {
			indicatorID = 'body';
		}

		var loadingDiv = jQuery('<div id="loading-indicator-'+ indicatorID +'"></div>');
		var overlayDiv = jQuery('<div id="loading-indicator-'+ indicatorID +'-overlay"></div>');
		
		jQuery(loadingDiv).attr('id', 'loading-indicator-' + indicatorID );
		jQuery(document.body).append(loadingDiv);
		
		// Set overlay classes
		jQuery(overlayDiv).attr('id', 'loading-indicator-' + indicatorID + '-overlay');
		// Append to body, otherwise position() doesn't work on Webkit-based browsers
		jQuery(document.body).append(overlayDiv);

		// Create the overlay
		jQuery(overlayDiv).css('display', 'none');
		jQuery(overlayDiv).css('visibility', 'hidden');
		
		
		jQuery(overlayDiv).addClass('loading-indicator-overlay');
		
		if ( settings.addClass ){
			jQuery(overlayDiv).addClass(settings.addClass + '-overlay');
		}
		// Set overlay position
		var overlay_width;
		var overlay_height;
		
		var border_top_width = jQuery('body').css('border-top-width');
		var border_left_width = jQuery('body').css('border-left-width');
		
		// IE will return values like 'medium' as the default border, 
		// but we need a number
		border_top_width = isNaN(parseInt(border_top_width)) ? 0 : border_top_width;
		border_left_width = isNaN(parseInt(border_left_width)) ? 0 : border_left_width;
		
		var overlay_left_pos = jQuery('body').offset().left + parseInt(border_left_width);
		var overlay_top_pos = jQuery('body').offset().top + parseInt(border_top_width);
		
		if ( settings.overlayWidth !== null ) {
			overlay_width = settings.overlayWidth;
		}
		else {
			
			overlay_width = parseInt(jQuery('body').width()) + parseInt(jQuery('body').css('padding-right')) + parseInt(jQuery('body').css('padding-left'));
		}

		if ( settings.overlayHeight !== null ) {
			overlay_height = settings.overlayWidth;
		}
		else {
			overlay_height = parseInt(jQuery('body').height()) + parseInt(jQuery('body').css('padding-top')) + parseInt(jQuery('body').css('padding-bottom'));
		}

		jQuery(overlayDiv).css('width', overlay_width.toString() + 'px');
		jQuery(overlayDiv).css('height', overlay_height.toString() + 'px');

		jQuery(overlayDiv).css('left', overlay_left_pos.toString() + 'px');
		jQuery(overlayDiv).css('position', 'absolute');

		jQuery(overlayDiv).css('top', overlay_top_pos.toString() + 'px' );
		jQuery(overlayDiv).css('z-index', settings.overlayZIndex);

		// Set any custom overlay CSS		
   		if ( settings.overlayCSS ) {
   			jQuery(overlayDiv).css ( settings.overlayCSS );
   		}

		// We have to append the element to the body first
		// or .width() won't work in Webkit-based browsers (e.g. Chrome, Safari)
		jQuery(loadingDiv).css('display', 'none');
		jQuery(loadingDiv).css('visibility', 'hidden');
		
		jQuery(loadingDiv).css('position', 'absolute');
		jQuery(loadingDiv).css('z-index', settings.indicatorZIndex);

		// Set top margin
		var indicatorTop = overlay_top_pos;
		
		if ( settings.marginTop ) {
			indicatorTop += parseInt(settings.marginTop);
		}
		
		var indicatorLeft = overlay_left_pos;
		
		if ( settings.marginLeft ) {
			indicatorLeft += parseInt(settings.marginTop);
		}
		
		
		jQuery(loadingDiv).css('width', '80px');
		jQuery(loadingDiv).css('height', '80px');
		jQuery(loadingDiv).css('left', (indicatorLeft + ((jQuery(overlayDiv).width() - parseInt(jQuery(loadingDiv).width())) / 2)).toString()  + 'px');
		jQuery(loadingDiv).css('top', (indicatorTop + ((jQuery(overlayDiv).height() - parseInt(jQuery(loadingDiv).height())) / 2)).toString()  + 'px');
		jQuery(loadingDiv).css('text-align', 'center');
		jQuery(loadingDiv).css('vertical-align', 'middle');

		// Set any custom css for loading indicator
   		if ( settings.css ) {
   			jQuery(loadingDiv).css ( settings.css );
   		}
		// Set up callback options
		var callback_options = {
			'overlay': overlayDiv,
			//'indicator': loadingDiv,
			'element': this
		};
		// beforeShow callback
		if ( typeof(settings.beforeShow) == 'function' ) {
			settings.beforeShow( callback_options );
		}
		
		var webroot = document.location.href;
		webroot = webroot.substring(webroot.indexOf('//') + 2, webroot.length);
		webroot = webroot.substring(webroot.indexOf('/') + 1, webroot.length);
		webroot = webroot.substring(0, webroot.indexOf('/'));
		var rootpath = "/" + webroot;
		
		//jQuery(loadingDiv).addClass('timing-indicator');
		var sWidth = 60;
		var sHeight = 60;
		
		var loadingDivHtml = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="' + sWidth + '" height="' + sHeight + '">';
		loadingDivHtml += '<param name="movie" value="' + rootpath + '/img/exporting.swf">';
		loadingDivHtml += '<param name="menu" value="false">';
		loadingDivHtml += '<param name="quality" value="high">';
		loadingDivHtml += '<param name="wmode" value="transparent">';
		loadingDivHtml += '<embed src="' + rootpath + '/img/exporting.swf" visibility="visible" width="'+sWidth+'" height="'+sHeight+'" quality="high" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>';
		loadingDivHtml += '</object>';
		loadingDiv.html(loadingDivHtml);
		
		var count =  settings.time;
		if (count == null) {
			count = 60;// 默认 60 秒
		}
		
    	
    	jQuery.removeCookie("IS_EXPORT_COMPLETE");  
    	
		countdown = setInterval(function(){
			if (count <= 0) {
		    	jQuery(document.body).find('#loading-indicator-' + indicatorID ).remove();
				jQuery(document.body).find('#loading-indicator-' + indicatorID + '-overlay' ).remove();
				window.clearInterval(countdown);
			}else{
                try {
                    if (jQuery.cookie("IS_EXPORT_COMPLETE") && jQuery.cookie("IS_EXPORT_COMPLETE") != null && jQuery.cookie("IS_EXPORT_COMPLETE") == "true") {
                        jQuery.removeCookie("IS_EXPORT_COMPLETE");  
						jQuery(document.body).find('#loading-indicator-' + indicatorID ).remove();
						jQuery(document.body).find('#loading-indicator-' + indicatorID + '-overlay' ).remove();
                        window.clearInterval(countdown);
                    }
                } 
                catch (e) {
                
                }
			}
		    count--;
		}, 1000);
		
		// Show the overlay
		jQuery(overlayDiv).show();
		jQuery(overlayDiv).css('visibility', 'visible');
		// Show the loading indicator
		jQuery(loadingDiv).show();
		jQuery(loadingDiv).css('visibility', 'visible');
		
		// afterShow callback
		if ( typeof(settings.afterShow) == 'function' ) {
			settings.afterShow( callback_options );
		}
		
        setTimeout(function(){
            $form = jQuery('#' + form);
			var actionUrl = $form.attr('action');
            $form.attr('action', url);
            $form.submit();
			$form.attr('action', actionUrl);
        }, 1000);
		return this;
    };
    