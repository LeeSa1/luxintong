$(function() {
	var base = "/conference";
    var id = $.cookie('id');
    console.log(id);
    $.post(base+"/app/common!readPlanList",{
    	"plan.category":"0"
    },function(data){
    	data=JSON.parse(data);
    	console.log(data);
    	if (data.status=="success"){
    		var list = new Array();
    		for (i=0;i<data.message.length;i++){
    			var dataObj = data.message[i];
    			var fromtime,totime,guest,head,dock,escort;
    			var guestList = new Array();
    			if (dataObj.fromtime!="" && dataObj.totime!=""){
    				fromtime = dataObj.fromtime;
    				totime = dataObj.totime;
    			}else if (dataObj.fromtime!="" && dataObj.totime==""){
    				fromtime = dataObj.fromtime;
    				totime = "";
    			}else if (dataObj.fromtime=="" && dataObj.totime!=""){
    				fromtime = dataObj.totime;
    				totime = "";
    			}
    			
    			if (data.message[i].fromdate!=null && data.message[i].todate!=null){
    				for (j=data.message[i].fromdate.time;j<=data.message[i].todate.time;j+=86400000){
    					var obj=new Object();
    					obj.date=j;
    					obj.fromtime=fromtime;
    					obj.totime=totime;
    					
    					obj.content=dataObj.content;
    					
    					obj.location=dataObj.location;
    					
    					list.push(obj);
    				}
    			}else if (data.message[i].fromdate!=null && data.message[i].todate==null){
    				var obj=new Object();
    				obj.date=dataObj.fromdate.time;
    				obj.fromtime=fromtime;
					obj.totime=totime;
					
					obj.content=dataObj.content;
					
					obj.location=dataObj.location;
					
					list.push(obj);
    			}else if (data.message[i].fromdate==null && data.message[i].todate!=null){
    				var obj=new Object();
    				obj.date=dataObj.todate.time;
    				obj.fromtime=fromtime;
					obj.totime=totime;
					
					obj.content=dataObj.content;
					
					obj.location=dataObj.location;
					
					list.push(obj);
    			}
    		}
    		console.log(list);
    		list.sort(function(a,b){
    			var index;
    			if (a.date!=b.date){
    				index = a.date-b.date;
    			}else{
    				var afrom=parseFloat(a.fromtime.substring(0,5).replace(":","."));
    				var bfrom=parseFloat(b.fromtime.substring(0,5).replace(":","."));
    				index = afrom-bfrom;
    			}
    			return index;
    		});
    		console.log(list);
    		var index=0;
    		var listday=new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六","星期日")
    		for (i=0;i<list.length;i++){
    			if (i==0||list[i].date-list[i-1].date>0){
    				index=0;
    			}else{
    				index++;
    			}
    			if (index==0){
    				var date=new Date(list[i].date);
    				$("table tbody").append('<tr class="tr-bg"><td colspan="3">'+date.getFullYear()+"年"+(date.getMonth()+1)+"月"+date.getDate()+"日，"+listday[date.getDay()])
    			}
    			var time="";
    			if (list[i].fromtime!="" && list[i].totime!=""){
    				/*var timefrom=toAMPM(list[i].fromtime);
    				var timeto=toAMPM(list[i].totime);*/
    				var timefrom=list[i].fromtime;
    				var timeto=list[i].totime;
    				if (timefrom.split(":").length>2){
    					timefrom = timefrom.substring(0,timefrom.length-3);
    				}
    				if (timeto.split(":").length>2){
    					timeto = timeto.substring(0,timeto.length-3);
    				}
    				time=timefrom+"-"+timeto;
    			}else{
    				//var timefrom=toAMPM(list[i].fromtime);
    				var timefrom=list[i].fromtime;
    				if (timefrom.split(":").length>2){
    					timefrom = timefrom.substring(0,timefrom.length-3);
    				}
    				time=timefrom;
    			}
    			var locationList=list[i].location.split(",");
    			var location=locationList.join("<br>");
    			var html = "<tr>"
    				     + "<td style='width:24%;text-align:center;vertical-align:middle;'>"+time+"</td>"
    				     + "<td style='vertical-align:middle;'>"+"<span>"+list[i].content+"</span>"+"</td>"
    				     + "<td style='text-align:center;'>"+location+"</td>"
    				     + "</tr>";
    			
    			console.log(html);
    			$("table tbody").append(html);
    		}
    	}
    });
});

function toAMPM(time){
    var times = time.split(":");
    if (parseInt(times[0],10)<12){
    	times[0]="上午"+parseInt(times[0],10);
    }else if (parseInt(times[0],10)==12){
    	times[0]="中午"+times[0];
    }else if (parseInt(times[0],10)>12 && parseInt(times[0],10)<18){
    	var ampmTime = parseInt(times[0],10)-12;
    	times[0]="下午"+ampmTime;
    }else if (parseInt(times[0],10)>17){
    	var ampmTime = parseInt(times[0],10)-12;
    	times[0]="晚上"+ampmTime;
    }
    var result = times.join(":");
    return result;
}