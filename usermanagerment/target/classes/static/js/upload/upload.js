
/**
 * 附件上传
 * @param {Object} id
 * @param {Object} table
 */
function upload(instanceId,instanceTable){
	var url = base + '/sysmgr/attachment!uploadPage.action?instanceId=' + instanceId + '&instanceTable=' + instanceTable ;
    //使用art
    art.dialog.open(url, {
        width: 650,
        height: 450,
        drag: true,
        resize: false,
        fixed: false,
        lock: true,
        background: '#000', // 背景色
        opacity: 0.7, // 透明度
        title: '上传附件',
        close: function(){
			window.location.reload(true);
        }
    });
}



/**
 * 附件导入
 * @param {Object} uploadHandler    ------ ExcelFileHandler
 * @param {Object} param   -----  id=cccc&name=ddd
 * 
 * 利用页面传入参数，在FileHandlerConstant找到对应的Handler进行上传操作。
 */
function importFile ( uploadHandler, param ){
    var uri = base + '/sysmgr/attachment!importPage.action?UploadHandler=' + uploadHandler + "&" + param ;
    //使用art
    art.dialog.open(uri, {
        width: 650,
        height: 450,
        drag: true,
        resize: false,
        fixed: false,
        lock: true,
        background: '#000', // 背景色
        opacity: 0.7, // 透明度
        title: '导入附件',
        close: function(){
        
        }
    });
}

