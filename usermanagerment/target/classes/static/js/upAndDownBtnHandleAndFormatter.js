function setUpbtnAndDownbtn() {
    var upLength;
    upLength = $("table").bootstrapTable("getData").length;
    // 上移
    $(".upbtn").each(function () {// 遍历上移按钮
        $(this).click(function () {// 页面点中的上移按钮对象
            var $tr = $(this).parents("tr");// 得到当前点中按钮的祖先元素 tr
            // console.log($tr.index());
            $tr.prev().before($tr);// 移动当前tr元素内容到其前一个tr｛$tr.prev()得到点击按钮所在tr前面一个tr}的前面
            /* 移动到最第二个时 点击上移按钮对象隐藏 */
            if ($tr.index() == 2) {
                $(this).hide();// 隐藏点击的按钮 为上移按钮
                $tr.next().find(".upbtn").show();// tr后面的第一个同类元素tr(开始为页面第一个
                // 上移为隐藏状态)后代中的所有class 为
                // .upbtn对象显示
            }
            /* 该行移动到倒数第二行时 自己的下移按钮显示 下面的tr中下移按钮隐藏 */
            if ($tr.index() == upLength - 2) {
                $tr.find(".downbtn").show();// tr后代中的class 为 .downbtn对象显示
                $tr.next().find(".downbtn").hide();// tr后面的同类元素tr(开始为页面最后一个
                // 下移为隐藏状态)后代中所有class 为
                // .downbtn对象隐藏
            }
        });
    });
    // 下移
    $(".downbtn").each(function () {
        $(this).click(function () {
            var $tr = $(this).parents("tr");
            // console.log($tr);
            $tr.next().after($tr);
            if ($tr.index() == 3) {
                $tr.find(".upbtn").show();
                $tr.prev().find(".upbtn").hide();
            }
            if ($tr.index() == upLength - 1) {
                $(this).hide();
                $tr.find(".upbtn").show();
                $tr.prev().find(".downbtn").show();
            }
        });
    });

}

//文书序号
function numberFormatter(value, row, index) {
    return ++index;
}

//附件新名称和附件文书id
var newName;
var caseDocumentId;

//文书名称和录入状态
function nameFormatter(value, row) {
    if (row.id == 8 || row.id == 122 || row.id == 137) {
        //判断当前附加材料文书是否录入 录入了读取新名称 没有录入 读取文书列表中名称
        $.ajax({
            url: "/luzheng/app/case!readCaseDocumentByCaseIdAndDocType",
            data: {
                "caseDocument.caseId": caseId,
                "caseDocument.docType": row.id
            },
            async: false,
            success: function (data) {
                data = JSON.parse(data);
                if (data.message != null) {
                    caseDocumentId = data.message.id;
                    //本案件附加材料新名称
                    $.ajax({
                        url: "/luzheng/app/upload!getMaterialNewNameByCaseIdAndCaseDocId",
                        async: false,
                        data: {
                            "caseDocument.caseId": caseId,
                            "caseDocument.id": caseDocumentId
                        },
                        success: function (data) {
                            data = JSON.parse(data);
                            newName = data.message.newName;
                        }
                    });
                } else {
                    newName = row.name;//没有录入 显示文书列表中默认的名称
                }
            }
        });
        if ($.inArray(row.id, docArray) != -1) {
            return newName + '<i class="icon-ok"></i>';
        } else {
            return row.name;
        }
    } else {
        if ($.inArray(row.id, docArray) != -1) {
            return row.name + '<i class="icon-ok"></i>';
        } else {
            return row.name;
        }
    }
}

//文书操作
function handleFormatter(value, row) {
    if (row.id == 8 || row.id == 122 || row.id == 137) {
        return '<a href="'
            + row.inputHtml
            + '?caseId='
            + caseId
            + '&docType='
            + row.id
            + '&content='
            + newName
            + '" class="tip" title="编辑"><i class="ico-edit info"></i></a>'
            + '<a href="'
            + row.printHtml
            + '?caseId='
            + caseId
            + '&docType='
            + row.id
            + '&content='
            + newName
            + '" class="tip" title="打印" onclick=""><i class="ico-print info"></i></a>'
            + '<a href="javascript:void(0);" onclick="del('
            + row.id
            + ')" class="tip" title="删除"><i class="ico-trash"></i></a>';
    } else {
        return '<a href="'
            + row.inputHtml
            + '?caseId='
            + caseId
            + '&docType='
            + row.id
            + '&type='
            + row.id
            + '&content='
            + row.name
            + '" class="tip" title="编辑"><i class="ico-edit info"></i></a>'
            + '<a href="'
            + row.printHtml
            + '?caseId='
            + caseId
            + '&docType='
            + row.id
            + '&type='
            + row.id
            + '&content='
            + row.name
            + '" class="tip" title="打印" onclick=""><i class="ico-print info"></i></a>'
            + '<a href="javascript:void(0);" onclick="del('
            + row.id
            + ')" class="tip" title="删除"><i class="ico-trash"></i></a>';
    }
}

//删除文书  根据案件id与文书类型的编号
function del(docType) {
    if (window.confirm("确定删除此文书")) {
        $
            .ajax({
                url: "/luzheng/app/case!deleteCaseDocumentByCaseIdAndDocType",
                data: {
                    "caseId": caseId,
                    "docType": docType
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.status == "success") {
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                    $("#table").bootstrapTable("refresh");
                }
            });
    }
}

//获取已经编写的文书
function getOreadlyEdit() {
    //获取已经编辑的文书
    $.ajax({
        url: "/luzheng/app/case!getAllCaseDcoumentByCaseId",
        data: {
            "caseId": caseId
        },
        async: false,
        success: function (data) {
            data = JSON.parse(data);
            if (data.message.length > 0) {
                for (i = 0; i < data.message.length; i++) {
                    docArray.push(data.message[i].docType);
                }
            }
        }
    });
}

//顺序上调与下调
function orderFormatter(value, row, index) {
    var upIcon = '<span class="upbtn"><i class="ico-arrow-up"></i></span>';
    var dounIcon = '<span class="downbtn"><i class="ico-arrow-down"></i></span>'
    if (index == 0 || index == 1) {
        return null;
    }
    if (index == 2) {
        upIcon = '<span class="upbtn" style="display: none;"><i class="ico-arrow-up"></i></span>';
    }
    if (index == $("#table").bootstrapTable("getData").length - 1) {
        dounIcon = '<span class="downbtn" style="display: none;"><i class="ico-arrow-down"></i></span>';
    }
    return upIcon + dounIcon;
}