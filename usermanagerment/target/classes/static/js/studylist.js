﻿

$(".content .remore").each(function() {
	if($(window).width() > 1080) {
		var maxwidth = 60;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	} else {
		var maxwidth = 50;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	}
});

$('.img-box').find('img').each(function() {
	if($(this).attr('src') == "") {
		$(this).parents('.img-box').css('width', '0');
		$(this).parents('.img-box').next().css({
			'width': '100%',
			'padding-left': '0'
		});
	}
});

function smart_img($imgdom) {
	var w = $imgdom.width();
	var h = $imgdom.height();
	if(w > h) {
		$imgdom.css({
			'height': '100%'
		});
		var left = ($imgdom.width() - $imgdom.parent().width()) / 2 * (-1);
		$imgdom.css({
			'margin-left': left + 'px'
		});
		if($imgdom.width() < $imgdom.parent().width()) {
			$imgdom.css({
				'width': '100%',
				'height': 'auto',
				'margin-left': '0'
			});
		}
	} else {
		$imgdom.css({
			'width': '100%'
		});
	}
}

$(function() {
	$img = $('.img-box').find('img');
	smart_img($img);
    $('.right .head>i').hide();
	$('#wrapper .right2 .head>i').on('click', function() {
		$('#wrapper .right2 .content').slideToggle();
		$('#wrapper .right2>ul').fadeToggle();
	})

	$('#wrapper .right2 .head>i').toggle(
		function() {
			$(this).attr('class', 'glyphicon glyphicon-chevron-up');
		},
		function() {
			$(this).attr('class', 'glyphicon glyphicon-chevron-down')
		}
	)
})

$(function() {
	$table = $('.left .content>ul');
	$pageli = $('.page');
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var type = getQueryString('type');
	var channel = getQueryString('channel');
	if (!channel) channel = '';
	var subject = getQueryString('subject');
	if (!subject) subject = '';
	$('.top li a').text('');
	$('.top li a').eq(0).text('学习');
	$('.top li a').eq(0).attr('href','study.html');
	if (type == 'subresource'){
		var sChannel = '';
		if (channel == 0){
			sChannel = '病例';
		}else if (channel == 1){
			sChannel = '指南';
		}else if (channel == 2){
			sChannel = '文献';
		}
		$('.right .content ul').html('');
	    $('.right2 .content ul').html('');
		if (sChannel == ''){
			$('.top li i').eq(0).hide();
		}else{
			$('.top li i').eq(0).show();
		}
		$('.top li a').eq(1).text(sChannel);
		$('.subresource').removeAttr('style');
		$('.resource').hide();
		$('.resource2').hide();
		$('.policy').hide();
		$('.policy2').hide();
		$('.subresource .head span').text(sChannel+'导航');
		$('.subresource2 ul').eq(0).find('li').eq(0).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=内科');
	    $('.subresource2 ul').eq(0).find('li').eq(1).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=外科');
	    $('.subresource2 ul').eq(0).find('li').eq(2).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=妇产科');
	    $('.subresource2 ul').eq(0).find('li').eq(3).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=儿科');
	    $('.subresource2 ul').eq(0).find('li').eq(4).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=皮肤性病科');
		$('.subresource2 ul').eq(0).find('li').eq(5).find('a').attr('href','studylist.html?type=subresource&channel='+channel+'&subject=临床其他');
		$('.top li a').eq(1).attr('href','studylist.html?type=subresource&channel='+channel);
		if (subject != ''){
			$('.top li i').eq(1).show();
			$('.top li a').eq(2).text(decodeURI(subject));
			$('.top li a').eq(2).attr('href','studylist.html?type=subresource&channel='+channel+'&subject='+subject);
		}else{
			$('.top li i').eq(1).hide();
		}
		$('.left .head span').text(decodeURI(subject)+sChannel);
		$.post(base+"/app/common!readStudySubResList",{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
			   subject:subject
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=subresource&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	}else if (type == 'resource'){
		$('.resource').removeAttr('style');
		$('.subresource').hide();
		$('.subresource2').hide();
		$('.policy').hide();
		$('.policy2').hide();
		$('.top li i').eq(0).show();
		$('.top li a').eq(1).text('资源');
		$('.top li a').eq(1).attr('href','studylist.html?type=resource');
		if (channel != ''){
		var sChannel = '';
	    if (channel == "0"){
			sChannel = '书籍';
		}else if (channel == "1"){
			sChannel = '视频';
		}else if (channel == "2"){
			sChannel = '课件';
		}else if (channel == "3"){
			sChannel = '考试';
		}else if (channel == "4"){
			sChannel = '论文';
		}
		if (sChannel == ''){
			$('.top li i').eq(1).hide();
		}else{
			$('.top li i').eq(1).show();
		}
		$('.top li a').eq(2).text(sChannel);
		$('.top li a').eq(2).attr('href','studylist.html?type=resource&channel='+channel);
		var sSubject = '';
		if (subject != ''){
			if (channel<3){
			if (subject == 1){
			sSubject = '-中医';
		}else if (subject == 2){
			sSubject = '-外科';
		}else if (subject == 3){
			sSubject = '-医技科';
		}else if (subject == 4){
			sSubject = '-精神科';
		}else if (subject == 5){
			sSubject = '-医学保健';
		}else if (subject == 6){
			sSubject = '-儿科';
		}else if (subject == 7){
			sSubject = '-妇产科';
		}else if (subject == 8){
			sSubject = '-内科';
		}else if (subject == 9){
			sSubject = '-医药学';
		}else if (subject == 10){
			sSubject = '-男科与生殖医学';
		}else{
			sSubject = '-其他';
		}
			}else{
				if (subject == 0){
			sSubject = '-执业医师';
		}else if (subject == 1){
			sSubject = '-医学考研';
		}else if (subject == 2){
			sSubject = '-药师考试';
		}else if (subject == 3){
			sSubject = '-口腔医学考试';
		}else{
			sSubject = '-其他';
		}
			}
		}
		$('.left .head span').text(sChannel+sSubject);
	}else{
		$('.left .head span').text('资源');
	}
	    $.post(base+"/app/common!readStudyResourceList",{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
			   subject:subject
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (!obj.pic||obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   //var link = 'detail.html?type=policy&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li data-id="'+obj.id+'" data-channel="'+obj.channel+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><div class="img-box" '+imgStyle+'><a href="javascript:void(0)"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="javascript:void(0)">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	$('.left .content').on('click','li div ul li:nth-of-type(1)',function() {
		var channel = $(this).parent().parent().parent().attr('data-channel');
		if (channel == '书籍' || channel == '视频'){
			if ($(this).parent().parent().parent().attr('data-pic')==undefined||$(this).parent().parent().parent().attr('data-pic')==''){
			$('.modal-box img').eq(0).attr('src','img/default.png');
		}else{
			$('.modal-box img').eq(0).attr('src',$(this).parent().parent().parent().attr('data-pic'));
		}
		$('.modal-box li').eq(0).find('span').text($(this).parent().parent().parent().attr('data-name'));
		$('.modal-box li').eq(1).find('span').text($(this).parent().parent().parent().attr('data-author'));
		$('.modal-box li').eq(2).find('span').text($(this).parent().parent().parent().attr('data-press'));
		var subject = $(this).parent().parent().parent().attr('data-cat');
		var sSubject = '';
		console.log(subject);
		if (subject == 1){
			sSubject = '中医';
		}else if (subject == 2){
			sSubject = '外科';
		}else if (subject == 3){
			sSubject = '医技科';
		}else if (subject == 4){
			sSubject = '精神科';
		}else if (subject == 5){
			sSubject = '医学保健';
		}else if (subject == 6){
			sSubject = '儿科';
		}else if (subject == 7){
			sSubject = '妇产科';
		}else if (subject == 8){
			sSubject = '内科';
		}else if (subject == 9){
			sSubject = '医药学';
		}else if (subject == 10){
			sSubject = '男科与生殖医学';
		}else{
			sSubject = '其他';
		}
		console.log(sSubject);
		$('.modal-box li').eq(4).find('span').text(sSubject);
		$('.modal-box li').eq(6).find('span').text($(this).parent().parent().parent().attr('data-xcode'));
		$('.modal-box p').text($(this).parent().parent().parent().attr('data-desc'));
		$('.modal-box button').eq(0).attr('onClick','openurl("'+$(this).parent().parent().parent().attr('data-link')+'","resource",'+$(this).parent().parent().parent().attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box').fadeIn(100)
		$('.modal-box').find('img').each(function() {
			$(this).height($(this).width() / 3 * 4);
			var m = $(this).height() / 2 * (-1);
			$(this).css({
				'top': '50%',
				'marginTop': m
			})
		})
		}else{
			$('.modal-box2 li').eq(0).find('h3').text($(this).parent().parent().parent().attr('data-name'));
		$('.modal-box2 li').eq(1).find('span').text($(this).parent().parent().parent().attr('data-author'));
		console.log($(this).parent().parent().parent().attr('data-time'));
		var time = new Date(parseInt($(this).parent().parent().parent().attr('data-time')));
		$('.modal-box2 li').eq(2).find('span').text(time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate());
		$('.modal-box2 li').eq(3).find('span').text($(this).parent().parent().parent().attr('data-xcode'));
		$('.modal-box2 p').text($(this).parent().parent().parent().attr('data-desc'));
		$('.modal-box2 button').eq(0).attr('onClick','openurl("'+$(this).parent().parent().parent().attr('data-link')+'","resource",'+$(this).parent().parent().parent().attr('data-id')+')');
		$('#modal').fadeIn(100).find('.modal-box2').fadeIn(100)
		}
	})
	$('.modal-box , .modal-box2').on('click','.btn-danger', function() {
		$('#modal').fadeOut(100).find('.modal-box, .modal-box2').fadeOut(100)
	})
	}else if (type == 'policy'){
		if (!channel) channel = '';
		var sChannel = '';
	if (channel != ''){
	    if (channel == "0"){
			sChannel = '法律法规';
		}else if (channel == "1"){
			sChannel = '委令';
		}else if (channel == "2"){
			sChannel = '委厅文件';
		}
		$('.left .head span').text('政策-'+sChannel);
	}else{
		$('.left .head span').text('政策');
	}
	$('.right .content ul').html('');
	$('.policy2 .head i').hide();
	$('.policy').removeAttr('style');
	$('.subresource').hide();
	$('.subresource2').hide();
	$('.resource').hide();
	$('.resource2').hide();
	$('.policy2 ul').eq(0).find('li').eq(0).find('a').attr('href','studylist.html?type=policy&channel=0');
	$('.policy2 ul').eq(0).find('li').eq(1).find('a').attr('href','studylist.html?type=policy&channel=1');
	$('.policy2 ul').eq(0).find('li').eq(2).find('a').attr('href','studylist.html?type=policy&channel=2');
		$('.top li i').eq(0).show();
	    $('.top li a').eq(1).text('政策');
		$('.top li a').eq(1).attr('href','studylist.html?type=policy');
		console.log(channel);
		console.log(sChannel);
		if (sChannel == ''){
			$('.top li i').eq(1).hide();
		}else{
			$('.top li i').eq(1).show();
		}
		$('.top li a').eq(2).text(sChannel);
		$('.top li a').eq(2).attr('href','studylist.html?type=policy&channel='+channel);
	    $.post(base+"/app/common!readStudyPolicyList",{
		       "user.token":token,
			   "user.id":id,
			   channel:channel,
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (!obj.pic||obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=policy&id='+obj.id;
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+obj.pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	}
	$('.left .content>ul').on('click','li .info div:nth-of-type(2)',function(){
        $('#discuss').attr('data-id',$(this).attr('data-id'));
		toDiscuss();
	});
	$('#discuss .btn-danger').on('click',function(){
				$('#discuss').slideUp(300);
			})
			
			$('#discuss .btn-info').on('click', function(){
				var content = $('#discuss .user-box textarea').val();
				if (content == ''){
					alert('请输入心得！');
					return;
				}else{
					$('#discuss .user-box textarea').val('');
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = getQueryString('type');
	            var resId = $('#discuss').attr('data-id');
				$('.comment>ul').html('');
				$.post(base+'/app/common!createComment',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "comment.content":content
					   },function(data){
						   console.log(data);
						   listDiscuss(1);
					   });
			})
	nextSubject(0);
})

function doSearch(){
	$table = $('.left .content>ul');
	$pageli = $('.page');
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var pageindex = $('#pageindex').val();
	console.log(pageindex);
	var s = $('.search #search-input').val();
	var type = getQueryString('type');
	var channel = getQueryString('channel');
	if (!channel) channel = '';
	var subject = getQueryString('subject');
	if (!subject) subject = '';
	$('.page .title').remove();
	if (type == 'subresource'){
		$.post(base+"/app/common!readStudySubResList",{
		       "user.token":token,
			   "user.id":id,
			    channel:channel,
				name:s,
				subject:subject,
			   "page.index":pageindex
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   if (s != ''){
				   $('.top li a').eq(3).text('搜索关键词：'+s+'，共'+data.message.total+'条搜索结果');
			   }else{
				   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   }
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=subresource&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	}else if (type == 'resource'){
	$.post(base+"/app/common!readStudyResourceList",{
		       "user.token":token,
			   "user.id":id,
			    channel:channel,
				subject:subject,
				name:s,
			   "page.index":pageindex
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   if (s != ''){
				   $('.top li a').eq(3).text('搜索关键词：'+s+'，共'+data.message.total+'条搜索结果');
			   }else{
				   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   }
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (!obj.pic||obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   //var link = 'detail.html?type=policy&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li data-id="'+obj.id+'" data-channel="'+channel+'" data-name="'+obj.name+'" data-author="'+obj.author+'" data-cat="'+obj.subjectId+'" data-press="'+obj.press+'" data-pic="'+pic+'" data-desc="'+obj.desc+'" data-link="'+obj.link+'" data-xcode="'+obj.xcode+'" data-time="'+obj.createdAt+'"><div class="img-box" '+imgStyle+'><a href="javascript:void(0)"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="javascript:void(0)">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	}else if (type == 'policy'){
		$.post(base+"/app/common!readStudyPolicyList",{
		       "user.token":token,
			   "user.id":id,
			    channel:channel,
				name:s,
			   "page.index":pageindex
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   if (s != ''){
				   $('.top li a').eq(3).text('搜索关键词：'+s+'，共'+data.message.total+'条搜索结果');
			   }else{
				   $('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   }
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (!obj.pic||obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=policy&id='+obj.id;
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+obj.pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
		
	}
}

function toDiscuss(){
	            listDiscuss(1);
				$('#discuss').fadeIn(300);
}

function listDiscuss(pageindex){
	            var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = getQueryString('type');
	            var resId = $('#discuss').attr('data-id');

				$('.comment>ul').html('');
				$('#discuss .page .title').remove();
				console.log(type+','+resId);
				$.post(base+'/app/common!readCommentList',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "page.index":pageindex
					   },function(data){
						   console.log(data);
						   data = JSON.parse(data);
			               if (data.status == 'success'){
							   for (i=0;i<data.message.resultList.length;i++){
								   var obj = data.message.resultList[i];
								   var time = new Date(obj.createdAt);
								   var photo = obj.user.photo;
								   if (photo.indexOf('/upload')==0){
									   photo = base+photo;
								   }
								   var html = '<li><div><img src="'+photo+'" /></div>'
								            + '<div><p><b>'+obj.user.name+': </b><span>'+obj.content+'</span></p>'
								            + '<p>'+time.getFullYear()+'-'+fillzero((time.getMonth()+1))+'-'+fillzero(time.getDate())+' '+fillzero(time.getHours())+':'+fillzero(time.getMinutes())+':'+fillzero(time.getSeconds())+'</p></div></li>';
								   $('.comment>ul').append(html);
							   }
							   $table = $('#discuss .comment>ul');
				               $pageli = $('#discuss .page');
				               $myson = $('#discuss .comment>ul>li')
				               mypage($table, 10,data.message.index-1,data.message.resultList.length, $pageli, $myson);
						   }
					   });

}

function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  r[2]; return null;
}

function fillzero(s){
			if (parseInt(s)<10){
				return '0'+s;
			}else{
				return s;
			}
		}

function nextSubject(index){
	var base = "/hoslibrary";
	var type = getQueryString('type');
	var channel = getQueryString('channel');
	if (!channel) channel = '';
	if (type == 'subresource'){
	$.post(base+"/app/common!readSubjectList",{
		       pid:index+1
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
				   for (i=0;i<data.message.length;i++){
					   var obj = data.message[i];
					   var html = '<li><a href="studylist.html?type=subresource&channel='+channel+'&subject='+obj.name+'">'+obj.name+'</a></li>';
					   $('.subresource .content ul').eq(index).append(html);
					   $('.subresource2 .content ul').eq(index).append(html);
				   }
				   if (index < 5){
					   nextSubject(index+1);
				   }
			   }
		   });
	}else if (type == 'resource'){
		$('.resource .content h4').eq(index).find('a').attr('href','studylist.html?type=resource&channel='+index);
	$('.resource2 .content h4').eq(index).find('a').attr('href','studylist.html?type=resource&channel='+index);
	for (i=0;i<$('.resource .content ul').eq(index).find('li').length;i++){
		if (index < 3){
		    $('.resource .content ul').eq(index).find('li').eq(i).find('a').attr('href','studylist.html?type=resource&channel='+index+'&subject='+(i+1));
			$('.resource2 .content ul').eq(index).find('li').eq(i).find('a').attr('href','studylist.html?type=resource&channel='+index+'&subject='+(i+1));
		}else{
			$('.resource .content ul').eq(index).find('li').eq(i).find('a').attr('href','studylist.html?type=resource&channel='+index+'&subject='+i);
			$('.resource2 .content ul').eq(index).find('li').eq(i).find('a').attr('href','studylist.html?type=resource&channel='+index+'&subject='+i);
		}
	}
	if (index < 4){
		nextSubject(index+1);
	}
	}else if (type == 'policy'){
		$('.policy .content h4').eq(index).find('a').attr('href','studylist.html?type=policy&channel='+index);
	if (index < 2){
		nextSubject(index+1);
	}
	}
}

function searchText(){
	//if ($('.search #search-input').val()!=''){
		$('#pageindex').val(1);
		$('.page .title').remove();
		doSearch();
	//}
}

function openurl(url,resType,resId){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	$.post(base+"/app/common!logView",{
		                "user.token":token,
			            "user.id":id,
						"viewlog.resType":resType,
			            "viewlog.resId":resId
		            },function(data){
						console.log(data);
					}
				);
    window.open(base+'/sysmgr/openresource?url='+url);
}

function nofind(obj){
	obj.src="img/default.png";
}