
String.prototype.replaceAll = function(s1,s2) { 
    return this.replace(new RegExp(s1,"gm"),s2); 
}

/*
	 * 字符串去掉前后空格
	 * */
String.prototype.trim= function(){  
    // 用正则表达式将前后空格  
    // 用空字符串替代。  
    return this.replace(/(^\s*)|(\s*$)/g, "");  
}

/**
 * 页面初始化通用方法
 * @param {Object} "[title]"
 */
$(document).ready(function() {

	//优化title显示效果
	$("[title]:not(select)").each(function() {
		var $this = $(this);
		var title = $this.attr("title");
		$this.unbind("title");
		$this.attr("title", null);
		$this.poshytip({
			content : title
		});
	});
	
	//字符串显示效果
	$("[class*=substrlength]").each(function() {
		var $this = $(this);
		var srcstr = $this.html();
		var length = 0;
		if($this.metadata() && $this.metadata().substrlength){
			length = $this.metadata().substrlength;
		}
		var str = substringfun(srcstr,length);
		$this.html(str);
		$this.poshytip({
			content : srcstr
		});
	});
	
});


var shop = {
	base: "/store",
	currencySign: "￥",// 货币符号
	currencyUnit: "元",// 货币单位
	priceScale: "2",// 商品价格精确位数
	priceRoundType: "roundHalfUp",// 商品价格精确方式
	orderScale: "2",// 订单金额精确位数
	orderRoundType: "roundHalfUp"// 订单金额精确方式
};

/**
 * 打开新的窗口
 * @param {Object} url
 */
function openLink(url){
	var linkForm = $('<form id="linkForm" style="display:none;" method="post" target="_blank"></form>');
	$("body").prepend(linkForm);
	linkForm.attr('action',url);
	linkForm.submit();
	linkForm.remove();	
}

/**
 * 使用原来的窗口打开
 * @param {Object} url
 */
function openSelfLink(url){
	var linkForm = $('<form id="linkForm" style="display:none;" method="post" target="_self"></form>');
	$("body").prepend(linkForm);
	linkForm.attr('action',url);
	linkForm.submit();
	linkForm.remove();	
}

/**
 * 截取字符串
 * @param {Object} src
 * @param {Object} length
 */
function substringfun(src,length){
	if(src == null){
		return "";
	}else {
		var strlength = src.length;
		if(strlength <= length){
			return src;
		}
		else{
			var str = src.substring(0,length) + "...";
			return str;
		}
	}
}

/**
 * 获取上下文路径
 */
function getBase(){
    var contextPath = document.location.pathname;
    var index = contextPath.substr(1).indexOf("/");
    contextPath = contextPath.substr(0, index + 1);
    return contextPath;
}

//获取cookie
function getCookie(name){
	var arrStr = document.cookie.split("; ");
	for(var i = 0;i < arrStr.length;i ++){
		var temp = arrStr[i].split("=");
		if(temp[0] == name) return unescape(temp[1]);
   }
}
			
//添加cookie cookie名/值/过期时间
function addCookie(name,value,hours){
    var str = name + "=" + escape(value);
    if(hours > 0){  //为0或为空时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        date.setTime(date.getTime() + hours*3600*1000);
        str += "; expires=" + date.toGMTString()+";path=/";
   }
   document.cookie = str;
}

//删除cookie
function delCookie(name){
   document.cookie = name+"=;expires="+(new Date(0)).toGMTString();
}

//数组indexOf方法扩展
if(!Array.indexOf)
{
    Array.prototype.indexOf = function(obj)
    {               
        for(var i=0; i<this.length; i++)
        {
            if(this[i]==obj)
            {
                return i;
            }
        }
        return -1;
    }
}	

if(!Array.prototype.lastIndexOf){

	 Array.prototype.lastIndexOf = function(v)
	 {
		 for(var i=this.length-1;i>=0;i--)
		 {
			 if(this[i]==v) 
			 {
				 return i;
			 }
		 }
		 return -1;
	 }
}

if(!Array.prototype.remove){
	Array.prototype.remove=function(dx)
	{	
		if(isNaN(dx)||dx>this.length){return false;}
		for(var i=0,n=0;i<this.length;i++)
		{
			if(this[i]!=this[dx])
			{
				this[n++]=this[i];
			}
		}
		this.length-=1;
	}
}

if(!Array.prototype.delRepeat){
	Array.prototype.delRepeat=function(){
	 var newArray=[];
	 var provisionalTable = {};
	 for (var i = 0, item; (item= this[i]) != null; i++) {
	        if (!provisionalTable[item]) {
	            newArray.push(item);
	            provisionalTable[item] = true;
	        }
	    }
	    return newArray;
	}
}

String.prototype.format = function(args) {
    var result = this;
    if (arguments.length > 0) {    
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if(args[key]!=undefined){
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                	var reg= new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
}

var amountLtoU = function(num) {
	if (isNaN(num)) {
		return "无效数值！";
	}
	var strOutput = "";
	var strUnit = '仟佰拾亿仟佰拾万仟佰拾元角分';
	num += "00";
	var intPos = num.indexOf('.');
	if (intPos >= 0)
		num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
	strUnit = strUnit.substr(strUnit.length - num.length);
	for (var i = 0; i < num.length; i++)
		strOutput += '零壹贰叁肆伍陆柒捌玖'.substr(num.substr(i, 1), 1)
				+ strUnit.substr(i, 1);
	return strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(
			/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元')
			.replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");
};
