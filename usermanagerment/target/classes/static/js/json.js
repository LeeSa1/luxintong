/*
Copyright (c) 2005 JSON.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
    The global object JSON contains two methods.

    JSON.stringify(value) takes a JavaScript value and produces a JSON text.
    The value must not be cyclical.

    JSON.parse(text) takes a JSON text and produces a JavaScript value. It will
    return false if there is an error.
*/
var JSON = function () {
    var m = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        s = {
            'boolean': function (x) {
                return String(x);
            },
            number: function (x) {
                return isFinite(x) ? String(x) : 'null';
            },
            string: function (x) {
                if (/["\\\x00-\x1f]/.test(x)) {
                    x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
                        var c = m[b];
                        if (c) {
                            return c;
                        }
                        c = b.charCodeAt();
                        return '\\u00' +
                            Math.floor(c / 16).toString(16) +
                            (c % 16).toString(16);
                    });
                }
                return '"' + x + '"';
            },
            object: function (x) {
                if (x) {
                    var a = [], b, f, i, l, v;
                    if (x instanceof Array) {
                        a[0] = '[';
                        l = x.length;
                        for (i = 0; i < l; i += 1) {
                            v = x[i];
                            f = s[typeof v];
                            if (f) {
                                v = f(v);
                                if (typeof v == 'string') {
                                    if (b) {
                                        a[a.length] = ',';
                                    }
                                    a[a.length] = v;
                                    b = true;
                                }
                            }
                        }
                        a[a.length] = ']';
                    } else if (x instanceof Object) {
                        a[0] = '{';
                        for (i in x) {
                            v = x[i];
                            f = s[typeof v];
                            if (f) {
                                v = f(v);
                                if (typeof v == 'string') {
                                    if (b) {
                                        a[a.length] = ',';
                                    }
                                    a.push(s.string(i), ':', v);
                                    b = true;
                                }
                            }
                        }
                        a[a.length] = '}';
                    } else {
                        return;
                    }
                    return a.join('');
                }
                return 'null';
            }
        };
    return {
        copyright: '(c)2005 JSON.org',
        license: 'http://www.crockford.com/JSON/license.html',
/*
    Stringify a JavaScript value, producing a JSON text.
*/
        stringify: function (v) {
            var f = s[typeof v];
            if (f) {
                v = f(v);
                if (typeof v == 'string') {
                    return v;
                }
            }
            return null;
        },
/*
    Parse a JSON text, producing a JavaScript value.
    It returns false if there is a syntax error.
*/
        parse: function (text) {
            try {
                return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(
                        text.replace(/"(\\.|[^"\\])*"/g, ''))) &&
                    eval('(' + text + ')');
            } catch (e) {
                return false;
            }
        }
    };
}();






/*MAP对象，实现MAP功能


接口：

size()     获取MAP元素个数

isEmpty()    判断MAP是否为空

clear()     删除MAP所有元素

put(key, value)   向MAP中增加元素（key, value)

remove(key)    删除指定KEY的元素，成功返回True，失败返回False

get(key)    获取指定KEY的元素值VALUE，失败返回NULL

element(index)   获取指定索引的元素（使用element.key，element.value获取KEY和VALUE），失败返回NULL

containsKey(key)  判断MAP中是否含有指定KEY的元素

containsValue(value) 判断MAP中是否含有指定VALUE的元素

values()    获取MAP中所有VALUE的数组（ARRAY）

keys()     获取MAP中所有KEY的数组（ARRAY）



例子：

var map = new Map();



map.put("key", "value");

var val = map.get("key")

*/



function Map() {

 this.elements = new Array();



 //获取MAP元素个数

 this.size = function() {

     return this.elements.length;

 }



 //判断MAP是否为空

 this.isEmpty = function() {

     return (this.elements.length < 1);

 }



 //删除MAP所有元素

 this.clear = function() {

     this.elements = new Array();

 }



 //向MAP中增加元素（key, value)

 this.put = function(_key, _value) {

     this.elements.push( {

         key : _key,

         value : _value

     });

 }



 //删除指定KEY的元素，成功返回True，失败返回False

 this.remove = function(_key) {

     var bln = false;

     try {

         for (i = 0; i < this.elements.length; i++) {

             if (this.elements[i].key == _key) {

                 this.elements.splice(i, 1);

                 return true;

             }

         }

     } catch (e) {

         bln = false;

     }

     return bln;

 }



 //获取指定KEY的元素值VALUE，失败返回NULL

 this.get = function(_key) {

     try {

         for (i = 0; i < this.elements.length; i++) {

             if (this.elements[i].key == _key) {

                 return this.elements[i].value;

             }

         }

     } catch (e) {

         return null;

     }

 }



 //获取指定索引的元素（使用element.key，element.value获取KEY和VALUE），失败返回NULL

 this.element = function(_index) {

     if (_index < 0 || _index >= this.elements.length) {

         return null;

     }

     return this.elements[_index];

 }



 //判断MAP中是否含有指定KEY的元素

 this.containsKey = function(_key) {

     var bln = false;

     try {

         for (i = 0; i < this.elements.length; i++) {

             if (this.elements[i].key == _key) {

                 bln = true;

             }

         }

     } catch (e) {

         bln = false;

     }

     return bln;

 }



 //判断MAP中是否含有指定VALUE的元素

 this.containsValue = function(_value) {

     var bln = false;

     try {

         for (i = 0; i < this.elements.length; i++) {

             if (this.elements[i].value == _value) {

                 bln = true;

             }

         }

     } catch (e) {

         bln = false;

     }

     return bln;

 }



 //获取MAP中所有VALUE的数组（ARRAY）

 this.values = function() {

     var arr = new Array();

     for (i = 0; i < this.elements.length; i++) {

         arr.push(this.elements[i].value);

     }

     return arr;

 }



 //获取MAP中所有KEY的数组（ARRAY）

 this.keys = function() {

     var arr = new Array();

     for (i = 0; i < this.elements.length; i++) {

         arr.push(this.elements[i].key);

     }

     return arr;

 }

}


/*List对象，实现List功能
接口：
size()     获取MAP元素个数
isEmpty()    判断MAP是否为空
clear()     删除MAP所有元素
put(key, value)   向MAP中增加元素（key, value)
remove(key)    删除指定KEY的元素，成功返回True，失败返回False
get(key)    获取指定KEY的元素值VALUE，失败返回NULL
element(index)   获取指定索引的元素（使用element.key，element.value获取KEY和VALUE），失败返回NULL
containsKey(key)  判断MAP中是否含有指定KEY的元素
containsValue(value) 判断MAP中是否含有指定VALUE的元素
values()    获取MAP中所有VALUE的数组（ARRAY）
keys()     获取MAP中所有KEY的数组（ARRAY）

例子：
var map = new Map();
map.put("key", "value");
var val = map.get("key")

*/



function List() {

 this.elements = new Array();

 //获取List元素个数
 this.size = function() {
     return this.elements.length;
 }

 //判断List是否为空
 this.isEmpty = function() {
     return (this.elements.length < 1);
 }
 
 //删除List所有元素
 this.clear = function() {
     this.elements = new Array();
 }

 //向List中增加元素（key, value)
 this.add = function(_value) {
     this.elements.push(_value);
 }
 
 //获取指定index的元素值VALUE，失败返回NULL
 this.get = function(_index) {
     try {
         return this.elements[eval(_index)];
     } catch (e) {
         return null;
     }
 }

 //删除指定KEY的元素，成功返回True，失败返回False
 this.remove = function(_index) {
     var bln = false;
     try {
         for (i = 0; i < this.elements.length; i++) {
             if (i == _index) {
                 this.elements.splice(i, 1);
                 return true;
             }
         }
     } catch (e) {
         bln = false;
     }
     return bln;
 }
 
  //删除指定value的元素，成功返回True，失败返回False
 this.removeElement = function(_value) {
     var bln = false;
     try {
         for (i = 0; i < this.elements.length; i++) {
             if (this.elements[i] == _value) {
                 this.elements.splice(i, 1);
                 return true;
             }
         }
     } catch (e) {
         bln = false;
     }
     return bln;
 }


 //判断List中是否含有指定KEY的元素
 this.contains = function(_value) {
     var bln = false;
     try {
         for (i = 0; i < this.elements.length; i++) {
             if (this.elements[i] == _value) {
                 bln = true;
             }
         }
     } catch (e) {
         bln = false;
     }
     return bln;
 }

}
