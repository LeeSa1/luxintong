
/*XMLHttpRequest 是一个JavaScript对象
	属性 描述 
	onreadystatechange :状态改变的事件触发器 
	readyState :对象状态(integer): 0 = 未初始化 	1 = 读取中	2 = 已读取	3 = 交互中	4 = 完成 	
	responseText: 服务器进程返回数据的文本版本 
	responseXML :服务器进程返回数据的兼容DOM的XML文档对象 
	status :服务器返回的状态码, 如：404 = "文件未找到" 、200 ="成功" 
	statusText: 服务器返回的状态文本信息 
*/
/*1. XMLHttpRequest对象初始化*/
var http_request = getXMLHttpRequest();
function getXMLHttpRequest() {
    var request = null;
//IE浏览器
    try {
        request = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e) {
        try {
            request = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (e) {
        }
    }
//Mozilla浏览器	
    if (!request && typeof XMLHttpRequest != "undefined") {
        request = new XMLHttpRequest;
    }
    return request;
}
/*2.指定响应处理函数
　 　XMLHttpRequest对成功返回的信息有两种处理方式：
　　 responseText：将传回的信息当字符串使用；
　　 responseXML：将传回的信息当XML文档使用，可以用DOM处理。
*/
function processRequest() {
    if (http_request.readyState == 4) { // 判断对象状态
        if (http_request.status == 200) { // 信息已经成功返回，开始处理信息
            try {
                eval(http_request.responseText);//处理传回的字符串信息
            }
            catch (e) {
            }
        }
    }
}
/*3.发出HTTP请求 */
function sendRequest(URL) {
    if (http_request && http_request.readyState != 0 && http_request.readyState != 4) {
        http_request.abort();
    }
    URL=URL +'&radm='+Math.random();
    URL=encodeURI(URL);
    URL=encodeURI(URL);	
    http_request = getXMLHttpRequest();
    if (http_request) {
        http_request.onreadystatechange = processRequest;
        http_request.open("GET", URL, false);
        http_request.send(null);
    }
}

/*3.发出HTTP请求 */
function sendRequest(URL, callbackMethod) {
    if (http_request && http_request.readyState != 0 && http_request.readyState != 4) {
        http_request.abort();
    }
    if(URL.indexOf("?") > -1){
    	URL=URL +'&radm='+Math.random();
    }else{
    	URL=URL +'?radm='+Math.random();
    }
    URL=encodeURI(URL);
    URL=encodeURI(URL);	
    http_request = getXMLHttpRequest();
    if (http_request) {
        http_request.onreadystatechange = callbackMethod == null ? processRequest : callbackMethod;
        http_request.open("GET", URL, false);
        http_request.send(null);
    }
}
function processRequest(){
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			try {
				eval(http_request.responseText);
			}
			catch (e) {
			
			}
		}
	}
}
/* 公共对象*/
function ObjProperty(code, name, id) {
    this.code = code;
    this.name = name;
    this.id = id;
}
function MenuObject(id, name, code,type,url) {
    this.code = code;
    this.name = name;
    this.id = id;
    this.type = type;
    this.url = url;
}


function sendRequestCall(request,url,callBackFunc){
	request.open("GET",url,true); 
	request.onreadystatechange=callBackFunc;
	request.send(null);
}
