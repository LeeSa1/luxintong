// 日期选择框
var $currentDatePicker;
var datePickerOptions = {
    format: "Y-m-d",
	date: new Date(),
	calendars: 1,
	starts: 1,
	position: "right",
	prev: "<<",
	next: ">>",
	locale: {
		days: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
		daysShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六", "周日"],
		daysMin: ["日", "一", "二", "三", "四", "五", "六", "日"],
		months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		weekMin: ' '
	},
	onBeforeShow: function(){
		$currentDatePicker = $(this);
		var currentDate = $.trim($currentDatePicker.val());
		if (currentDate != "") {
			var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/;
			if(currentDate.match(reg) != null) {
				$currentDatePicker.DatePickerSetDate($currentDatePicker.val(), true);
			}
		}
	},
	onChange: function(formated, dates){
		$currentDatePicker.val(formated);
	}
};
$("input.datePicker").DatePicker(datePickerOptions);

// 重新绑定日期选择框
$.bindDatePicker = function () {
	$("input.datePicker").DatePicker(datePickerOptions);
}