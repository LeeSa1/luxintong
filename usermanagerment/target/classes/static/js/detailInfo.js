$(function() {
	var base = "/conference";
	var userid = GetQueryString("userid");
	console.log(userid);
	$.post(base+"/app/common!readUserInfo",{
 	   "user.id":userid
    },function(data){
    	data=JSON.parse(data);
    	console.log(data);
    	if (data.status=="success"){
    		var image=data.message.image;
    		if (image.indexOf("/upload")==0){
    			image=base+image;
    		}
    		$(".thumbnail img").attr("src",image);
    		$(".thumbnail h3").text(data.message.name);
    		$(".thumbnail p").eq(0).text(data.message.office+data.message.job);
    		$(".thumbnail p").eq(1).text(data.message.phone);
    		var contact = "";
    	    var reception = "";
    	    if (data.message.contactUser!=null){
    	    	contact = "联系人："+data.message.contactUser.name+" "+data.message.contactUser.phone;
    	    }
    	    $(".thumbnail p").eq(2).text(contact);
    	    if (data.message.receptionUser!=null){
    	    	reception = "接待人："+data.message.receptionUser.name+" "+data.message.receptionUser.phone;
    	    }
    	    $(".thumbnail p").eq(3).text(reception);
    	    $(".resume p").eq(0).html(data.message.resume);
    	    $(".resume p").eq(1).text(data.message.note);
    	}
    });
});

function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}