
/**
 * iframe 打开方式，会自动动态调整iframe高度
 * onStart 开始打开iframe时候触发
 * onReady 完成打开iframe时候触发
 */
(function(window) {

	frameHandler = function(id, url, options) {
		var that = this;
		options = options || {};
		that.options = {
			onStart : options.onStart || function() {},
			onReady : options.onReady || function() {}
		}
		
		var onStartFun = that.options.onStart;
		var onReadyFun = that.options.onReady;
		
		var inner;
		var _iframeName = id;
		var inner = {
			_isSupport : false,
			init : function() {
				var frame = document.getElementById(id);
				frame.contentWindow.focus();
				frame.src = url;
				//    inner.check();
				//    if (inner._isSupport) {
				//        if (!frame.addEventListener) {
				//            frame.attachEvent("onload", function() {
				//                alert(44)
				//                frame.detachEvent("onload", arguments.callee);
				//                inner.adjustFrameHeight();
				//                frame.contentWindow.attachEvent("onresize",
				//                        inner.adjustFrameHeight);
				//            });
				//        } else {
				//            frame.addEventListener("load", function() {
				//                var fn = arguments.callee;
				//                setTimeout(function() {
				//                    frame.removeEventListener('load', fn, false);
				//                }, 100);
				//                inner.adjustFrameHeight();
				//                frame.contentWindow.document.documentElement
				//                        .addEventListener('DOMSubtreeModified',
				//                                inner.adjustFrameHeight, false);
				//            }, false);
				//        }
				//    } else if (frame.addEventListener) {// for FF 2, Safari 2, Opera 9.6+
				//        frame.addEventListener("load", function() {
				//            var fn = arguments.callee;
				//            setTimeout(function() {
				//                frame.removeEventListener('load', fn, false);
				//            }, 100);
				//            inner.adjustFrameHeight();
				//            frame.contentWindow.document.documentElement
				//                    .addEventListener('DOMNodeInserted',
				//                            inner.adjustFrameHeight, false);
				//            frame.contentWindow.document.documentElement
				//                    .addEventListener('DOMNodeRemoved',
				//                            inner.adjustFrameHeight, false);
				//        }, false);
				//    }
                setInterval(function(){
                    if (inner.getHeight() != frame.style.height) {
                        inner.adjustFrameHeight();
                    }
                }, 200);
                
                jQuery(frame.contentWindow.document).ready(function(){
                	onStartFun(); 
                });
				
				
				if (frame.attachEvent){ 
					frame.attachEvent("onload", onReadyFun);
				} else { 
					frame.onload = function(){ 
						onReadyFun(); 
					}
				};
				return true;
			},
			getFrame : function() {
				return document.getElementById(_iframeName).contentWindow;
			},
			adjustFrameHeight : function() {
				var elem = document.getElementById(_iframeName);
				if(elem.contentWindow.document.documentElement && elem.contentWindow.document.documentElement.scrollHeight){
					elem.style.height = elem.contentWindow.document.documentElement.scrollHeight  + 'px';
				}else if(elem.contentWindow.document.body){
					elem.style.height = elem.contentWindow.document.body.scrollHeight  + 'px';
				}
			},
			getHeight : function() {
				var elem = document.getElementById(_iframeName);
				if(elem.contentWindow.document.documentElement && elem.contentWindow.document.documentElement.scrollHeight){
					return elem.contentWindow.document.documentElement.scrollHeight  + 'px';
				}else if(elem.contentWindow.document.body){
					return elem.contentWindow.document.body.scrollHeight  + 'px';
				}
			},
			check : function() {
				var doc = document.documentElement;
				var dummy;
				if (doc.addEventListener) {
					doc.addEventListener("DOMSubtreeModified", function() {
						inner._isSupport = true;
						doc.removeEventListener("DOMSubtreeModified", arguments.callee, false);
					}, false);
				} else {
					inner._isSupport = true;
					return;
				}
				dummy = document.createElement("div");
				doc.appendChild( dummy );
				doc.removeChild( dummy );
			}
		}
		return inner.init();
	};
})(window);


//调整子窗口的iframe高度
function reinitIframe(id) {
	var iframe = document.getElementById(id);
	try {
		var bHeight = iframe.contentWindow.document.body.scrollHeight;
		var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
		var height = Math.max(bHeight, dHeight);
		iframe.height = height;
	} catch (ex) {
	}
}

// 自动调整iframe高度
function iframeAutoFit() { 
		try { 
				if(window!=parent) { 
					var a = parent.document.getElementsByTagName("IFRAME"); 
					for(var i=0; i<a.length; i++){ 
						if(a[i].contentWindow==window){ 
							var h1=0; 
							var h2=0; 
							var d=document;
							var dd=d.documentElement; 
							a[i].parentNode.style.height = a[i].offsetHeight +"px"; 
							a[i].style.height = "10px"; 
							if(dd && dd.scrollHeight){
								h1=dd.scrollHeight
							}; 
							if(d.body){
								h2=d.body.scrollHeight
							}; 
							var h=Math.max(h1, h2); 
							if(document.all){
								h += 4;
							} 
							if(window.opera){
								h += 1;
							} 
							a[i].style.height = a[i].parentNode.style.height = h +"px"; 
						} 
					} 
				} 
			} 
			catch (ex){
			} 
} 


//自动调整iframe高度和宽度
function iframeAutoFitAll() { 
			try { 
				if(window!=parent) { 
					var a = parent.document.getElementsByTagName("IFRAME"); 
					for(var i=0; i<a.length; i++){ 
						if(a[i].contentWindow==window){ 
							var h1=0; 
							var h2=0; 
							var w1=0; 
							var w2=0;
							var d=document;
							var dd=d.documentElement; 
							a[i].parentNode.style.height = a[i].offsetHeight +"px"; 
							a[i].parentNode.style.width = a[i].offsetWidth +"px"; 
							a[i].style.height = "10px"; 
							a[i].style.width = "10px"; 
							if(dd && dd.scrollHeight){
								h1=dd.scrollHeight;
							} 
							if(d.body){
								h2=d.body.scrollHeight;
							}
							if(dd && dd.scrollWidth){
								w1=dd.scrollWidth;
							} 
							if(d.body){
								w2=d.body.scrollWidth;
							} 
							var h=Math.max(h1, h2); 
							var w=Math.max(w1, w2); 
							h += 4;
							w += 4;
							a[i].style.height = a[i].parentNode.style.height = h +"px"; 
							a[i].style.width = a[i].parentNode.style.width = w +"px"; 
						} 
					} 
				} 
			} 
			catch (ex){
				
			} 
} 

