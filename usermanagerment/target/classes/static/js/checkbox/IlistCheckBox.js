function IlistCheckBox(name,type){
	this._name=name;
	this._type=type;
	this._indexs={};
	this._ids=[];
}
IlistCheckBox.prototype.insertCheckBox=function(config){
	if(this._indexs[config.id]!=null)
		return ;
	
	this._indexs[config.id]=config;
	this._ids.push(config.id);
	
	var divContentNode=document.getElementById(this._name);
	//divContentNode.id="listCheckBox";
	
	var ulNode=document.createElement("ul");
	
	var liNode=document.createElement("li");
	
	var inputNode=document.createElement("input");
	inputNode.type=this._type;
	inputNode.id=config.id;
	inputNode.value=config.value;
	inputNode.name=config.name;

	var divNode=document.createElement("div");
	
	switch(this._type){
		case 'radio':
			divNode.className="radio";
			break;
		case 'checkbox':
			divNode.className="checkbox";
			break;
	}
	
	var that=this;
	divNode.id="div_"+config.id;
	divNode.setAttribute("checkboxId",config.id);
	divNode.onclick=function(){
		var checkboxId=this.getAttribute("checkboxId");
		var checkboxNode=document.getElementById(checkboxId);
		switch(that._type){
			case 'radio':
					this.className="radioed";
					checkboxNode.checked=true;
					for(var k=0;k<that._ids.length;k++){
						if(that._ids[k]!=checkboxId){
							var otherNode=document.getElementById("div_"+that._ids[k]);
							otherNode.className="radio";
							otherNode.checked=false;
						}
					}
					if(config.onclick!=null&&config.onclick!=""){
						eval(config.onclick);
					}
				break;
			case 'checkbox':
				if(checkboxNode.checked){
					checkboxNode.checked=false;
					this.className="checkbox";
				}else{
					checkboxNode.checked=true;
					this.className="checkboxed";
				}
				break;
		}
	}
	var labelNode=document.createElement("label");
	
	var textNode=document.createTextNode(config.text);
	labelNode.appendChild(textNode);
	
	liNode.appendChild(divNode);
	liNode.appendChild(inputNode);
	liNode.appendChild(labelNode);
	
	ulNode.appendChild(liNode)
	divContentNode.appendChild(ulNode);
}
IlistCheckBox.prototype.setChecked=function (ids){
	//设置需要选中的节点的ids，ids是一个数组
	if(ids==null||ids.length<=0){
		
		return;
	}
	for(var i=0;i<ids.length;i++){
		if(ids[i]=="")
			continue;
		document.getElementById(ids[i]).checked="checked";
		var targetNode=document.getElementById("div_"+ids[i]);
		if(!targetNode)
			continue;
		switch(this._type){
			case 'radio':
				targetNode.className="radioed";
				break;
			case 'checkbox':
				targetNode.className="checkboxed";
				break;
		}
	}
	
}