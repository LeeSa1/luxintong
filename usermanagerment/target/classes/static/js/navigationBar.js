if (GetQueryString("notlian") == null) {
    document.writeln("<div id='lian'>");
    document.writeln("                                    <ul id=\'menu_list\' class=\'ulwid\'>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link1\")\' class=\'link\' id=\'link1\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 5;\'>");
    document.writeln("                                                <span class=\'lispan\'>立案环节</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link2\")\' class=\'link\' id=\'link2\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 4;\'>");
    document.writeln("                                                <span class=\'lispan\'>调查取证</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link3\")\' class=\'link\' id=\'link3\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 3;\'>");
    document.writeln("                                                <span class=\'lispan\'>核审环节</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link4\")\' class=\'link\' id=\'link4\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 2;\'>");
    document.writeln("                                                <span class=\'lispan\'>听证环节</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link5\")\' class=\'link\' id=\'link5\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 1;\'>");
    document.writeln("                                                <span class=\'lispan\'>执行环节</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                        <a href=\'javascript:void(0)\'");
    document.writeln("                                           onclick=\'setUrl(\"link7\")\' class=\'link\' id=\'link7\'>");
    document.writeln("                                            <li class=\'liwid\' style=\'z-index: 0;\'>");
    document.writeln("                                                <span class=\'lispan\'>文书归档</span>");
    document.writeln("                                            </li>");
    document.writeln("                                        </a>");
    document.writeln("                                    </ul>");
    document.writeln("                                </div>						");

    //立案环节移除简易程序的两个环节 避免jquery选择出错
    $("#notLian").remove();

} else {
    $("#notLian").show()
}


/**
 * 设置导航栏宽度样式
 */
function setMenuCss() {
    var cliwid = document.body.clientWidth;
    if (cliwid > 770) {
        if (GetQueryString("notlian") == null) {
            $(".liwid").css("width", "16.55%");
        } else {
            $(".liwid").css("width", "49.8%");
        }
    }
    else {
        $(".liwid").css("width", "90%");
    }
}

setMenuCss();

