$(function() {
	var base = "/conference";
	var locationId = GetQueryString("locationId");
	console.log(locationId);
	$.post(base+"/app/common!readLocationInfo",{
 	   "location.id":locationId
    },function(data){
    	data=JSON.parse(data);
    	console.log(data);
    	if (data.status=="success"){
    		var image=data.message.images;
    		if (image.indexOf("/upload")==0){
    			image=base+image;
    		}
    		$(".thumbnail #example2").attr("href",image);
    		$(".thumbnail img").attr("src",image);
    		$(".thumbnail h3").text(data.message.name);
    		$(".thumbnail p").eq(0).text(data.message.address);
    		
    		var contact = "";
    		var contactList = new Array();
    	    if (data.message.contactList!=null){
    	    	for (i=0;i<data.message.contactList.length;i++){
    	    		var s = "会务："+data.message.contactList[i].name+" "+data.message.contactList[i].phone;
    	    		contactList.push(s);
    	    	}
    	    	contact = contactList.join("<br>");
    	    }
    	    $(".thumbnail p").eq(1).text(contact);
    	    
    	    
    	    $(".resume p").eq(0).text(data.message.guide);
    	    $(".resume p").eq(1).text(data.message.note);
    	}
    });
});

function GetQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}