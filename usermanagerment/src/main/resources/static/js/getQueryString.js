//读取路径中的案件id及其他相关信息
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	// console.log(window.location.search);
	var r = window.location.search.substr(1).match(reg);
	// console.log(r);
	if (r != null)
		return decodeURI(r[2]);
	return null;
}

//打印案件文书编号
function getDocWord(docWord,docNumber){
    docWord = docWord.split("〔");
    $("#docWord").text(docWord[0]);
    docWord = docWord[1].split("〕");
    $("#year").text(docWord[0]);
    $("#docNumber").text(docNumber);
}

function setApproveAutoAndTime(){
    //得到领导审批签名和时间
    $.ajax({
        url: "/luzheng/app/case!getApproveByCaseDocIdAndCaseId",
        data: {
            "caseId": caseId,
            "caseDocId": caseDocumentId
        },
        success: function (data) {
            data = JSON.parse(data);
            for (var i = 0; i < data.message.length; i++) {
                if (data.message[i].witchone == 1) {
                    $("#autograph1").attr("src", data.message[i].autograph);
                    $("#autograph1Time").text(DateFormatStr(data.message[i].date, "yyyy 年 MM 月 dd 日"));
                } else {
                    $("#autograph2").attr("src", data.message[i].autograph);
                    $("#autograph2Time").text(DateFormatStr(data.message[i].date, "yyyy 年 MM 月 dd 日"));
                }
            }
        }
    });
}

