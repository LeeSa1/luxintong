



function addListener(element, type, callback) {
		    if (element.addEventListener) {
		        element.addEventListener(type, callback);
		    } else if (element.attachEvent) {
		        element.attachEvent('on' + type, callback);
		    }
		}
		addListener(document, 'DOMContentLoaded', function () {
		    
		    var mq = window.matchMedia("(max-width: 760px)");
		    if (mq.matches) {
		    	$("#menu_list").hide();
		    	$("#menu_button_wrapper").show();
		    }

		    addListener(document.getElementById("menu_button"), 'click', function () {
		    	$("#menu_list").toggle();
		    });
		    
		    addListener(window, 'resize', function () {
		        var width = window.innerWidth ||
		                    document.documentElement.clientWidth ||
		                    document.body.clientWidth;
		        
		        if (width > 760) {
		        	$("#menu_list").show();
		        	$("#menu_button_wrapper").hide();
		        } else {
		        	$("#menu_list").hide();
		        	$("#menu_button_wrapper").show();
		        	
		        }
		    });
		    
		});
		
