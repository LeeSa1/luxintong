﻿

$(".content .remore").each(function() {
	if($(window).width() > 1080) {
		var maxwidth = 60;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	} else {
		var maxwidth = 50;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	}
});

$('.img-box').find('img').each(function() {
	if($(this).attr('src') == "") {
		$(this).parents('.img-box').css('width', '0');
		$(this).parents('.img-box').next().css({
			'width': '100%',
			'padding-left': '0'
		});
	}
});

function smart_img($imgdom) {
	var w = $imgdom.width();
	var h = $imgdom.height();
	if(w > h) {
		$imgdom.css({
			'height': '100%'
		});
		var left = ($imgdom.width() - $imgdom.parent().width()) / 2 * (-1);
		$imgdom.css({
			'margin-left': left + 'px'
		});
		if($imgdom.width() < $imgdom.parent().width()) {
			$imgdom.css({
				'width': '100%',
				'height': 'auto',
				'margin-left': '0'
			});
		}
	} else {
		$imgdom.css({
			'width': '100%'
		});
	}
}

$(function() {
	$img = $('.img-box').find('img');
	smart_img($img);

	$('#wrapper .right2 .head>i').on('click', function() {
		$('#wrapper .right2 .content').slideToggle();
		$('#wrapper .right2>ul').fadeToggle();
	})

	$('#wrapper .right2 .head>i').toggle(
		function() {
			$(this).attr('class', 'glyphicon glyphicon-chevron-up');
		},
		function() {
			$(this).attr('class', 'glyphicon glyphicon-chevron-down')
		}
	)
})

$(function() {
    $('.right2 ul').eq(0).find('li').eq(0).find('a').attr('href','document.html?subject=内科');
	$('.right2 ul').eq(0).find('li').eq(1).find('a').attr('href','document.html?subject=外科');
	$('.right2 ul').eq(0).find('li').eq(2).find('a').attr('href','document.html?subject=妇产科');
	$('.right2 ul').eq(0).find('li').eq(3).find('a').attr('href','document.html?subject=儿科');
	$('.right2 ul').eq(0).find('li').eq(4).find('a').attr('href','document.html?subject=皮肤性病科');
	$('.right2 ul').eq(0).find('li').eq(5).find('a').attr('href','document.html?subject=临床其他');
	$table = $('.left .content>ul');
	$pageli = $('.left .content .page');
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var subject = getQueryString('subject');
	if (!subject) subject = '';
	$('.top li a').text('');
	$('.top li a').eq(0).text('文献');
	$('.top li a').eq(0).attr('href','document.html');
	if (subject != ''){
			$('.top li i').eq(0).show();
			$('.top li a').eq(1).text(decodeURI(subject));
			$('.top li a').eq(1).attr('href','document.html?subject='+subject);
		}else{
			$('.top li i').eq(0).hide();
		}
	$('.left .head span').text(decodeURI(subject)+'文献');
	$('.right .content ul').html('');
	$('.right2 .content ul').html('');
	nextSubject(0);
	
	$.post(base+"/app/common!readSubResList",{
		       "user.token":token,
			   "user.id":id,
			   channel:"2",
			   subject:subject
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   //$('.top li a').eq(3).text('共'+data.message.total+'条记录');
			   $('.top li a').eq(3).text('');
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=subresource&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
	$('.left .content>ul').on('click','li .info div:nth-of-type(2)',function(){
        $('#discuss').attr('data-id',$(this).attr('data-id'));
		toDiscuss();
	});
	$('#discuss .btn-danger').on('click',function(){
				$('#discuss').slideUp(300);
			})
			
			$('#discuss .btn-info').on('click', function(){
				var content = $('#discuss .user-box textarea').val();
				if (content == ''){
					alert('请输入心得！');
					return;
				}else{
					$('#discuss .user-box textarea').val('');
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "subresource";
	            var resId = $('#discuss').attr('data-id');
				$('.comment>ul').html('');
				$.post(base+'/app/common!createComment',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "comment.content":content
					   },function(data){
						   console.log(data);
						   listDiscuss(1);
					   });
			})
})

function doSearch(){
	$table = $('.left .content>ul');
	$pageli = $('.left .content .page');
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var pageindex = $('#pageindex').val();
	console.log(pageindex);
	var s = $('.search #search-input').val();
	var subject = getQueryString('subject');
	if (!subject) subject = '';
	$('.page .title').remove();
	$.post(base+"/app/common!readSubResList",{
		       "user.token":token,
			   "user.id":id,
			    channel:"2",
				name:s,
				subject:subject,
			   "page.index":pageindex
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
			   if (s != ''){
				   $('.top li a').eq(3).text('搜索关键词：'+s+'，共'+data.message.total+'条搜索结果');
			   }else{
				   //$('.top li a').eq(3).text('共'+data.message.total+'条记录');
				   $('.top li a').eq(3).text('');
			   }
			   $table.html('');
			   for (i=0;i<data.message.resultList.length;i++){
				   var obj = data.message.resultList[i];
				   var time = new Date(obj.createdAt);
				   var imgStyle = '';
				   var divStyle = '';
				   if (obj.pic == ''){
					   imgStyle = 'style="width:0"';
					   divStyle = 'style="width: 100%; padding-left: 0;"';
				   }
				   var link = 'detail.html?type=subresource&id='+obj.id;
				   var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
				   var html = '<li><div class="img-box" '+imgStyle+'><a href="'+link+'" target="_blank"><img src="'+pic+'"></a></div>'
							+ '<div '+divStyle+'><ul><li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>'
							+ '<li class="remore">'+obj.desc+'</li>'
							+ '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>'+time.getFullYear()+'-'+(time.getMonth()+1)+'-'+time.getDate()+'</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
				   $table.append(html);
				   $myson = $('.left .content>ul>li');
			   }
			   mypage($table, 10, data.message.index-1,data.message.total,$pageli, $myson);
			   }else if (data.status == 'token'){
				   window.location.href = 'login.html';
			   }
		   });
}

function toDiscuss(){
	            listDiscuss(1);
				$('#discuss').fadeIn(300);
}

function listDiscuss(pageindex){
	            var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "subresource";
	            var resId = $('#discuss').attr('data-id');

				$('.comment>ul').html('');
				$('#discuss .page .title').remove();
				console.log(type+','+resId);
				$.post(base+'/app/common!readCommentList',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "page.index":pageindex
					   },function(data){
						   console.log(data);
						   data = JSON.parse(data);
			               if (data.status == 'success'){
							   for (i=0;i<data.message.resultList.length;i++){
								   var obj = data.message.resultList[i];
								   var time = new Date(obj.createdAt);
								   var photo = obj.user.photo;
								   if (photo.indexOf('/upload')==0){
									   photo = base+photo;
								   }
								   var html = '<li><div><img src="'+photo+'" /></div>'
								            + '<div><p><b>'+obj.user.name+': </b><span>'+obj.content+'</span></p>'
								            + '<p>'+time.getFullYear()+'-'+fillzero((time.getMonth()+1))+'-'+fillzero(time.getDate())+' '+fillzero(time.getHours())+':'+fillzero(time.getMinutes())+':'+fillzero(time.getSeconds())+'</p></div></li>';
								   $('.comment>ul').append(html);
							   }
							   $table = $('#discuss .comment>ul');
				               $pageli = $('#discuss .page');
				               $myson = $('#discuss .comment>ul>li')
				               mypage($table, 10,data.message.index-1,data.message.resultList.length, $pageli, $myson);
						   }
					   });

}

function fillzero(s){
			if (parseInt(s)<10){
				return '0'+s;
			}else{
				return s;
			}
		}

function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  r[2]; return null;
}

function nextSubject(index){
	var base = "/hoslibrary";
	$.post(base+"/app/common!readSubjectList",{
		       pid:index+1
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   if (data.status == 'success'){
				   for (i=0;i<data.message.length;i++){
					   var obj = data.message[i];
					   var html = '<li><a href="document.html?subject='+obj.name+'">'+obj.name+'</a></li>';
					   $('.right .content ul').eq(index).append(html);
					   $('.right2 .content ul').eq(index).append(html);
				   }
				   if (index < 5){
					   nextSubject(index+1);
				   }
			   }
		   });
}

function searchText(){
	//if ($('.search #search-input').val()!=''){
		$('#pageindex').val(1);
		$('.page .title').remove();
		doSearch();
	//}
}