$(function() {
	var base = "/conference";
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readLocationInfoByName",
    	data:{"location.name":"会场位置平面示意图"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message!=null){
    				var image=data.message.images;
    	    		if (image.indexOf("/upload")==0){
    	    			image=base+image;
    	    		}
    				$(".thumbnail img").eq(0).attr("src",image);
    				$("#example1").attr("href",image);
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readLocationInfoByName",
    	data:{"location.name":"会场3楼场地分布图"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message!=null){
    				var image=data.message.images;
    	    		if (image.indexOf("/upload")==0){
    	    			image=base+image;
    	    		}
    				$(".thumbnail img").eq(1).attr("src",image);
    				$("#example2").attr("href",image);
    			}
    		}
    	}
	});
	$.ajax({
    	type:"post",
    	url:base+"/app/common!readLocationInfoByName",
    	data:{"location.name":"会场5楼场地分布图"},
    	async:false,
    	success:function(data){
    		data=JSON.parse(data);
    		console.log(data);
    		if (data.status=="success"){
    			if (data.message!=null){
    				var image=data.message.images;
    	    		if (image.indexOf("/upload")==0){
    	    			image=base+image;
    	    		}
    				$(".thumbnail img").eq(2).attr("src",image);
    				$("#example3").attr("href",image);
    			}
    		}
    	}
	});
});