String.prototype.replaceAll = function(s1,s2) { 
    return this.replace(new RegExp(s1,"gm"),s2); 
}

/**
 * 页面初始化消息提示
 */
$(document).ready(function() {

	var exception = $.cookie('exception');
	if (exception && exception != null) {
		$.message("errors", exception);
		$.removeCookie('exception');
	}else{
		var contentWindow = $.cookie('contentWindow');
		var contentWindowStatus = $.cookie('contentWindowStatus');
		
		var messageWindow = $.cookie('messageWindow');
		var messageWindowStatus = $.cookie('messageWindowStatus');
		
		var tipWindow = $.cookie('tipWindow');
		var tipWindowStatus = $.cookie('tipWindowStatus');
		
		var topMessage = $.cookie('topMessage');
		var topMessageStatus = $.cookie('topMessageStatus');
		
		var onloadMessage = $.cookie('onloadMessage');
		
		var tokenMessage = $.cookie('tokenMessage');
		
		if (contentWindow && contentWindow != null) {
			$.window(contentWindowStatus, contentWindow);
		}
		if (messageWindow && messageWindow != null) {
			$.message(messageWindowStatus, messageWindow);
		}
		if (tipWindow && tipWindow != null) {
			$.tip(tipWindowStatus, tipWindow);
		}
		if (topMessage && topMessage != null) {
			$.top(topMessageStatus, topMessage);
		}
		if (onloadMessage && onloadMessage != null) {
			eval(onloadMessage)
		}
		if (tokenMessage && tokenMessage != null) {
			$.token(tokenMessage);
		}
	}
	$.removeCookie('contentWindow');
	$.removeCookie('contentWindowStatus');
	$.removeCookie('messageWindow');
	$.removeCookie('messageWindowStatus');
	$.removeCookie('tipWindow');
	$.removeCookie('tipWindowStatus');
	$.removeCookie('topMessage');
	$.removeCookie('topMessageStatus');
	$.removeCookie('onloadMessage');
	$.removeCookie('tokenMessage');
});

// 内容窗口
$.window = function(){
    var $contentWindow = $("#contentWindow");
	$contentWindow.jqm({
		overlay: 60,
		closeClass: "windowClose",
		modal: false,
		trigger: false,
		onHide: function(object) {
			object.o.remove();
			object.w.fadeOut();
		}
	}).jqDrag(".windowTop");
    var $windowTitle = $("#contentWindow .windowTitle");
    var $windowContent = $("#contentWindow .windowContent");
    var windowTitle;
    var windowContent;
    if (arguments.length == 1) {
        windowTitle = "";
        windowContent = arguments[0];
    }
    else {
        windowTitle = arguments[0];
        windowContent = arguments[1];
    }
	if(windowTitle == "success"){
		windowTitle = "操作成功";
	}else if(windowTitle == "warn"){
		windowTitle = "警告信息";
	}else if(windowTitle == "errors"){
		windowTitle = "错误信息";
	}
	windowContent = windowContent.replaceAll('&lt;','<');
	windowContent = windowContent.replaceAll('&gt;','>');
    $windowTitle.html(windowTitle);
    $windowContent.html(windowContent);
    $contentWindow.jqmShow();
	var h = $("body").height();
	if(h<500){
		$("body").height(500);
	}
};

// 警告信息
$.message = function(){
    var $messageWindow = $("#messageWindow");
	$messageWindow.jqm({
		overlay: 60,
		closeClass: "messageClose",
		modal: true,
		trigger: false,
		onHide: function(object) {
			object.o.remove();
			object.w.fadeOut();
		}
	}).jqDrag(".windowTop");
    var $icon = $("#messageWindow .icon");
    var $messageText = $("#messageWindow .messageText");
    var $messageButton = $("#messageWindow .messageButton");
    var messageType;
    var messageText;
    if (arguments.length == 1) {
        messageType = "warn";
        messageText = arguments[0];
    }
    else {
        messageType = arguments[0];
        messageText = arguments[1];
    }
    if (messageType == "success") {
        $icon.removeClass("warn").removeClass("errors").addClass("success");
    }
    else if (messageType == "errors") {
        $icon.removeClass("warn").removeClass("success").addClass("errors");
    }
    else {
        $icon.removeClass("success").removeClass("errors").addClass("warn");
    }
	messageText = messageText.replaceAll('&lt;','<');
	messageText = messageText.replaceAll('&gt;','>');
    $messageText.html(messageText);
    $messageWindow.jqmShow();
	$messageButton.unbind('click').click(function(){$messageWindow.jqmHide()});
    $messageButton.focus();
	var h = $("body").height();
	if(h<500){
		$("body").height(500);
	}
};

// 确认信息
$.confirm = function(messageText,callback){
    var $confirmWindow = $("#confirmWindow");
	$confirmWindow.jqm({
		overlay: 60,
		closeClass: "messageClose",
		modal: true,
		trigger: false,
		onHide: function(object) {
			object.o.remove();
			object.w.fadeOut();
		}
	}).jqDrag(".windowTop");
    var $icon = $("#confirmWindow .icon");
    var $messageText = $("#confirmWindow .messageText");
    var $confirmButton = $("#confirmWindow .messageButton.confirm");
	var $cancelButton = $("#confirmWindow .messageButton.cancel");
	messageText = messageText.replaceAll('&lt;','<');
	messageText = messageText.replaceAll('&gt;','>');
    $messageText.html(messageText);
    $confirmWindow.jqmShow();
    $confirmButton.unbind('click').click(function(){
        if ($.isFunction(callback)) {
            callback.apply();
        }
        $confirmWindow.jqmHide();
    });
	$cancelButton.unbind('click').click(function(){
        $confirmWindow.jqmHide();
    });
    $confirmButton.focus();
	var h = $("body").height();
	if(h<500){
		$("body").height(500);
	}
};


//滑动提示框
$.tip = function () {
	var $tipWindow = $("#tipWindow",window.top.document);
	if($tipWindow.length == 0){
		$tipWindow = $("#tipWindow",window.parent.document);
	}
	if($tipWindow.length == 0){
		$tipWindow = $("#tipWindow");
	}
	var $icon = $tipWindow.find(".icon");
	var $messageText = $tipWindow.find(".messageText");
	var messageType;
	var messageText;
	if (arguments.length == 1) {
		messageType = "warn";
		messageText = arguments[0];
	} else {
		messageType = arguments[0];
		messageText = arguments[1];
	}
	if (messageType == "success") {
		$icon.removeClass("warn").removeClass("errors").addClass("success");
	} else if (messageType == "errors") {
		$icon.removeClass("warn").removeClass("success").addClass("errors");
	} else {
		$icon.removeClass("success").removeClass("errors").addClass("warn");
	}
	messageText = messageText.replaceAll('&lt;','<');
	messageText = messageText.replaceAll('&gt;','>');
	$messageText.html(messageText);
	$tipWindow.css({"margin-left": "-" + parseInt($tipWindow.width() / 2) + "px", "left": "50%"});
	setTimeout(function() {
		$tipWindow.animate({left: 0, opacity: "hide"}, "slow");
	}, 3000);
	$tipWindow.show();
};

//顶部提示框
$.top = function () {
	var $topMessage = $("#topMessage");
	var $icon = $("#topMessage .icon");
	var $cancel= $("#topMessage .cancel");
	var $messageText = $("#topMessage .messageText");
	var messageType;
	var messageText;
	if (arguments.length == 1) {
		messageType = "warn";
		messageText = arguments[0];
	} else {
		messageType = arguments[0];
		messageText = arguments[1];
	}
	if (messageType == "success") {
		$topMessage.removeClass("topMessageWarn").removeClass("topMessageErrors").addClass("topMessageSuccess");
		$icon.removeClass("warn").removeClass("errors").addClass("success");
	} else if (messageType == "errors") {
		$topMessage.removeClass("topMessageWarn").removeClass("topMessageSuccess").addClass("topMessageErrors");
		$icon.removeClass("warn").removeClass("success").addClass("errors");
	} else {
		$topMessage.removeClass("topMessageSuccess").removeClass("topMessageErrors").addClass("topMessageWarn");
		$icon.removeClass("success").removeClass("errors").addClass("warn");
	}
	$cancel.click(function(){$topMessage.hide()});
	messageText = messageText.replaceAll('&lt;','<');
	messageText = messageText.replaceAll('&gt;','>');
	$messageText.html(messageText);
	$topMessage.show();
};

//用户令牌
$.token = function(tokenMessage){
	var length = $("form").length;
	if(tokenMessage && length>0){
		$("form").each(function(){
			var $form = $(this);
			var input = "<input type='hidden' name='tokenMessage' value='"+ tokenMessage +"' />";
			$form.prepend(input);
		});
	}
};