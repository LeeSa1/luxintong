$('.learn-head').children('ul').on('click', 'li', function() {
	var m = $(this).index();
	$('.learn-head li').removeClass('active').eq(m).addClass('active');
	$('.learn-head').next('div').children('ul').fadeOut(200).delay(220).eq(m).fadeIn(200);

})

$(function() {
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	nextSubject(0);
	
})

$(function() {
	$('.head>button').toggle(
		function() {
			$(this).attr('class', 'sbtn btn-success');
			$(this).children('i').attr('class', 'glyphicon glyphicon-ok');
			$(this).children('span').text('已学习');
			$(this).parents('.head').next().children('.wait-study').slideToggle().siblings('.ok-study').slideToggle();
		},
		function() {
			$(this).attr('class', 'sbtn btn-warning');
			$(this).children('i').attr('class', 'glyphicon glyphicon-pencil');
			$(this).children('span').text('待学习');
			$(this).parents('.head').next().children('.ok-study').slideToggle().siblings('.wait-study').slideToggle();
		}
	)
})

$(function() {
	if($(window).width() < 991) {
		$('.head>span').on('click', function(e) {
			$(this).next('ul').slideDown();
			e.stopPropagation();
		})

		$(document).on('click', function() {
			$('.head>ul').slideUp();
		})
	}
})

function nextSubject(index){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	var sObj = $('.style'+(index+1)+' .wait-study');
	$('.style'+(index+1)+' .head a').attr('href','policylist.html?channel='+index);
	var s = $('.search #search-input').val();
	$.post(base+"/app/common!readPolicyList",{
		       "user.token":token,
			   "user.id":id,
		       channel:index,
			   name:s
		   },function(data){
			   console.log(data);
			   data = JSON.parse(data);
			   console.log($(sObj));
			   if (data.status == 'success'){
				   $('.style'+(index+1)+' ul').html('');
				   for (i=0;i<data.message.resultList.length;i++){
					   var obj = data.message.resultList[i];
					   console.log(obj);
					   var link = 'detail.html?type=policy&id='+obj.id;
					   var html = '<li><a href="'+link+'" target="_blank">'+obj.name+'</a></li>';
					   console.log($(sObj));
					   $('.style'+(index+1)+' .wait-study').append(html);
					   $('.style'+(index+1)+' .ok-study').append(html);
				   }
				   if (index < 6){
					   nextSubject(index+1);
				   }
			   }
		   });
}

function searchText(){
	//if ($('.search #search-input').val()!=''){
		doSearch();
	//}
}

function doSearch(){
	nextSubject(0);
}