
//锁定表头和列     

(function(window){
	
	 fixTable = function (TableID, FixColumnNumber,options) {
		
		//TableID            要锁定的Table的ID     
		//FixColumnNumber    要锁定列的个数     
		//stepWidth          非冻结列的步长，默认55px    
		//width              显示的宽度 ，默认整个网页的宽度
		//height             显示的高度 ，默认整个table的高度
		
		options = options || {};
		
		var that = this;
		that.options = {
			bodyWidth: options.bodyWidth || ($('body').width()-20),
			stepWidth: options.stepWidth || 55,
			width: options.width || options.bodyWidth || ($('body').width()-20),
			height: options.height || $("#" + TableID).height() + 5,
            frozen: options.frozen || false
		}
		
		bodyWidth = that.options.bodyWidth;
		stepWidth = that.options.stepWidth;
		width = that.options.width;
		height = that.options.height;
		frozen = that.options.frozen;
		
		var fn = {
			link : window.location.href,
			reflash : function(type) {
			   var tWidth = $("#" + TableID).attr('width');
			   if(type == 'show'){
			   	  tWidth = parseInt(tWidth) + parseInt(stepWidth);
			   }else{
			   	  tWidth = parseInt(tWidth) - parseInt(stepWidth);
			   }
			   
			   $("#" + TableID + '_tableFixClone').attr('width',tWidth);
			   $("#" + TableID + '_tableHeadClone').attr('width',tWidth);
			   $("#" + TableID + '_tableColumnClone').attr('width',tWidth);
			   $("#" + TableID).attr('width',tWidth);
			   var ColumnsWidth = 0;
			   var ColumnsNumber = 0;
			   $("#" + TableID + "_tableColumn tr:last td:lt(" + FixColumnNumber + "):visible")
				.each(function() {
					ColumnsWidth += $(this).outerWidth(true);
					ColumnsNumber++;
				});
				ColumnsWidth += 2;
				if ($.browser.msie) {
				switch ($.browser.version) {
					case "7.0":
						if (ColumnsNumber >= 3)
							ColumnsWidth--;
						break;
					case "8.0":
						if (ColumnsNumber >= 2)
							ColumnsWidth--;
						break;
					}
				}
				$("#" + TableID + "_tableColumn").css("width", ColumnsWidth);
				$("#" + TableID + "_tableFix").css("width", ColumnsWidth);
			},
			putSelect : function(key) {
                $.cookie('-' + fn.link + '-' + key, '1' ,{ expires: 365 });
			},
			removeSelect : function(key) {
			    $.removeCookie('-' + fn.link + '-' + key);
			},
			getSelect : function(key) {
			    return $.cookie('-' + fn.link + '-' + key);
			}
		}
		
		var $outerWidth = 0;
		var $columnNumber = FixColumnNumber - 1;
		$("#" + TableID + " tr:first th:gt(" + $columnNumber + "):visible").each(function() {
			$outerWidth = $outerWidth + stepWidth;
		});
        $("#" + TableID).attr('width', bodyWidth + $outerWidth); //根据列数动态调整
        
		
        if (frozen && frozen == true) {
			
			//隐藏明细项
			var list = new Array();
			$("#" + TableID + " tr:first th.frozen ").each(function() {
				var $this = $(this);
				$this.addClass('frozen_'+$this.index());
	            list.push({
	                index: $this.index(),
	                key: 'frozen_' + $this.index(),
	                value: $this.text()
	            });
			});
			
	        //将需要隐藏的列加上class  ：'frozen_' + $this.index() ，选择的时候根据class进行整列显示、隐藏
	        if (list.length > 0) {
	        
	            $("#" + TableID).before('<div><table id="SelectTable" width="100%" class="SelectTable">'+
										'<tr><th>已选</th><td id="frozen_select"></td></tr><tr><th>请选择</th><td id="frozen_element"></td></tr>'+
										'</table></div><div class="blank"></div>');
	            
	            for (var i = 0; i < list.length; ++i) {
	                $("#" + TableID + " tr td ").each(function(){
	                    var $this = $(this);
	                    if ($this.index() == list[i].index) {
	                        $this.addClass('frozen');
	                        $this.addClass(list[i].key);
	                    }
	                });
	                $("#frozen_element").append('<div class="select_box_s1" id="' + list[i].key + '" >' + list[i].value + '</div>');
	            }
				
				$(".select_box_s1").click(function(){
	                    var $this = $(this);
						var $text = $this.text();
						var $id = $this.attr('id');
						var $frozenSelect = $("#frozen_select");
						if($this.attr('class') == 'select_box_s1'){ //选中
						   $this.attr('class','');
						   $this.addClass('select_box_s2');
						   $frozenSelect.append( "<div class='select_box_s5' id='frozen_select_"+ $id +"'>"
						        + "<div class='select_box_s3'>显示:</div>"
								+ "<div class='select_box_s4'>" + $text + "</div>"
								+ "<div class='select_box_s6'>X</div>"
								+ "</div>");
						   
						   $('.'+ $id).show();
						   fn.reflash('show');
						   fn.putSelect($id);
						   
						   //为已经选中的box绑定事件
	                       $("#frozen_select_"+ $id +".select_box_s5").click(function(){
						   	    var $selectBoxId = $(this).attr('id').replace('frozen_select_','');
								$("#"+$selectBoxId).attr('class','');
								$("#"+$selectBoxId).addClass('select_box_s1');
								$('.'+ $selectBoxId).hide();
								fn.reflash('hide');
								fn.removeSelect($selectBoxId);
								$(this).remove();
	                       });
						}else{ //取消
						   $this.attr('class','');
						   $this.addClass('select_box_s1');
						   $("#frozen_select_"+$id).remove();
						   $('.'+ $id).hide();
						   fn.reflash('hide');
						   fn.removeSelect($id);
						}
	            });
				
				//初始化的时候为已经选过的项做默认
                $(".select_box_s1").each(function(){
                    var $this = $(this);
                    var $id = $this.attr('id');
					var flag = fn.getSelect($id);
                    if (flag && flag != null && flag == '1') {
                        $this.click();
                    }
                });
				
	            
	        }
        
        }
		
						
	    
		
		if ($("#" + TableID + "_tableLayout").length != 0) {
			$("#" + TableID + "_tableLayout").before($("#" + TableID));
			$("#" + TableID + "_tableLayout").empty();
		} else {
			$("#" + TableID).after(
					"<div id='" + TableID
							+ "_tableLayout' style='overflow:hidden;height:"
							+ height + "px; width:" + width + "px;'></div>");
		}
		$(
				'<div id="' + TableID + '_tableFix"></div>' + '<div id="'
						+ TableID + '_tableHead"></div>' + '<div id="'
						+ TableID + '_tableColumn"></div>' + '<div id="'
						+ TableID + '_tableData"></div>').appendTo(
				"#" + TableID + "_tableLayout");
		var oldtable = $("#" + TableID);
		var tableFixClone = oldtable.clone(true);
		tableFixClone.attr("id", TableID + "_tableFixClone");
		$("#" + TableID + "_tableFix").append(tableFixClone);
		var tableHeadClone = oldtable.clone(true);
		tableHeadClone.attr("id", TableID + "_tableHeadClone");
		$("#" + TableID + "_tableHead").append(tableHeadClone);
		var tableColumnClone = oldtable.clone(true);
		tableColumnClone.attr("id", TableID + "_tableColumnClone");
		$("#" + TableID + "_tableColumn").append(tableColumnClone);
		$("#" + TableID + "_tableData").append(oldtable);
		$("#" + TableID + "_tableLayout table").each(function() {
			$(this).css("margin", "0");
		});
		var HeadHeight = $("#" + TableID + "_tableHead thead").height();
		HeadHeight += 2;
		$("#" + TableID + "_tableHead").css("height", HeadHeight);
		$("#" + TableID + "_tableFix").css("height", HeadHeight);
		var ColumnsWidth = 0;
		var ColumnsNumber = 0;
		$("#" + TableID + "_tableColumn tr:last td:lt(" + FixColumnNumber + ")")
				.each(function() {
					ColumnsWidth += $(this).outerWidth(true);
					ColumnsNumber++;
				});
		ColumnsWidth += 2;
		if ($.browser.msie) {
			switch ($.browser.version) {
			case "7.0":
				if (ColumnsNumber >= 3)
					ColumnsWidth--;
				break;
			case "8.0":
				if (ColumnsNumber >= 2)
					ColumnsWidth--;
				break;
			}
		}
		$("#" + TableID + "_tableColumn").css("width", ColumnsWidth);
		$("#" + TableID + "_tableFix").css("width", ColumnsWidth);
		$("#" + TableID + "_tableData").scroll(
				function() {
					$("#" + TableID + "_tableHead").scrollLeft(
							$("#" + TableID + "_tableData").scrollLeft());
					$("#" + TableID + "_tableColumn").scrollTop(
							$("#" + TableID + "_tableData").scrollTop());
				});
		$("#" + TableID + "_tableFix").css( {
			"overflow" : "hidden",
			"position" : "relative",
			"z-index" : "50",
			"background-color" : "White"
		});
		$("#" + TableID + "_tableHead").css( {
			"overflow" : "hidden",
			"width" : width - 0,
			"position" : "relative",
			"z-index" : "45",
			"background-color" : "White"
		});
		$("#" + TableID + "_tableColumn").css( {
			"overflow" : "hidden",
			"height" : height - 17,
			"position" : "relative",
			"z-index" : "40",
			"background-color" : "White"
		});
		$("#" + TableID + "_tableData").css( {
			"overflow" : "scroll",
			"overflow-y": "hidden",
			"width" : width,
			"height" : height,
			"position" : "relative",
			"z-index" : "35"
		});
		if ($("#" + TableID + "_tableHead").width() > $(
				"#" + TableID + "_tableFix table").width()) {
			$("#" + TableID + "_tableHead").css("width",
					$("#" + TableID + "_tableFix table").width());
			$("#" + TableID + "_tableData").css("width",
					$("#" + TableID + "_tableFix table").width() + 0);
		}
		if ($("#" + TableID + "_tableColumn").height() > $(
				"#" + TableID + "_tableColumn table").height()) {
			$("#" + TableID + "_tableColumn").css("height",
					$("#" + TableID + "_tableColumn table").height());
			$("#" + TableID + "_tableData").css("height",
					$("#" + TableID + "_tableColumn table").height() + 17);
		}
		$("#" + TableID + "_tableFix").offset(
				$("#" + TableID + "_tableLayout").offset());
		$("#" + TableID + "_tableHead").offset(
				$("#" + TableID + "_tableLayout").offset());
		$("#" + TableID + "_tableColumn").offset(
				$("#" + TableID + "_tableLayout").offset());
		$("#" + TableID + "_tableData").offset(
				$("#" + TableID + "_tableLayout").offset());
	    $("#" + TableID + "_tableLayout").height(
				$("#" + TableID + "_tableData").height());
	}
	
})(window);