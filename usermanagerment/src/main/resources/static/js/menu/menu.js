$(document).ready(function(){
    /**
     * the menu
     */
    var $divsub = $('div.divsub');
	$divsub.each(function(index){
		 var $this = $(this);
		 var ulCount = $this.children('ul').length;
		 var width = ulCount*112 ;
		 $this.css('width', width);
		 if(index >= 5){
		 	$this.removeClass("divsub01").addClass("divsub02");
		 }
	});
    
});

//获取到左边的距离
function getAbsLeft(obj){
    var l = obj.offsetLeft;
    while (obj.offsetParent != null) {
        obj = obj.offsetParent;
        l += obj.offsetLeft;
    }
    return l;
}

/*******机构选择开始******/
var timeout = 500;
var closetimer = 0;
var ddmenuitem = 0;

// open hidden layer
function mopen(id){
    // cancel close timer
    mcancelclosetime();
    // close old layer
    if (ddmenuitem) {
        ddmenuitem.style.visibility = 'hidden';
    }
    // get new layer and show it
    ddmenuitem = document.getElementById(id);
    ddmenuitem.style.visibility = 'visible';
    
}

// close showed layer
function mclose(){
    if (ddmenuitem) {
        ddmenuitem.style.visibility = 'hidden';
    }
}

// go close timer
function mclosetime(){
    closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime(){
    if (closetimer) {
        window.clearTimeout(closetimer);
        closetimer = null;
    }
}

/*******机构选择结束******/

function openLink(url){
	$("body").prepend('<form id="linkForm" style="display:none;" method="post" target="_blank"></form>');
	$("#linkForm").attr('action',url);
	$("#linkForm").submit();
}

function openSelfLink(url){
	$("body").prepend('<form id="linkForm" style="display:none;" method="post" target="_self"></form>');
	$("#linkForm").attr('action',url);
	$("#linkForm").submit();
}

/**
 * 获取上下文路径
 */
function getBase(){
    var contextPath = document.location.pathname;
    var index = contextPath.substr(1).indexOf("/");
    contextPath = contextPath.substr(0, index + 1);
    return contextPath;
}

function openMenu(url){
	if(typeof url != "undefined"){
		if(url.indexOf("http:") > -1){
			openLink(url)
		}else{
			url = getBase() + "/" + url;
			$("body").showLoading();
			openSelfLink(url);
		}
	}
}

function logout(){
	var _base = getBase();
	var url =  _base + "/" + "sysmgr/login!logout.action";
	openSelfLink(url);
}

function setActiveOrg(orgId){
    var _base = getBase();
    var url = _base + "/" + "sysmgr/login!setActiveOrg.action";
    var params = "orgId=" + orgId;
    jQuery.ajax({
        url: url,
        data: params,
        async: false,
        type: "GET",
        success: function(){
            //window.location.reload();
            window.location.href = window.location;
        }
    });
}
