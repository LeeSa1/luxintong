//输入和编辑页面当事人和文书编号加载
$(function() {
	var userId = $.cookie('id');
	var caseId = GetQueryString("caseId");
	// 当事人信息
	$.ajax({
		url : "/luzheng/app/case!readCaseInfo",
		data : {
			"id" : caseId
		},
		success : function(data) {
			data = JSON.parse(data);
			$("#litigantName1").val(data.message.litigantName);
			$("#litigantName").val(data.message.litigantName);
			$("#litigantCard").val(data.message.litigantCard);
			$("#litigantAge").val(data.message.litigantAge);
			$("#litigantPhone").val(data.message.litigantPhone);
			$("#litigantNameAddressPostcode").val(data.message.litigantNameAddressPostcode);
			$("#litigantWorkUnit").val(data.message.litigantWorkUnit);
			$("#litigantOccupation").val(data.message.litigantOccupation);
			$("#litigantAddress").val(data.message.litigantAddress);
			$("#litigantRepresentative").val(data.message.litigantRepresentative);//法人代表 法定代表人
			$("#litigantRepresentativePhone").val(data.message.litigantRepresentativePhone);
			//案件名称 执法人员信息
			$("#caseName").val(data.message.caseName);
			$("#officer1Id").val(data.message.officer1Id);
			$("#officer1IdCard").val(data.message.officer1IdCard);
			$("#officer2Id").val(data.message.officer2Id);
			$("#officer2IdCard").val(data.message.officer2IdCard);
			//代理人
			$("#agentName").val(data.message.agentName);
			$("#agentSex").val(data.message.agentSex);
			$("#agentAge").val(data.message.agentAge);
			$("#agentWorkUnit").val(data.message.agentWorkUnit);
			$("#agentOccupation").val(data.message.agentOccupation);
			$("#orgName").val(data.message.orgName);
		}
	});
	// 加载文书编号
	$.ajax({
		url : "/luzheng/app/common!findAllDocumentNo",
		data : {
			"user.id" : userId
		},
		async : false,
		success : function(data) {
			// console.log(data);
			data = JSON.parse(data);
			$("#docWord option").remove();
			$("#docWord").append('<option value="">请选择案件文书编号</option>');
			for (i = 0; i < data.message.length; i++) {
				$("#docWord").append(
						'<option value="' + data.message[i].docNo + '" docNo="'
								+ data.message[i].id + '">'
								+ data.message[i].docNo + '</option>');
			}
		}
	});
});
// 案件编号改变的时候生成随机编号
function funDocWord() {
	$("#docNoId").val($("#docWord option:selected").attr("docNo"));
	if($("#docWord").val() != ""){
        $.ajax({
            url : "/luzheng/app/case!readDocumentNumber",
            data : {
                "id" : $("#docWord option:selected").attr("docNo")
            },
            // async:false,
            success : function(data) {
                data = JSON.parse(data);
                $("#docNumber").val(data.message);
            }
        });
    }else{
        $("#docNumber").val("");
	}
}