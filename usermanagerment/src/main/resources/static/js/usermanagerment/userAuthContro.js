var vt = $.cookie('userType');
// 超级管理员
if (vt == "0") {
    $("#organization").show();
    $("#case").hide();
    $("#information").hide();
    $("#satistical").hide();
    $("#SMS").hide();
}
// 执法单位
if (vt == "1") {
    $("#organization").show();
    $("#case").hide();
    $("#information").hide();
    $("#satistical").hide();
    $("#SMS").hide();
}
// 执法人员
if (vt == "2") {
    $("#organization").hide();
    $("#case").show();
    $("#information").show();
    $("#satistical").show();
    $("#SMS").show();
}