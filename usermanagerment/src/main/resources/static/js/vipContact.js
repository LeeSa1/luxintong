$(function() {
	var base = "/conference";
    var id = $.cookie('id');
    console.log(id);
    $.post(base+"/app/common!readVIPList",{
 	   "user.id":id
    },function(data){
    	data=JSON.parse(data);
    	console.log(data);
    	if (data.status=="success"){
    		var vipList = new Array();
    		var vips = "";
    		for (i=0;i<data.message.length;i++){
    			var vipUser = data.message[i];
    			vipList.push("<strong>嘉&emsp;宾：</strong>"+vipUser.name+" "+vipUser.office+vipUser.job)
    		}
    		vips = vipList.join("<br>");
    		$("#vip1 p").eq(0).html(vips);
    	}
    });
    $.post(base+"/app/common!readContactSchedule",{
    	   "user.id":id
    },function(data){
    	data=JSON.parse(data);
    	console.log(data);
    	if (data.status=="success"){
    		var list = new Array();
    		for (i=0;i<data.message.length;i++){
    			var dataObj = data.message[i];
    			var fromtime,totime,guest,contact,head,dock,escort;
    			var guestList = new Array();
    			var contactList = new Array();
    			if (dataObj.fromtime!="" && dataObj.totime!=""){
    				fromtime = dataObj.fromtime;
    				totime = dataObj.totime;
    			}else if (dataObj.fromtime!="" && dataObj.totime==""){
    				fromtime = dataObj.fromtime;
    				totime = "";
    			}else if (dataObj.fromtime=="" && dataObj.totime!=""){
    				fromtime = dataObj.totime;
    				totime = "";
    			}
    			if (dataObj.guestList!=null){
    				for (k=0;k<dataObj.guestList.length;k++){
    					var name=dataObj.guestList[k].name;
    					if (name.length==2){
    						var tmpname = name.substring(0,1)+"&emsp;"+name.substring(1);
    						name=tmpname;
    					}
    					guestList.push(name+" "+dataObj.guestList[k].office+dataObj.guestList[k].job);
    					if (dataObj.guestList[k].contactUser!=null){
    						contactList.push(name+"联系人: "+dataObj.guestList[k].contactUser.name+" <a href='tel:"+dataObj.guestList[k].contactUser.phone+"'>"+dataObj.guestList[k].contactUser.phone+"</a>");
    					}
    				}
    			}
    			guest="";
    			if (guestList.length>0){
    				guest=guestList.join("<br>");
    			}
    			contact="";
    			if (contactList.length>0){
    				contact=contactList.join("<br>");
    			}
    			if (dataObj.headList!=null){
    				head="";
    				for (k=0;k<dataObj.headList.length;k++){
    					head+="<span>负责人："+dataObj.headList[k].name+" <a href='tel:"+dataObj.headList[k].phone+"'>"+dataObj.headList[k].phone+"</a></span><br>"
					          
    				}
    			}
    			if (dataObj.dockList!=null){
    				dock="";
    				for (k=0;k<dataObj.dockList.length;k++){
    					dock+="<span>对接人："+dataObj.dockList[k].name+" <a href='tel:"+dataObj.dockList[k].phone+"'>"+dataObj.dockList[k].phone+"</a></span><br>"
    				}
    			}
    			if (dataObj.escortList!=null){
    				escort="";
    				for (k=0;k<dataObj.escortList.length;k++){
    					escort+="<span>陪同人："+dataObj.escortList[k].name+" <a href='tel:"+dataObj.escortList[k].phone+"'>"+dataObj.escortList[k].phone+"</a></span><br>"
    				}
    			}
    			console.log(head);
    			console.log(dock);
    			console.log(escort);
    			if (data.message[i].fromdate!=null && data.message[i].todate!=null){
    				for (j=data.message[i].fromdate.time;j<=data.message[i].todate.time;j+=86400000){
    					var obj=new Object();
    					obj.date=j;
    					obj.fromtime=fromtime;
    					obj.totime=totime;
    					obj.name=dataObj.name;
    					obj.content=dataObj.content;
    					obj.guest=guest;
    					obj.contact=contact;
    					obj.location=dataObj.location;
    					obj.locationId=dataObj.locationInfo.id;
    					obj.head=head;
    					obj.dock=dock;
    					obj.escort=escort;
    					obj.note=dataObj.note
    					list.push(obj);
    				}
    			}else if (data.message[i].fromdate!=null && data.message[i].todate==null){
    				var obj=new Object();
    				obj.date=dataObj.fromdate.time;
    				obj.fromtime=fromtime;
					obj.totime=totime;
					obj.name=dataObj.name;
					obj.content=dataObj.content;
					obj.guest=guest;
					obj.contact=contact;
					obj.location=dataObj.location;
					obj.locationId=dataObj.locationInfo.id;
					obj.head=head;
					obj.dock=dock;
					obj.escort=escort;
					obj.note=dataObj.note
					list.push(obj);
    			}else if (data.message[i].fromdate==null && data.message[i].todate!=null){
    				var obj=new Object();
    				obj.date=dataObj.todate.time;
    				obj.fromtime=fromtime;
					obj.totime=totime;
					obj.name=dataObj.name;
					obj.content=dataObj.content;
					obj.guest=guest;
					obj.contact=contact;
					obj.location=dataObj.location;
					obj.locationId=dataObj.locationInfo.id;
					obj.head=head;
					obj.dock=dock;
					obj.escort=escort;
					obj.note=dataObj.note
					list.push(obj);
    			}
    		}
    		console.log(list);
    		list.sort(function(a,b){
    			var index;
    			if (a.date!=b.date){
    				index = a.date-b.date;
    			}else{
    				var afrom=parseFloat(a.fromtime.substring(0,5).replace(":","."));
    				var bfrom=parseFloat(b.fromtime.substring(0,5).replace(":","."));
    				index = afrom-bfrom;
    			}
    			return index;
    		});
    		console.log(list);
    		var index=0;
    		var listday=new Array("星期日","星期一","星期二","星期三","星期四","星期五","星期六","星期日")
    		for (i=0;i<list.length;i++){
    			if (i==0||list[i].date-list[i-1].date>0){
    				index=0;
    			}else{
    				index++;
    			}
    			if (index==0){
    				var date=new Date(list[i].date);
    				$("table tbody").append('<tr class="tr-bg"><td colspan="3">'+date.getFullYear()+"年"+(date.getMonth()+1)+"月"+date.getDate()+"日，"+listday[date.getDay()])
    			}else{
    				$("table tbody").append("<tr><td colspan='2' style='background: #99ccff;'></td></tr>")
    			}
    			var time="";
    			if (list[i].fromtime!="" && list[i].totime!=""){
    				/*var timefrom=toAMPM(list[i].fromtime);
    				var timeto=toAMPM(list[i].totime);*/
    				var timefrom=list[i].fromtime;
    				var timeto=list[i].totime;
    				if (timefrom.split(":").length>2){
    					timefrom = timefrom.substring(0,timefrom.length-3);
    				}
    				if (timeto.split(":").length>2){
    					timeto = timeto.substring(0,timeto.length-3);
    				}
    				time=timefrom+"-"+timeto;
    			}else{
    				//var timefrom=toAMPM(list[i].fromtime);
    				var timefrom=list[i].fromtime;
    				if (timefrom.split(":").length>2){
    					timefrom = timefrom.substring(0,timefrom.length-3);
    				}
    				time=timefrom;
    			}
    			/*var guest = "";
    			if (list[i].guest!=""){
    				guest = "<span>"+list[i].guest+"</span>";
    			}*/
    			var duty = list[i].head+list[i].dock+list[i].escort;
    			if (duty.indexOf("<br>")>-1){
    				duty = duty.substring(0,duty.length-4);
    			}
    			/*var html = "<tr>"
    				     + "<td style='text-align:center;vertical-align:middle;'>"+time+"</td>"
    				     + "<td style='vertical-align:middle;'>"+"<span>"+list[i].name+"</span><br>"+"<span>"+list[i].content+"</span><br>"+guest+"<span>"+list[i].location+"</span>"+"</td>"
    				     + "<td style='width:20%;text-align:center;'>"+duty+"</td>"
    				     + "</tr>";*/
    			var date=new Date(list[i].date);
    			var html = "<tr><td style='text-align:center;'>活动名称</td><td>"+list[i].name+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>活动时间</td><td>"+(date.getMonth()+1)+"月"+date.getDate()+"日&emsp;"+time+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>活动地点</td><td onclick='lookLocation("+list[i].locationId+")'>"+list[i].location+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>嘉&emsp;&emsp;宾</td><td>"+list[i].guest+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>活动内容</td><td>"+list[i].content+"</td></tr>"
    			         //+ "<tr><td style='text-align:center;'>联&ensp;系&ensp;人</td><td>"+list[i].contact+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>对&ensp;接&ensp;人</td><td>"+duty+"</td></tr>"
    			         + "<tr><td style='text-align:center;'>备&emsp;&emsp;注</td><td>"+list[i].note+"</td></tr>";
    			         
    			console.log(html);
    			$("table tbody").append(html);
    		}
    	}
    });
});

function toAMPM(time){
    var times = time.split(":");
    if (parseInt(times[0],10)<12){
    	times[0]="上午"+parseInt(times[0],10);
    }else if (parseInt(times[0],10)==12){
    	times[0]="中午"+times[0];
    }else if (parseInt(times[0],10)>12 && parseInt(times[0],10)<18){
    	var ampmTime = parseInt(times[0],10)-12;
    	times[0]="下午"+ampmTime;
    }else if (parseInt(times[0],10)>17){
    	var ampmTime = parseInt(times[0],10)-12;
    	times[0]="晚上"+ampmTime;
    }
    var result = times.join(":");
    return result;
}

function lookLocation(locationId){
	window.open("map.html?locationId="+locationId);
}