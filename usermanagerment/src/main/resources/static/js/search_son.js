$(".content .remore").each(function() {
	if($(window).width() > 991) {
		var maxwidth = 120;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	} else {
		var maxwidth = 50;
		if($(this).text().length > maxwidth) {
			$(this).text($(this).text().substring(0, maxwidth));
			$(this).html($(this).html() + '...');
		}
	}
});

$('.img-box').find('img').each(function() {
	if($(this).attr('src') == "") {
		$(this).parents('.img-box').css('width', '0');
		$(this).parents('.img-box').next().css({
			'width': '100%'
		});
	}
});

/*for (var m = 0; m < 6; m++) {
	$('.page').before('<ul>'
			+'<li>'
			+'<a href="javascript:void(0)"><div>'
			+'<img src="img/simg.jpg">'
			+'</div></a>'
			+'<div>'
			+'<ul>'
			+'<a href="javascript:void(0)"><li>神经科思维养成：末梢感觉神经病变诊治思路</li></a>'
			+'<li class="remore">近期，Neurol Clin Pract 发表了一例持续性头痛和视野缺损患者，既往有先心病病史。对患者进行了 TCD 微栓子检测，最终明确了病因。 近期，Neurol Clin Pract 发表了一例持续性头痛和视野缺损患者，既往有先心病病史。对患者进行了 TCD 微栓子检测，最终明确了病因。 近期，Neurol Clin Pract 发表了一例持续性头痛和视野缺损患者，既往有先心病病史。对患者进行了 TCD 微栓子检测，最终明确了病因。</li>'
			+'</ul>'
			+'</div>'
	+'</li></ul>')
}*/

function smart_img($imgdom) {
	var w = $imgdom.width();
	var h = $imgdom.height();
	if(w > h) {
		$imgdom.css({
			'height': '100%'
		});
		var left = ($imgdom.width() - $imgdom.parent().width()) / 2 * (-1);
		$imgdom.css({
			'margin-left': left + 'px'
		});
		/*if($imgdom.width() < $imgdom.parent().width()) {
			$imgdom.css({ 'width': '100%', 'height': 'auto', 'margin-left': '0' });
		}*/
	} else {
		$imgdom.css({
			'width': '100%'
		});
	}
}

$(function() {
	$img = $('.img-box').find('img');
	smart_img($img);
})

$(function() {
	$table = $('.content>ul');
	$pageli = $('.page');
	$myson = $('.content>ul>li')
	mypage($table, 10, $pageli, $myson);
})