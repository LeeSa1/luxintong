//判断案件是否结案 如果结案页面元素不可用 页面底部只有返回按钮
function setReadOrEdit() {
    var caseId = GetQueryString("caseId");
    if ($.cookie(caseId) == "read") {
//        $("input").attr("disabled", true);
//        $("textarea").attr("disabled", true);
//        $("select").attr("disabled", true);
//        $("i").attr("onclick", null);//页面上的加号
//        $("div").attr("onclick", null);//行政处罚案件  附加材料垃圾桶删除按钮
        $("tr").each(
            function () {
                $(this).children().eq(2).hide();// tr下第三个td标签隐藏
                $(this).children().eq(3).children().eq(1).attr("title",
                    "查看").children().empty().html(
                    "<i class='ico-folder-open'>");
                // tr下第四个td中的 最后两个子元素隐藏
                $(this).children().eq(3).children().eq(0).hide();
                $(this).children().eq(3).children().eq(2).hide();
            });
        $(".form-actions")
            .empty()
            .html(
                "<button onclick='javascript:history.back(-1);' type='button' class='btn btn-danger'>返回</button>");
    }
}

$(function () {
    var caseId = GetQueryString("caseId");
    if ($.cookie(caseId) == "read") {
//        $("input").attr("disabled", true);
//        $("textarea").attr("disabled", true);
//        $("select").attr("disabled", true);
//        $("i").attr("onclick", null);
        $("tr").each(
            function () {
                $(this).children().eq(2).hide();// tr下第三个td标签隐藏
                $(this).children().eq(3).children().eq(0).attr("title",
                    "查看").children().empty().html(
                    "<i class='ico-folder-open'>");
                // tr下第四个td中的 最后两个子元素隐藏
                $(this).children().eq(3).children().eq(1).hide();
                $(this).children().eq(3).children().eq(2).hide();
            });
        $(".form-actions")
            .empty()
            .html(
                "<button onclick='javascript:history.back(-1);' type='button' class='btn btn-danger'>返回</button>");
    }
});
// 结案 改变案件状态
function finishCase() {
    var caseId = GetQueryString("caseId");
    var status = -1;
    if (window.confirm("确定结案,结案后不可再编辑案件!")) {
        $.ajax({
            url: "/luzheng/app/case!updateCaseInfo",
            data: {
                "caseInfo.id": caseId,
                "caseInfo.status": status
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == "success") {
                    if ("1" == obj.message) {
                        window.location.href = "cf-case.html";
                    } else if ("2" == obj.message) {
                        window.location.href = "pb-case.html";
                    } else if ("4" == obj.message) {
                        window.location.href = "xk-case.html";
                    } else{
                        window.location.href = "qz-case.html";
                    }
                }
            }
        });
    }
}
//移交处理
function HandOver() {
    window.location.href = "case-changeOfficer.html?caseId=" + GetQueryString("caseId");
}
