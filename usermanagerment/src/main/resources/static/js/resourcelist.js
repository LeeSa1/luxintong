﻿$(".content .remore").each(function() {
    if ($(window).width() > 1080) {
        var maxwidth = 60;
        if ($(this).text().length > maxwidth) {
            $(this).text($(this).text().substring(0, maxwidth));
            $(this).html($(this).html() + '...');
        }
    } else {
        var maxwidth = 50;
        if ($(this).text().length > maxwidth) {
            $(this).text($(this).text().substring(0, maxwidth));
            $(this).html($(this).html() + '...');
        }
    }
});

$('.img-box').find('img').each(function() {
    if ($(this).attr('src') == "") {
        $(this).parents('.img-box').css('width', '0');
        $(this).parents('.img-box').next().css({
            'width': '100%',
            'padding-left': '0'
        });
    }
});

function smart_img($imgdom) {
    var w = $imgdom.width();
    var h = $imgdom.height();
    if (w > h) {
        $imgdom.css({
            'height': '100%'
        });
        var left = ($imgdom.width() - $imgdom.parent().width()) / 2 * ( - 1);
        $imgdom.css({
            'margin-left': left + 'px'
        });
        if ($imgdom.width() < $imgdom.parent().width()) {
            $imgdom.css({
                'width': '100%',
                'height': 'auto',
                'margin-left': '0'
            });
        }
    } else {
        $imgdom.css({
            'width': '100%'
        });
    }
}

$(function() {
    $img = $('.img-box').find('img');
    smart_img($img);

    $('#wrapper .right2 .head>i').on('click',
    function() {
        $('#wrapper .right2 .content').slideToggle();
        $('#wrapper .right2>ul').fadeToggle();
    })

    $('#wrapper .right2 .head>i').toggle(function() {
        $(this).attr('class', 'glyphicon glyphicon-chevron-up');
    },
    function() {
        $(this).attr('class', 'glyphicon glyphicon-chevron-down')
    })
})

$(function() {
    $('.right2 ul').eq(0).find('li').eq(0).find('a').attr('href', 'resourcelist.html?channel=0');
    $('.right2 ul').eq(0).find('li').eq(1).find('a').attr('href', 'resourcelist.html?channel=1');
    $('.right2 ul').eq(0).find('li').eq(2).find('a').attr('href', 'resourcelist.html?channel=2');
    $('.right2 ul').eq(0).find('li').eq(3).find('a').attr('href', 'resourcelist.html?channel=3');
    $('.right2 ul').eq(0).find('li').eq(4).find('a').attr('href', 'resourcelist.html?channel=4');
    $table = $('.left .content>ul');
    $pageli = $('.left .content .page');
    var base = "/hoslibrary";
    var token = $.cookie('token');
    var id = $.cookie('id');
    var channel = getQueryString('channel');
    if (!channel) channel = '';
    var subject = getQueryString('subject');
    if (!subject) subject = '';
    if (channel != '') {
        var sChannel = '';
        if (channel == "0") {
            sChannel = '书籍';
        } else if (channel == "1") {
            sChannel = '视频';
        } else if (channel == "2") {
            sChannel = '课件';
        } else if (channel == "3") {
            sChannel = '考试';
        } else if (channel == "4") {
            sChannel = '论文';
        }
		$('.top li a').text('');
	    $('.top li a').eq(0).text(sChannel);
	    $('.top li a').eq(0).attr('href','resource.html?channel='+channel);
        var sSubject = '';
        if (subject != '') {
            if (channel < 3) {
                if (subject == 1) {
                    sSubject = '-中医';
                } else if (subject == 2) {
                    sSubject = '-外科';
                } else if (subject == 3) {
                    sSubject = '-医技科';
                } else if (subject == 4) {
                    sSubject = '-精神科';
                } else if (subject == 5) {
                    sSubject = '-医学保健';
                } else if (subject == 6) {
                    sSubject = '-儿科';
                } else if (subject == 7) {
                    sSubject = '-妇产科';
                } else if (subject == 8) {
                    sSubject = '-内科';
                } else if (subject == 9) {
                    sSubject = '-医药学';
                } else if (subject == 10) {
                    sSubject = '-男科与生殖医学';
                } else {
                    sSubject = '-其他';
                }
            } else {
                if (subject == 0) {
                    sSubject = '-执业医师';
                } else if (subject == 1) {
                    sSubject = '-医学考研';
                } else if (subject == 2) {
                    sSubject = '-药师考试';
                } else if (subject == 3) {
                    sSubject = '-口腔医学考试';
                } else {
                    sSubject = '-其他';
                }
            }
			$('.top li i').eq(0).show();
			$('.top li a').eq(1).text(decodeURI(sSubject.replace('-','')));
			$('.top li a').eq(1).attr('href','resource.html?channel='+channel+'&subject='+subject);
        }
        $('.left .head span').text(sChannel + sSubject);
    } else {
		$('.top li a').text('');
	    $('.top li a').eq(0).text('资源');
	    $('.top li a').eq(0).attr('href','resource.html');
        $('.left .head span').text('资源');
    }
    //$('.right .content ul').html('');
    nextSubject(0);

    $.post(base + "/app/common!readResourceList", {
        "user.token": token,
        "user.id": id,
        channel: channel,
        subject: subject
    },
    function(data) {
        console.log(data);
        data = JSON.parse(data);
        if (data.status == 'success') {
            //$('.top li a').eq(3).text('共'+data.message.total+'条记录');
			$('.top li a').eq(3).text('');
            $table.html('');
            for (i = 0; i < data.message.resultList.length; i++) {
                var obj = data.message.resultList[i];
                var time = new Date(obj.createdAt);
                var imgStyle = '';
                var divStyle = '';
                if (!obj.pic || obj.pic == '') {
                    imgStyle = 'style="width:0"';
                    divStyle = 'style="width: 100%; padding-left: 0;"';
                }
                //var link = 'detail.html?type=policy&id='+obj.id;
				var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
                var html = '<li data-id="' + obj.id + '" data-channel="' + channel + '" data-name="' + obj.name + '" data-author="' + obj.author + '" data-cat="' + obj.subjectId + '" data-press="' + obj.press + '" data-pic="' + pic + '" data-desc="' + obj.desc + '" data-link="' + obj.link + '" data-xcode="' + obj.xcode + '" data-time="' + obj.createdAt + '"><div class="img-box" ' + imgStyle + '><a href="javascript:void(0)"><img src="' + pic + '"></a></div>' + '<div ' + divStyle + '><ul><li><a href="javascript:void(0)">' + obj.name + '</a></li>' + '<li class="remore">' + obj.desc + '</li>' + '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>' + time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate() + '</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
                $table.append(html);
                $myson = $('.left .content>ul>li');
            }
            mypage($table, 10, data.message.index - 1, data.message.total, $pageli, $myson);
        } else if (data.status == 'token') {
            window.location.href = 'login.html';
        }
    });
    $('.left .content').on('click', 'li div ul li:nth-of-type(1)',function() {
        var channel = $(this).parent().parent().parent().attr('data-channel');
        if (channel < 3) {
			$('.modal-box .tansuo').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
		    $('#discuss').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
		    $('#report').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
			if ($(this).parent().parent().parent().attr('data-pic')==undefined||$(this).parent().parent().parent().attr('data-pic')==''){
			$('.modal-box img').eq(0).attr('src','img/default.png');
		}else{
			$('.modal-box img').eq(0).attr('src',$(this).parent().parent().parent().attr('data-pic'));
		}
            
            $('.modal-box li').eq(0).find('span').text($(this).parent().parent().parent().attr('data-name'));
            $('.modal-box li').eq(1).find('span').text($(this).parent().parent().parent().attr('data-author'));
            $('.modal-box li').eq(2).find('span').text($(this).parent().parent().parent().attr('data-press'));
            var subject = $(this).parent().parent().parent().attr('data-cat');
            var sSubject = '';
            console.log(subject);
            if (subject == 1) {
                sSubject = '中医';
            } else if (subject == 2) {
                sSubject = '外科';
            } else if (subject == 3) {
                sSubject = '医技科';
            } else if (subject == 4) {
                sSubject = '精神科';
            } else if (subject == 5) {
                sSubject = '医学保健';
            } else if (subject == 6) {
                sSubject = '儿科';
            } else if (subject == 7) {
                sSubject = '妇产科';
            } else if (subject == 8) {
                sSubject = '内科';
            } else if (subject == 9) {
                sSubject = '医药学';
            } else if (subject == 10) {
                sSubject = '男科与生殖医学';
            } else {
                sSubject = '其他';
            }
            console.log(sSubject);
            $('.modal-box li').eq(4).find('span').text(sSubject);
            $('.modal-box li').eq(6).find('span').text($(this).parent().parent().parent().attr('data-xcode'));
            $('.modal-box p').text($(this).parent().parent().parent().attr('data-desc'));
            $('.modal-box button').eq(0).attr('onClick', 'openurl("' + $(this).parent().parent().parent().attr('data-link') + '","resource",'+$(this).parent().parent().parent().attr('data-id')+')');
            $('#modal').fadeIn(100).find('.modal-box').fadeIn(100); 
			/*$('.modal-box').find('img').each(function() {
                $(this).height($(this).width() / 3 * 4);
                var m = $(this).height() / 2 * ( - 1);
                $(this).css({
                    'top': '50%',
                    'marginTop': m
                })
            })*/
			var img = $('.modal-box img').eq(0);
		    $(img).height($(img).width() / 3 * 4);
		    var m = $(img).height() / 2 * (-1);
		    $(img).css({
				'top': '50%',
				'marginTop': m
		    })
        } else if (channel > 2){
			console.log($(this));
			$('.modal-box2 .tansuo').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
		    $('#discuss').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
		    $('#report').attr('data-id',$(this).parent().parent().parent().attr('data-id'));
            $('.modal-box2 li').eq(0).find('h3').text($(this).parent().parent().parent().attr('data-name'));
            $('.modal-box2 li').eq(1).find('span').text($(this).parent().parent().parent().attr('data-author'));
            console.log($(this).parent().parent().parent().attr('data-time'));
            var time = new Date(parseInt($(this).parent().parent().parent().attr('data-time')));
            $('.modal-box2 li').eq(2).find('span').text(time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate());
            $('.modal-box2 li').eq(3).find('span').text($(this).parent().parent().parent().attr('data-xcode'));
            $('.modal-box2 p').text($(this).parent().parent().parent().attr('data-desc'));
            $('.modal-box2 button').eq(0).attr('onClick', 'openurl("' + $(this).parent().parent().parent().attr('data-link') + '","resource",'+$(this).parent().parent().parent().attr('data-id')+')');
            $('#modal').fadeIn(100).find('.modal-box2').fadeIn(100)
        }
    });
	$('.modal-box , .modal-box2').on('click', '.btn-danger',function() {
        $('#modal').fadeOut(100).find('.modal-box, .modal-box2').fadeOut(100)
    })
	$('.left .content>ul').on('click','li .info div:nth-of-type(2)',function(){
        $('#discuss').attr('data-id',$(this).attr('data-id'));
		toDiscuss();
	});
	$('#discuss .btn-danger').on('click',function(){
				$('#discuss').slideUp(300);
			})
			
			$('#discuss .btn-info').on('click', function(){
				var content = $('#discuss .user-box textarea').val();
				if (content == ''){
					alert('请输入心得！');
					return;
				}else{
					$('#discuss .user-box textarea').val('');
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#discuss').attr('data-id');
				$('.comment>ul').html('');
				$.post(base+'/app/common!createComment',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "comment.content":content
					   },function(data){
						   console.log(data);
						   listDiscuss(1);
					   });
			})
})

$('#report .btn-info').on('click', function(){
				var errtype = $('input[name="report"]').val();
				var content = $('#report textarea').val();
				if (errtype == ''){
					alert('请选择错误类型！');
					return;
				}
				var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#report').attr('data-id');
				$.post(base+'/app/common!createCorrect',{
					       "user.token":token,
			               "user.id":id,
						   "correct.type":errtype,
						   "correct.resType":type,
						   "correct.resId":resId,
						   "correct.content":content
					   },function(data){
						   data =JSON.parse(data);
						   alert(data.message);
						   $('#report').hide();
					   });
			})

function showCollection(obj){
    var cangs_list = '';
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
    var txt = obj.parent().parent().find('div:first-child').text();
	console.log(obj.parent().parent());
	console.log(txt);
	var type = "resource";
	var resId = obj.parents('.tansuo').attr('data-id');
    var message_id = resId;
    $.get(
        base+'/app/common!readCollectList',{
			"user.token":token,
			"user.id":id,
		    "collect.resType":type,
			"collect.resId":resId,
		},function(data){
			console.log(data);
			data =JSON.parse(data);
            for(var i = 0 ;i < data.message.resultList.length; i ++){
                cangs_list += data.message.resultList[i].resId+',';
            }
            var arr = cangs_list.split(',');
            var system = {
                win: false,
                mac: false,
                xll: false,
                ipad:false
            };
            //检测平台
            var p = navigator.platform;
            system.win = p.indexOf("Win") == 0;
            system.mac = p.indexOf("Mac") == 0;
            system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
            system.ipad = (navigator.userAgent.match(/iPad/i) != null)?true:false;
            var isPC = 1;
            if (system.win || system.mac || system.xll) {

            } else {
                isPC = 0;
            }
            //收藏、取消收藏
            if($.inArray(resId,arr) < 0){
				console.log(obj.parent().parent().find('div:first-child'));
                obj.parent().parent().find('div:first-child').text('收藏');
				obj.parent().parent().width('80px');
                if (isPC == 1)
                {obj.parent().parent().find('div:first-child').show();}
            }
            else{
				console.log(obj.parent().parent().find('div:first-child'));
                obj.parent().parent().find('div:first-child').text('取消收藏');
				obj.parent().parent().width('115px');
                if (isPC == 1)
                {obj.parent().parent().find('div:first-child').show();}
            }
			
        }
    );
};
    function hideCollection(obj){
	obj.parent().parent().find('div:first-child').hide();
}
function collection(obj){
	//alert(token);
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
    var txt = obj.find('div:first-child').text();
	var type = "resource";
	var resId = obj.parents('.tansuo').attr('data-id');
	console.log(resId);
    if(token == '' || token == undefined){
        alert('登录后才可以收藏');
    }
    else{
        var message_id = resId;
        if(txt == '收藏'){
            //调用收藏接口
            $.get(
                base+'/app/common!createCollect',
				{
					"user.token":token,
			        "user.id":id,
				    "collect.resType":type,
					"collect.resId":resId,
				},
				function(data){
                    console.log(data);
                    //if(data.status == 200){
                        data =JSON.parse(data);
						alert(data.message);
                        obj.find('div:first-child').text('取消收藏');
                    //}
                }
            )
        }
        if(txt == '取消收藏'){
            //调用取消收藏接口
            $.get(
                base+'/app/common!cancelCollect',{
					"user.token":token,
			        "user.id":id,
				    "collect.resType":type,
					"collect.resId":resId,
				},function(data){
                    //if(data.status == 200){
						data =JSON.parse(data);
						alert(data.message);
                        obj.find('div:first-child').text('收藏');
                    //}
                }
            )
        }
    }
};
/*点赞功能*/
function zan(obj){
    $('#report').show();
};

/*分享功能*/
function share(){
    $('#share1').click();
};
/*评论弹框*/
function toDiscuss(){
	            listDiscuss(1);
				$('#discuss').fadeIn(300);
}

function listDiscuss(pageindex){
	            var base = "/hoslibrary";
	            var token = $.cookie('token');
                var id = $.cookie('id');
				var type = "resource";
	            var resId = $('#discuss').attr('data-id');

				$('.comment>ul').html('');
				$('#discuss .page .title').remove();
				$.post(base+'/app/common!readCommentList',{
					       "user.token":token,
			               "user.id":id,
						   "comment.resType":type,
						   "comment.resId":resId,
						   "page.index":pageindex
					   },function(data){
						   console.log(data);
						   data = JSON.parse(data);
			               if (data.status == 'success'){
							   for (i=0;i<data.message.resultList.length;i++){
								   var obj = data.message.resultList[i];
								   var time = new Date(obj.createdAt);
								   var photo = obj.user.photo;
								   if (photo.indexOf('/upload')==0){
									   photo = base+photo;
								   }
								   var html = '<li><div><img src="'+photo+'" /></div>'
								            + '<div><p><b>'+obj.user.name+': </b><span>'+obj.content+'</span></p>'
								            + '<p>'+time.getFullYear()+'-'+fillzero((time.getMonth()+1))+'-'+fillzero(time.getDate())+' '+fillzero(time.getHours())+':'+fillzero(time.getMinutes())+':'+fillzero(time.getSeconds())+'</p></div></li>';
								   $('.comment>ul').append(html);
							   }
							   $table = $('#discuss .comment>ul');
				               $pageli = $('#discuss .page');
				               $myson = $('#discuss .comment>ul>li')
				               mypage($table, 10,data.message.index-1,data.message.resultList.length, $pageli, $myson);
						   }
					   });

}

function closeCorrect(){
	$('#report').hide();
}

function showTip(obj){
	var system = {
                win: false,
                mac: false,
                xll: false,
                ipad:false
            };
            //检测平台
            var p = navigator.platform;
            system.win = p.indexOf("Win") == 0;
            system.mac = p.indexOf("Mac") == 0;
            system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);
            system.ipad = (navigator.userAgent.match(/iPad/i) != null)?true:false;
            var isPC = 1;
            if (system.win || system.mac || system.xll) {

            } else {
                isPC = 0;
            }
	if (isPC == 1){
	obj.parents("li").find("div").eq(0).show();}
	obj.css('opacity','1');
}
function hideTip(obj){
	obj.parents("li").find("div").eq(0).hide();
	obj.css('opacity','0.8');
}

function fillzero(s){
			if (parseInt(s)<10){
				return '0'+s;
			}else{
				return s;
			}
		}

function doSearch() {
    $table = $('.left .content>ul');
    $pageli = $('.left .content .page');
    var base = "/hoslibrary";
    var token = $.cookie('token');
    var id = $.cookie('id');
    var pageindex = $('#pageindex').val();
    console.log(pageindex);
    var s = $('.search #search-input').val();
    var channel = getQueryString('channel');
    if (!channel) channel = '';
    var subject = getQueryString('subject');
    if (!subject) subject = '';
    $('.left .content .page .title').remove();

    $.post(base + "/app/common!readResourceList", {
        "user.token": token,
        "user.id": id,
        channel: channel,
        subject: subject,
        name: s,
        "page.index": pageindex
    },
    function(data) {
        console.log(data);
        data = JSON.parse(data);
        if (data.status == 'success') {
            if (s != ''){
				   $('.top li a').eq(3).text('搜索关键词：'+s+'，共'+data.message.total+'条搜索结果');
			   }else{
				   //$('.top li a').eq(3).text('共'+data.message.total+'条记录');
				   $('.top li a').eq(3).text('');
			   }
            $table.html('');
            for (i = 0; i < data.message.resultList.length; i++) {
                var obj = data.message.resultList[i];
                var time = new Date(obj.createdAt);
                var imgStyle = '';
                var divStyle = '';
                if (!obj.pic || obj.pic == '') {
                    imgStyle = 'style="width:0"';
                    divStyle = 'style="width: 100%; padding-left: 0;"';
                }
                //var link = 'detail.html?type=policy&id='+obj.id;
				var pic = obj.pic;
				   if (pic.indexOf('/upload')==0){
					   pic = base+pic;
				   }
                var html = '<li data-id="' + obj.id + '" data-channel="' + channel + '" data-name="' + obj.name + '" data-author="' + obj.author + '" data-cat="' + obj.subjectId + '" data-press="' + obj.press + '" data-pic="' + pic + '" data-desc="' + obj.desc + '" data-link="' + obj.link + '" data-xcode="' + obj.xcode + '" data-time="' + obj.createdAt + '"><div class="img-box" ' + imgStyle + '><a href="javascript:void(0)"><img src="' + pic + '"></a></div>' + '<div ' + divStyle + '><ul><li><a href="javascript:void(0)">' + obj.name + '</a></li>' + '<li class="remore">' + obj.desc + '</li>' + '<li class="info"><div><i class="glyphicon glyphicon-time"></i><span>' + time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate() + '</span></div><div data-id="'+obj.id+'"><i class="glyphicon glyphicon-comment"></i><span>'+obj.commentCount+'</span></div></li></ul></div></li>';
                $table.append(html);
                $myson = $('.left .content>ul>li');
            }
            mypage($table, 10, data.message.index - 1, data.message.total, $pageli, $myson);
        } else if (data.status == 'token') {
            window.location.href = 'login.html';
        }
    });
}

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return r[2];
    return null;
}

function nextSubject(index) {
    var base = "/hoslibrary";
    $('.right .content h4').eq(index).find('a').attr('href', 'resourcelist.html?channel=' + index);
    $('.right2 .content h4').eq(index).find('a').attr('href', 'resourcelist.html?channel=' + index);
    for (i = 0; i < $('.right .content ul').eq(index).find('li').length; i++) {
        if (index < 3) {
            $('.right .content ul').eq(index).find('li').eq(i).find('a').attr('href', 'resourcelist.html?channel=' + index + '&subject=' + (i + 1));
            $('.right2 .content ul').eq(index).find('li').eq(i).find('a').attr('href', 'resourcelist.html?channel=' + index + '&subject=' + (i + 1));
        } else {
            $('.right .content ul').eq(index).find('li').eq(i).find('a').attr('href', 'resourcelist.html?channel=' + index + '&subject=' + i);
            $('.right .content ul').eq(index).find('li').eq(i).find('a').attr('href', 'resourcelist.html?channel=' + index + '&subject=' + i);
        }
    }
    if (index < 4) {
        nextSubject(index + 1);
    }
}

function searchText() {
    //if ($('.search #search-input').val() != '') {
        $('#pageindex').val(1);
        $('.page .title').remove();
        doSearch();
    //}
}

function openurl(url,resType,resId){
	var base = "/hoslibrary";
	var token = $.cookie('token');
    var id = $.cookie('id');
	$.post(base+"/app/common!logView",{
		                "user.token":token,
			            "user.id":id,
						"viewlog.resType":resType,
			            "viewlog.resId":resId
		            },function(data){
						console.log(data);
					}
				);
    window.open(base+'/sysmgr/openresource?url='+url);
}

function nofind(obj){
	obj.src="img/default.png";
}
