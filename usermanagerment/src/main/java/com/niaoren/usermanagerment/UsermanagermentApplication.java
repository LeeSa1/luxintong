package com.niaoren.usermanagerment;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.niaoren.usermanagerment.mapper")
public class UsermanagermentApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsermanagermentApplication.class, args);
	}

}
