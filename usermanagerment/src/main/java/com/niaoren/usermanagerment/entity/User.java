package com.niaoren.usermanagerment.entity;

public class User {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Short id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.user_type
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Integer userType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.user_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String userName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.real_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String realName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.password
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String password;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.add_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Long addTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.card_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String cardId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.office_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Integer officeId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.auth_big_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String authBigCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.auth_small_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String authSmallCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.user_staff_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Integer userStaffId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.register_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String registerTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user.write_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String writeTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public User(Short id, Integer userType, String userName, String realName, String password, Long addTime, String cardId, Integer officeId, String authBigCode, String authSmallCode, Integer userStaffId, String registerTime, String writeTime) {
        this.id = id;
        this.userType = userType;
        this.userName = userName;
        this.realName = realName;
        this.password = password;
        this.addTime = addTime;
        this.cardId = cardId;
        this.officeId = officeId;
        this.authBigCode = authBigCode;
        this.authSmallCode = authSmallCode;
        this.userStaffId = userStaffId;
        this.registerTime = registerTime;
        this.writeTime = writeTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public User() {
        super();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.id
     *
     * @return the value of user.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Short getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.id
     *
     * @param id the value for user.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setId(Short id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.user_type
     *
     * @return the value of user.user_type
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.user_type
     *
     * @param userType the value for user.user_type
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.user_name
     *
     * @return the value of user.user_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getUserName() {
        return userName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.user_name
     *
     * @param userName the value for user.user_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.real_name
     *
     * @return the value of user.real_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getRealName() {
        return realName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.real_name
     *
     * @param realName the value for user.real_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.password
     *
     * @return the value of user.password
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getPassword() {
        return password;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.password
     *
     * @param password the value for user.password
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.add_time
     *
     * @return the value of user.add_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Long getAddTime() {
        return addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.add_time
     *
     * @param addTime the value for user.add_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.card_id
     *
     * @return the value of user.card_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getCardId() {
        return cardId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.card_id
     *
     * @param cardId the value for user.card_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setCardId(String cardId) {
        this.cardId = cardId == null ? null : cardId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.office_id
     *
     * @return the value of user.office_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Integer getOfficeId() {
        return officeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.office_id
     *
     * @param officeId the value for user.office_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setOfficeId(Integer officeId) {
        this.officeId = officeId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.auth_big_code
     *
     * @return the value of user.auth_big_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getAuthBigCode() {
        return authBigCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.auth_big_code
     *
     * @param authBigCode the value for user.auth_big_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setAuthBigCode(String authBigCode) {
        this.authBigCode = authBigCode == null ? null : authBigCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.auth_small_code
     *
     * @return the value of user.auth_small_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getAuthSmallCode() {
        return authSmallCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.auth_small_code
     *
     * @param authSmallCode the value for user.auth_small_code
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setAuthSmallCode(String authSmallCode) {
        this.authSmallCode = authSmallCode == null ? null : authSmallCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.user_staff_id
     *
     * @return the value of user.user_staff_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Integer getUserStaffId() {
        return userStaffId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.user_staff_id
     *
     * @param userStaffId the value for user.user_staff_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setUserStaffId(Integer userStaffId) {
        this.userStaffId = userStaffId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.register_time
     *
     * @return the value of user.register_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getRegisterTime() {
        return registerTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.register_time
     *
     * @param registerTime the value for user.register_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime == null ? null : registerTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user.write_time
     *
     * @return the value of user.write_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getWriteTime() {
        return writeTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user.write_time
     *
     * @param writeTime the value for user.write_time
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setWriteTime(String writeTime) {
        this.writeTime = writeTime == null ? null : writeTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        User other = (User) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUserType() == null ? other.getUserType() == null : this.getUserType().equals(other.getUserType()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getRealName() == null ? other.getRealName() == null : this.getRealName().equals(other.getRealName()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getAddTime() == null ? other.getAddTime() == null : this.getAddTime().equals(other.getAddTime()))
            && (this.getCardId() == null ? other.getCardId() == null : this.getCardId().equals(other.getCardId()))
            && (this.getOfficeId() == null ? other.getOfficeId() == null : this.getOfficeId().equals(other.getOfficeId()))
            && (this.getAuthBigCode() == null ? other.getAuthBigCode() == null : this.getAuthBigCode().equals(other.getAuthBigCode()))
            && (this.getAuthSmallCode() == null ? other.getAuthSmallCode() == null : this.getAuthSmallCode().equals(other.getAuthSmallCode()))
            && (this.getUserStaffId() == null ? other.getUserStaffId() == null : this.getUserStaffId().equals(other.getUserStaffId()))
            && (this.getRegisterTime() == null ? other.getRegisterTime() == null : this.getRegisterTime().equals(other.getRegisterTime()))
            && (this.getWriteTime() == null ? other.getWriteTime() == null : this.getWriteTime().equals(other.getWriteTime()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserType() == null) ? 0 : getUserType().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getAddTime() == null) ? 0 : getAddTime().hashCode());
        result = prime * result + ((getCardId() == null) ? 0 : getCardId().hashCode());
        result = prime * result + ((getOfficeId() == null) ? 0 : getOfficeId().hashCode());
        result = prime * result + ((getAuthBigCode() == null) ? 0 : getAuthBigCode().hashCode());
        result = prime * result + ((getAuthSmallCode() == null) ? 0 : getAuthSmallCode().hashCode());
        result = prime * result + ((getUserStaffId() == null) ? 0 : getUserStaffId().hashCode());
        result = prime * result + ((getRegisterTime() == null) ? 0 : getRegisterTime().hashCode());
        result = prime * result + ((getWriteTime() == null) ? 0 : getWriteTime().hashCode());
        return result;
    }
}