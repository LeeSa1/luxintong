package com.niaoren.usermanagerment.entity;

public class UserMenuAccess {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_menu_access.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_menu_access.menu_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private String menuName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_menu_access.root_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    private Integer rootId;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_menu_access
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public UserMenuAccess(Integer id, String menuName, Integer rootId) {
        this.id = id;
        this.menuName = menuName;
        this.rootId = rootId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_menu_access
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public UserMenuAccess() {
        super();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_menu_access.id
     *
     * @return the value of user_menu_access.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_menu_access.id
     *
     * @param id the value for user_menu_access.id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_menu_access.menu_name
     *
     * @return the value of user_menu_access.menu_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public String getMenuName() {
        return menuName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_menu_access.menu_name
     *
     * @param menuName the value for user_menu_access.menu_name
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_menu_access.root_id
     *
     * @return the value of user_menu_access.root_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public Integer getRootId() {
        return rootId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_menu_access.root_id
     *
     * @param rootId the value for user_menu_access.root_id
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    public void setRootId(Integer rootId) {
        this.rootId = rootId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_menu_access
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserMenuAccess other = (UserMenuAccess) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getMenuName() == null ? other.getMenuName() == null : this.getMenuName().equals(other.getMenuName()))
            && (this.getRootId() == null ? other.getRootId() == null : this.getRootId().equals(other.getRootId()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_menu_access
     *
     * @mbggenerated Tue May 28 10:54:19 CST 2019
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getMenuName() == null) ? 0 : getMenuName().hashCode());
        result = prime * result + ((getRootId() == null) ? 0 : getRootId().hashCode());
        return result;
    }
}