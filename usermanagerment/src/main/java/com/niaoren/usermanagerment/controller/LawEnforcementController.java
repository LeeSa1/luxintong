package com.niaoren.usermanagerment.controller;

import com.niaoren.usermanagerment.common.BaseResult;
import com.niaoren.usermanagerment.entity.Unit;
import com.niaoren.usermanagerment.entity.UserStaff;
import com.niaoren.usermanagerment.service.UnitService;
import com.niaoren.usermanagerment.service.UserStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LawEnforcementController {

    @Autowired
    private UnitService unitService;
    @Autowired
    private UserStaffService userStaffService;

    @GetMapping("/findAllOrgInfo")
    public BaseResult<List<Unit>> findAllOrgInfo(@RequestParam("userId") Integer userId,
                                                 @RequestParam("userType") Integer userType) {

        if (userType == 0) {
            List<Unit> units = unitService.getUnitList(userId);
            if (units != null) {
                return BaseResult.ok(units);
            }
        }
        return BaseResult.notok("无权限");

    }

    @GetMapping("/readAllOfficer")
    public BaseResult<List<UserStaff>> readAllOfficer(@RequestParam("id") Integer id) {
        List<UserStaff> userStaffs = userStaffService.getUserStaffList(id);
        if (userStaffs != null) {
            return BaseResult.ok(userStaffs);
        }
        return null;
    }

    @GetMapping("/readUserList")
    public BaseResult<List<Unit>> readUserList(@RequestParam("userId") Integer id) {
        List<Unit> unitList = unitService.getUnitList(id);
        if (unitList != null) {
            return BaseResult.ok(unitList);
        }
        return BaseResult.notok("查询失败");
    }

    @GetMapping("/readOrgInfo")
    public BaseResult<List<Unit>> getUnitName(@RequestParam("id") Integer id) {
        List<Unit> unitList = unitService.getUnitList(id);
        if (unitList != null) {
            return BaseResult.ok(unitList);
        }
        return BaseResult.notok("查询失败");
    }

    /**
     * 获取单位列表
     *
     * @creatDate 2019/5/29 9:59
     */
    @PostMapping("/findAllOrgInfoByUser")
    @ResponseBody
    public BaseResult<List<Unit>> findAllOrgInfoByUser(@RequestParam("userId") Integer id) {
        List<Unit> unitList = unitService.getUnitList(id);
        if (unitList != null) {
            return BaseResult.ok(unitList);
        }
        return BaseResult.notok("暂无单位信息");
    }

    /**
     * 获取个人列表
     *
     * @creatDate 2019/5/29 9:59
     */
    @PostMapping("/readOfficerList")
    @ResponseBody
    public BaseResult<List<UserStaff>> readOfficerList(@RequestParam("officeId") Integer officeId) {
        List<UserStaff> userStaffList = userStaffService.getUserStaffList(officeId);
        if (userStaffList != null) {
            return BaseResult.ok(userStaffList);
        }
        return BaseResult.notok("暂无个人信息");
    }

    /**
     * 添加用户
     *
     * @creatDate 2019/5/29 10:02
     */
    @PostMapping("/addUser")
    @ResponseBody
    public BaseResult addUser(@RequestParam("userType") Integer userType,  //用户类型
                              @RequestParam(value = "userOfficeId",required = false) Integer userOfficeId,   //
                              @RequestParam(value = "userCardId",required = false) String userCardId,
                              @RequestParam(value = "userUserName",required = false) String userUserName) {
        return BaseResult.ok();
    }


}
