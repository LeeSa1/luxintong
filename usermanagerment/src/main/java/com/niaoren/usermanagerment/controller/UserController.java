package com.niaoren.usermanagerment.controller;

import com.niaoren.usermanagerment.common.BaseResult;
import com.niaoren.usermanagerment.entity.UserStaff;
import com.niaoren.usermanagerment.service.UserService;
import com.niaoren.usermanagerment.service.UserStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    @Autowired
    private UserStaffService userStaffService;

    @Autowired
    private UserService userService;

    @GetMapping("/readUserInfo")
    @ResponseBody
    public BaseResult<UserStaff> readUserInfo(@RequestParam("id") Integer id) {
        UserStaff userInfo = userStaffService.getUserInfo(id);
        return BaseResult.ok(userInfo);
    }

    @GetMapping("pwdreset")
    public String pwdreset() {
        return "pwdreset";
    }

    @GetMapping("/logout")
    @ResponseBody
    public BaseResult logout(){
        return BaseResult.ok();
    }

    @PostMapping("/updatePassword")
    @ResponseBody
    public BaseResult updatePassword(@RequestParam("id") Integer id,
                                     @RequestParam("password") String password,
                                     @RequestParam("newPassword") String newPassword){

        int i = userService.updatePassword(id, password, newPassword);
        if (i != 0) {
            return BaseResult.ok();
        }
        return BaseResult.notok("修改失败");
    }

}
