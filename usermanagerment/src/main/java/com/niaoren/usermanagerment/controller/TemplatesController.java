package com.niaoren.usermanagerment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TemplatesController {

    @GetMapping("/")
    public String login() {
        return "login";
    }

    @GetMapping("/login")
    public String logout() {
        return "login";
    }

    //执法单位
    @GetMapping("/index")
    public String index() {
        return "index";
    }

    //首页
    @GetMapping("/home")
    public String home() {
        return "/home";
    }

    //
    @GetMapping("/index-increase")
    public String indexIncrease() {
        return "index-increase";
    }

    //执法人员
    @GetMapping("/index-officer")
    public String indexOfficer() {
        return "index-officer";
    }

    //账号管理
    @GetMapping("/index-id")
    public String indexId() {
        return "index-id";
    }

    @GetMapping("/cf-case")
    public String cfCase() {
        return "cf-case";
    }

    @GetMapping("/pb-case")
    public String pbCase() {
        return "pb-case";
    }

    @GetMapping("/qz-case")
    public String qzCase() {
        return "qz-case";
    }

    @GetMapping("/xk-case")
    public String xkCase() {
        return "xk-case";
    }

    @GetMapping("/person")
    public String person() {
        return "person";
    }

    @GetMapping("/car")
    public String car() {
        return "car";
    }

    @GetMapping("/document-no")
    public String documentNo() {
        return "document-no";
    }

    @GetMapping("/case")
    public String case1() {
        return "case";
    }

    @GetMapping("/casesate")
    public String casesate() {
        return "casesate";
    }

    @GetMapping("/constraint")
    public String constraint() {
        return "constraint";
    }

    @GetMapping("/permit-apply")
    public String permitApply() {
        return "permit-apply";
    }

    @GetMapping("/count-cri")
    public String countCri() {
        return "count-cri";
    }

    @GetMapping("/count-lin")
    public String countLin() {
        return "count-lin";
    }

    @GetMapping("/notice")
    public String notice() {
        return "notice";
    }

    @RequestMapping("/perdata")
    public String perdata() {
        return "perdata";
    }

    @GetMapping("/sellaws")
    public String sellaws() {
        return "sellaws";
    }

    @GetMapping("/index-officer-edit")
    public String indexOfficerEdit() {
        return "index-officer-edit";
    }

    @GetMapping("/mobnew")
    public String mobnew() {
        return "mobnew";
    }

    @GetMapping("/approval")
    public String approval() {
        return "approval";
    }

    @GetMapping("/index-increase-id")
    public String increaseId() {
        return "index-increase-id";
    }

    @GetMapping("/index-officer-increase")
    public String indexOfficerIncrease(){
        return "index-officer-increase";
    }
}
