package com.niaoren.usermanagerment.controller;

import com.niaoren.usermanagerment.common.BaseResult;
import com.niaoren.usermanagerment.common.Const;
import com.niaoren.usermanagerment.entity.User;
import com.niaoren.usermanagerment.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;


    /**
     * 跳转到login页面
     *
     * @creatDate 2019/5/27 9:55
     */
    @RequestMapping("/")
    public String login() {
        return "login";
    }

    @PostMapping("/doLogin")
    @ResponseBody
    public BaseResult<User> dologin(@RequestParam("userUserName") String userUserName,
                                    @RequestParam("userPassword") String userPassword,
                                    HttpSession session) {

        User userInfo = loginService.getUserInfo(userUserName, userPassword);
        if (userInfo != null) {
            //session.setAttribute(Const.CURRENT_USER,userInfo);
            return BaseResult.ok(userInfo);
        }
        return BaseResult.notok("error");
    }
}
