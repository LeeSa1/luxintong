package com.niaoren.usermanagerment.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseResult<T> implements Serializable {
    public static final String RESULT_OK = "ok";
    public static final String RESULT_NOT_OK = "not_ok";
    public static final String SUCCESS = "成功操作";

    private String result;
    private T data;
    private String success;
    private T code;
    private Cursor cursor;
    private List<Error> errors;

   public static BaseResult ok() {
        return createResult(RESULT_OK, null, SUCCESS, null, null);
    }


    public static <T> BaseResult<T> ok(T data,String success) {
        return createResult(RESULT_OK, data, success, null, null);
    }
    public static <T> BaseResult<T> ok(T data,T code) {
        return createResult(RESULT_OK, data, code, null, null);
    }

    public static <T> BaseResult<T> ok(T data) {
        return createResult(RESULT_OK, data, SUCCESS, null, null);
    }

    public static <T> BaseResult<T> ok(T data, Cursor cursor) {
        return createResult(RESULT_OK, data, SUCCESS, cursor, null);
    }

    public static BaseResult notOk(List<Error> errors) {
        return createResult(RESULT_NOT_OK, null, "", null, errors);
    }

    public static BaseResult notok(String status){

        return createResult(RESULT_NOT_OK,null , status, null, null);
    }

    public static <T> BaseResult<T> notok(T data) {
        return createResult(RESULT_NOT_OK, data, SUCCESS, null, null);
    }

    private static <T> BaseResult<T> createResult(String result, T data, T code, Cursor cursor, List<Error> errors) {
        BaseResult baseResult = new BaseResult();
        baseResult.setResult(result);
        baseResult.setData(data);
        baseResult.setCode(code);
        baseResult.setCursor(cursor);
        baseResult.setErrors(errors);

        return baseResult;
    }


    private static <T> BaseResult<T> createResult(String result, T data, String success, Cursor cursor, List<Error> errors) {
        BaseResult baseResult = new BaseResult();
        baseResult.setResult(result);
        baseResult.setData(data);
        baseResult.setSuccess(success);
        baseResult.setCursor(cursor);
        baseResult.setErrors(errors);

        return baseResult;
    }

    @Data
    @AllArgsConstructor
    public static class Cursor implements Serializable{
        private long total;
        private int offset;
        private int limit;
    }

    @Data
    @AllArgsConstructor
    public static class Error implements Serializable{
        private String field;
        private String message;
    }
}
