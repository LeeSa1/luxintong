package com.niaoren.usermanagerment.common;

public class Const {

    //user
    public static final String CURRENT_USER = "currentUser";  //当前用户
    public static final String ADMIN = "0";   //管理员
    public static final String DOCTOR = "2";  //医生
    public static final String HOSPITAL = "1"; //机构、医院

}
