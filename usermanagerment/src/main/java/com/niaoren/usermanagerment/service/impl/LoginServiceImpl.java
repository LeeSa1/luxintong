package com.niaoren.usermanagerment.service.impl;

import com.niaoren.usermanagerment.common.MD5Util;
import com.niaoren.usermanagerment.entity.User;
import com.niaoren.usermanagerment.entity.UserExample;
import com.niaoren.usermanagerment.mapper.UserMapper;
import com.niaoren.usermanagerment.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserMapper userMapper;

    /**
    * 验证用户是否存在以及输入密码是否正确
    * @creatDate  2019/5/27 10:12
    */
    @Override
    public User getUserInfo(String userUserName, String userPassword) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andUserNameEqualTo(userUserName);
        User user = userMapper.selectByExample(example).get(0);
        String md5EncodeUtf8 = MD5Util.MD5EncodeUtf8(userPassword);
        System.out.println(md5EncodeUtf8);
        if (md5EncodeUtf8.equals(user.getPassword())) {
            return user;
        }
        return null;
    }
}
