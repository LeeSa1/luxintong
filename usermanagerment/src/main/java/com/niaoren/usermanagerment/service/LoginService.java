package com.niaoren.usermanagerment.service;


import com.niaoren.usermanagerment.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface LoginService {

    User getUserInfo(String userUserName, String userPassword);
}
