package com.niaoren.usermanagerment.service;


import com.niaoren.usermanagerment.entity.UserStaff;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserStaffService {
    List<UserStaff> getUserStaffList(Integer id);

    UserStaff getUserInfo(Integer id);


}
