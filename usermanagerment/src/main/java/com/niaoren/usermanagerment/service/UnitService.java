package com.niaoren.usermanagerment.service;


import com.niaoren.usermanagerment.entity.Unit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UnitService {
    List<Unit> getUnitList(Integer userId);
}
