package com.niaoren.usermanagerment.service.impl;

import com.niaoren.usermanagerment.common.MD5Util;
import com.niaoren.usermanagerment.entity.User;
import com.niaoren.usermanagerment.mapper.UserMapper;
import com.niaoren.usermanagerment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int updatePassword(Integer id, String password, String newPassword) {
        User user1 = userMapper.selectByPrimaryKey(id.shortValue());
        String s = MD5Util.MD5EncodeUtf8(password);
        if (user1.getPassword().equals(s)) {
            User user = new User();
            user.setId(id.shortValue());
            String newPwd = MD5Util.MD5EncodeUtf8(newPassword);
            user.setPassword(newPwd);
            int i = userMapper.updateByPrimaryKeySelective(user);
            return i;
        }
        return 0;
    }
}
