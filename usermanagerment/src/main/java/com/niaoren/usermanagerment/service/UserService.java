package com.niaoren.usermanagerment.service;

public interface UserService {

    int updatePassword(Integer id, String password, String newPassword);

}
