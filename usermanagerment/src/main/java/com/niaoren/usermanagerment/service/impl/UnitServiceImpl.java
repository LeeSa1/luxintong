package com.niaoren.usermanagerment.service.impl;


import com.niaoren.usermanagerment.entity.Unit;
import com.niaoren.usermanagerment.entity.UnitExample;
import com.niaoren.usermanagerment.mapper.UnitMapper;
import com.niaoren.usermanagerment.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitMapper unitMapper;

    @Override
    public List<Unit> getUnitList(Integer userId) {
        UnitExample example = new UnitExample();
        UnitExample.Criteria criteria = example.createCriteria();
        criteria.andSuperiorUnitIdEqualTo(userId);
        List<Unit> units = unitMapper.selectByExample(example);
        return units;
    }
}
