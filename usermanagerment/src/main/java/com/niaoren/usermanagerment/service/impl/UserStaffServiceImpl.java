package com.niaoren.usermanagerment.service.impl;

import com.niaoren.usermanagerment.entity.UserExample;
import com.niaoren.usermanagerment.entity.UserStaff;
import com.niaoren.usermanagerment.entity.UserStaffExample;
import com.niaoren.usermanagerment.mapper.UserStaffMapper;
import com.niaoren.usermanagerment.service.UserStaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserStaffServiceImpl implements UserStaffService {

    @Autowired
    private UserStaffMapper userStaffMapper;

    @Override
    public List<UserStaff> getUserStaffList(Integer id) {
        UserStaffExample example = new UserStaffExample();
        UserStaffExample.Criteria criteria = example.createCriteria();
        criteria.andWorkUnitIdEqualTo(id);
        List<UserStaff> userStaffs = userStaffMapper.selectByExample(example);
        return userStaffs;
    }

    @Override
    public UserStaff getUserInfo(Integer id) {
        UserStaff userStaff = userStaffMapper.selectByPrimaryKey(id);
        return userStaff;
    }

}
